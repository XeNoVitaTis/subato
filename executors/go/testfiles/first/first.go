package first

func Id(n int )int{
  return n; 
}

func Fac(n int )int{
  if n<=1 {return n}else {return n*Fac(n-1)}
}

func Quersumme(n int) int{
  if n<0 {
    return Quersumme(-n)
  } else if n==0 {
    return 0
  } else { return n%10+Quersumme(n/10) }
}

func FromBinary(str string) int{
  r := 0
  for _, c := range str {
    r = r*2+int(c)-int('0')
  }
  return r;
}