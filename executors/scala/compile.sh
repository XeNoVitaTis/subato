#!/usr/bin/env bash
ls
find . -type f -name "*.scala"
mkdir classes
scalac -classpath /test/lib/junit-4.12.jar:/test/lib/hamcrest-core-1.3.jar -d classes `find . -type f -name "*.scala"`
