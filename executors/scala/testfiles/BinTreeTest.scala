import org.junit.Test
import junit.framework.TestCase
import org.junit.Assert._

class BinTreeTest extends TestCase{
  var xs:BinTree[Int] = Empty()
  var ys:BinTree[String] = Empty()
  var zs:BinTree[String] = Empty()

  override def setUp {
    xs = Empty() add 52 add -3 add 42 add 576 add -9876 add 4 add 0
    ys = Empty() add "das" add "pferd" add "frisst" add "keinen" add "gurkensalat"
    zs = Empty()
  }
  def test01 {
    assertTrue(xs contains 42);
  }
  def test02 {
    assertEquals(7L,xs.size);
    assertEquals(5L,ys.size);
    assertEquals(0L,zs.size);
  }
  def test03 {
    assertFalse(xs contains 43);

  }
  def test04 {
    assertTrue(ys contains "gurkensalat");
    assertFalse(ys contains "gurkensala");
  }
  def test05 {
    assertFalse(ys contains "43");
  }


}
