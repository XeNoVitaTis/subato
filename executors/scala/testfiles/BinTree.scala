trait BinTree[E]{
  def isEmpty() = this match {
    case Empty() => true
    case _ => false
  }
  def size():Long = this match {
    case Empty() => 0
    case Branch(l,el,r) => l.size + 1 + r.size
  }
  def contains(e : E)(implicit ordering:Ordering[E]):Boolean = this match {
    case Empty() => false
    case Branch(l,el,r) =>
      import ordering._;
      el==e || el>e&&l.contains(e)|| el<e&&r.contains(e)
  }
  def add(e : E)(implicit ordering:Ordering[E]):BinTree[E] = this match {
    case Empty() => Branch(Empty(),e,Empty())
    case Branch(l,el,r) =>{
      import ordering._;
      if (e==el)   this
      else if (e < el)  Branch(l.add(e),el,r) 
      else  Branch(l,el,r.add(e))
    }
  }
}

case class Empty[E]() extends BinTree[E]
case class Branch[E](val left:BinTree[E],val el:E,val right:BinTree[E]) extends BinTree[E]

object BinTreeMain extends App{
  val t1 = Empty() add 52 add -3 add 42 add 576 add -9876 add 4 add 0
  println(t1)
  val t2 = Empty() add "das" add "pferd" add "frisst" add "keinen" add "gurkensalat"
  println(t2)

}
