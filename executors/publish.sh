#!/usr/bin/env bash

docker login registry.gitlab.com/sveneric/subato
docker push registry.gitlab.com/sveneric/subato/executor_${1}:${2}

echo "You can now use this image in subato. Just add a testExecutor with image: registry.gitlab.com/sveneric/subato/executor_${1}:${2}"