package test
fun plus(x:Int,y:Int)=x+y

fun fac(x:Int):Int=if (x==0) 1 else x*fac(x-1)

fun ableitung(x: Double,f: (Double)->Double)=(f(x+0.1)-f(x))/0.1

fun ableitung(f: (Double)->Double):(Double)->Double = {x -> (f(x+0.1)-f(x))/0.1}
  