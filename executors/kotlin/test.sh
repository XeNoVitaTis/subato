#!/usr/bin/env bash

#getTestClassnames.sh
#kotlin  -cp /test/lib/junit-4.12.jar:/test/lib/hamcrest-core-1.3.jar:classes org.junit.runner.JUnitCore  `getTestClassnames.sh`

ls -l /usr/lib

ant test

antReturnCode=$?

if [ $antReturnCode -ne 0 ];then
     exit 1;
else
     exit 0;
fi
