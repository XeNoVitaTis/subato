#!/usr/bin/env bash

for FILE in `find . -type f -name "*Test.py"`
do
    python $FILE
done

antReturnCode=$?

cat TEST*.xml

if [ $antReturnCode -ne 0 ];then
     exit 1;
else
     exit 0;
fi
