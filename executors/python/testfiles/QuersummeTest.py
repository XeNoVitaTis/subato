import unittest
from quersumme import quersumme
import xmlrunner 

class QuersummeTest(unittest.TestCase):

    def test1(self):
        self.assertEqual(5, quersumme(5))
    def test2(self):
        self.assertEqual(7, quersumme(88))
    def test3(self):
        self.assertEqual(1, quersumme(42669865576565465698776956468756754534535))

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(QuersummeTest)
    xmlrunner.XMLTestRunner(output=".").run(suite)



