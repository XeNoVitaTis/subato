<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="assemblies">
    <testsuites>
      <xsl:apply-templates/>
    </testsuites>
  </xsl:template>

  <xsl:template match="assembly">
    <testsuite>
      <xsl:attribute name="tests">
	<xsl:value-of select="@total" />
      </xsl:attribute>
      <xsl:attribute name="failures">
	<xsl:value-of select="@failed" />
      </xsl:attribute>
      <xsl:attribute name="time">
	<xsl:value-of select="@time" />
      </xsl:attribute>
      <xsl:attribute name="name">
	<xsl:value-of select="@name" />
      </xsl:attribute>

      <xsl:apply-templates/>
    </testsuite>
  </xsl:template>
  
  <xsl:template match="test">
    <testcase>
      <xsl:attribute name="name">
	<xsl:value-of select="@name" />
      </xsl:attribute>
      <xsl:attribute name="time">
	<xsl:value-of select="@time" />
      </xsl:attribute>

      <xsl:apply-templates/>
    </testcase>
  </xsl:template>

  <xsl:template match="failure">
    <failure message="failed Failing" type="failed">
      <xsl:value-of select="message/text()"/>
    </failure>
  </xsl:template>  
  <xsl:template match="*">
    <xsl:apply-templates/>
  </xsl:template>
</xsl:stylesheet>
