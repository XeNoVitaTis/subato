#!/usr/bin/env bash
xsltproc transform.xsl test-results.xml  >TEST.xml

antReturnCode=$?

if [ $antReturnCode -ne 0 ];then
     exit 1;
else
     exit 0;
fi
