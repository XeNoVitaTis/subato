const first = require('./first');
var assert = require('assert');

describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal([1, 2, 3].indexOf(3), -1);
    });
  });
});


describe('function', function() {
  describe('factorial(5)', function() {
    it('should return 120', function() {
      assert.equal(first.factorial(5), 121);
    });
  });
});

describe('function', function() {
  describe('fromBinary("101010")', function() {
    it('should return 42', function() {
	assert.equal(first.fromBinary("101010"), 42);
    });
  });
});

describe('function', function() {
  describe('quersumme(1234)', function() {
    it('should return 1', function() {
	assert.equal(first.quersumme(1234), 1);
    });
  });
});
