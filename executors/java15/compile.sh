#!/usr/bin/env bash

/jdk-15/bin/javac --enable-preview --release 15 -Xlint:preview   -Xlint:deprecation  -Xlint:unchecked -cp /test/lib/junit-4.12.jar:/test/lib/hamcrest-core-1.3.jar -d classes `find . -type f -name "*.java"`
