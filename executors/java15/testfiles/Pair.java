record Pair<A,B>(A e1, B e2) {
  @Override public boolean equals(Object o){
    return o instanceof Pair that&&e1.equals(that.e1())&&e2.equals(that.e2());
  }
}
