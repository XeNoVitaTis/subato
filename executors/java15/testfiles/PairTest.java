import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PairTest {
  @Test
  public void test01() {
    var p = new Pair<>("hallo",42);
    assertEquals( 42, p.e2().intValue());
    assertEquals( "hallo", p.e1());
  }
}
