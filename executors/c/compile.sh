#!/usr/bin/env bash

/usr/bin/gcc -ggdb -O2 -fPIE -pie -Wformat -Wformat-security -Wall -o test-runner `find . -regextype posix-egrep -regex ".*\.(c)$"` -lm
