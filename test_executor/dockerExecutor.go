package main

import (
	"encoding/xml"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"io"
	"io/ioutil"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

type DockerExecutor struct {
	Ctx			 context.Context
	Cli			 *client.Client
	Container string
	Logger		*logrus.Logger
	Image		 string
}

type TestSuite struct {
	Name			string `xml:"name,attr"`
	Tests		 int32	`xml:"tests,attr"`
	Skipped	 int32	`xml:"skipped,attr"`
	Failures	int32	`xml:"failures,attr"`
	Errors		int32	`xml:"errors,attr"`
	Timestamp string `xml:"timestamp,attr"`
	Time			string `xml:"time,attr"`
	Testcase	[]struct {
		Name			string `xml:"name,attr"`
		Classname string `xml:"classname,attr"`
		Time			string `xml:"time,attr"`
		Failure	 struct {
			Text		string `xml:",chardata"`
			Message string `xml:"message,attr"`
			Type		string `xml:"type,attr"`
		} `xml:"failure"`
		Error	 struct {
			Text		string `xml:",chardata"`
			Message string `xml:"message,attr"`
			Type		string `xml:"type,attr"`
		} `xml:"error"`
	} `xml:"testcase"`
	SystemOut string `xml:"system-out"`
	SystemErr string `xml:"system-err"`
	Results	 struct {
		Tests		int32 `xml:"tests,attr"`
		Failures int32 `xml:"failures,attr"`
		Errors	 int32 `xml:"errors,attr"`
	} `xml:"results"`
	Storage struct {
		Allocated int32 `xml:"allocated,attr"`
		Free			int32 `xml:"free,attr"`
		Diff			int32 `xml:"diff,attr"`
	} `xml:"storage"`
}

type Checkstyle struct {
	XMLName xml.Name `xml:"checkstyle"`
	Text		string	 `xml:",chardata"`
	Version string	 `xml:"version,attr"`
	Files	 []struct {
		Text	string `xml:",chardata"`
		Name	string `xml:"name,attr"`
		Error []struct {
			Text		 string `xml:",chardata"`
			Line		 string `xml:"line,attr"`
			Severity string `xml:"severity,attr"`
			Message	string `xml:"message,attr"`
			Source	 string `xml:"source,attr"`
			Column	 string `xml:"column,attr"`
		} `xml:"error"`
	} `xml:"file"`
}

func (d *DockerExecutor) prepare() {
	_, err := d.Cli.ImagePull(d.Ctx, d.Image, types.ImagePullOptions{})
	if err != nil {
		d.Logger.Errorf("Error pulling image %s", err)
	}

	resp, err := d.Cli.ContainerCreate(d.Ctx, &container.Config{
		Image: d.Image,
		Cmd:	 []string{"/bin/sh"},
		Tty:	 true,
	}, nil, nil, "")

	if err != nil {
		d.Logger.Errorf("Error creating container: %s", err)
	}

	d.Container = resp.ID

	if err := d.Cli.ContainerStart(d.Ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		d.Logger.Errorf("Error starting container %s", err)
	}
}

func (d *DockerExecutor) insertFile(fileName string, content string) {
	d.exec([]string{"/bin/sh", "-c", "mkdir -p " + filepath.Dir(fileName)})
	d.exec([]string{"/bin/sh", "-c", "cat >" + fileName + " <<'EOF'\n" + content + " \nEOF"})
}

func (d *DockerExecutor) runPrepare(result TestResult) TestResult {
	d.Logger.Infof("Running Compile")

	var exitCode int
	result.CompilationOutput, exitCode = d.exec([]string{"/usr/bin/compile.sh"})

	if exitCode != 0 {
		result.CompilationFailed = true
	}

	d.Logger.Infof("Completed Compile")

	return result
}

func (d *DockerExecutor) runTest(result TestResult) TestResult {
	d.Logger.Infof("Running tests")

	_, exitCode := d.exec([]string{"/usr/bin/test.sh"})
	d.Logger.Infof("Completed tests")
	if exitCode == 283 {
		d.Logger.Warnf("Test ran into timeout")
		result.CompilationFailed = true
		result.CompilationOutput += "Tests ran into a timeout... Check if you have an endless loop in your code"
	}

	return result
}

func (d *DockerExecutor) runApprovalTest(result TestResult) TestResult {
	d.Logger.Infof("Running Approval tests")

	_, exitCode := d.exec([]string{"/usr/bin/approvalTest.sh"})
	d.Logger.Infof("Completed Approval tests")

	if exitCode == 283 {
		d.Logger.Warnf("Test ran into timeout")
		result.CompilationFailed = true
		result.CompilationOutput += "Tests ran into a timeout... Check if you have an endless loop in your code"
	}

	return result
}

func (d *DockerExecutor) runResults(result TestResult) TestResult {
	d.Logger.Infof("Running results")
	out, _ := d.exec([]string{"/usr/bin/results.sh"})
	//d.Logger.Infof("Processing xml src: %s", out)
	
	start, _ := regexp.Compile("<testsuite[>\\s][\\s\\S]*")
	s := start.FindString(out)
	r, _ := regexp.Compile("</testsuite>")
	results := r.Split(s, -1)

	for _, res := range results {
		suit := TestSuite{}
		if strings.TrimSpace(res) == "" { continue }
		//fmt.Println(res+"</testsuite>")
 
		err := xml.Unmarshal([]byte(res+"</testsuite>"), &suit)
		//d.Logger.Infof("Next xml src: %s", res)
		if err != nil {
			d.Logger.Errorf("Error unmarshalling xml src: %s, err: %s", res, err)
		}

		result.Tests += suit.Tests
		result.Tests += suit.Results.Tests
		result.Failures += suit.Failures
		result.Failures += suit.Results.Failures
		result.Errors += suit.Errors
		result.Errors += suit.Results.Errors

		result.Frees += suit.Storage.Free
		result.Allocations += suit.Storage.Allocated

		for _, testCase := range suit.Testcase {
			c := TestCase{
				Name:			testCase.Name,
				ClassName: testCase.Classname,
				Failure: &Failure{
					Text:		(testCase.Failure.Text+testCase.Error.Text),
					Message:	(testCase.Failure.Message+testCase.Error.Message),
					Type:		testCase.Failure.Type,
				},
			}
		
			result.TestCases = append(result.TestCases, &c)
		}
	}

	result.CompilationFailed = false

	d.Logger.Infof("Completed Results")
	return result
}

func (d *DockerExecutor) runCheckStyle(result TestResult) TestResult {
	d.Logger.Info("Running checkstyle")
	out, _ := d.exec([]string{"/usr/bin/checkstyle.sh"})

	checkstyle := Checkstyle{}

	r, _ := regexp.Compile("<checkstyle[\\s\\S]*</checkstyle>")

	style := r.FindAllString(out, 1)
	if (len(style)>0){
		err := xml.Unmarshal([]byte(style[0]), &checkstyle)
		if err != nil {
			d.Logger.Errorf("Error unmarshalling xml src: %s, err: %s", out, err)
		}		 

		for _, file := range checkstyle.Files {
			for _, styleError := range file.Error {
				styleResult := StyleCheckResult{}
				styleResult.File = file.Name
				styleResult.Message = styleError.Message
				styleResult.Severity = styleError.Severity
				styleResult.Line = styleError.Line
				styleResult.Column = styleError.Column
				result.StyleCheckResults = append(result.StyleCheckResults, &styleResult)
			}
		}

		result.StylecheckErrors = int32(len(result.StyleCheckResults))
	}
	d.Logger.Info("Done running checkstyle")
	return result
}

func (d *DockerExecutor) exec(cmd []string) (string, int) {
	c := types.ExecConfig{AttachStdout: true, AttachStderr: true,
		Cmd: cmd}

	execID, err := d.Cli.ContainerExecCreate(d.Ctx, d.Container, c)
	if err != nil {
		d.Logger.Errorf("Error creating container exec: %s", err)
	}

	config := types.ExecConfig{}
	res, er := d.Cli.ContainerExecAttach(d.Ctx, execID.ID, config)
	if er != nil {
		d.Logger.Errorf("Error attaching exec %s", err)
	}

	err = d.Cli.ContainerExecStart(d.Ctx, execID.ID, types.ExecStartCheck{})
	if err != nil {
		fmt.Println(err)
	}

	var exitCode int

	retryCount := 0
	for true {

		resp, err := d.Cli.ContainerExecInspect(d.Ctx, execID.ID)
		if err != nil {
			fmt.Println(err)
		}

		if !resp.Running {
			exitCode = resp.ExitCode
			break
		}

		//More than 10 seconds have passed
		if retryCount > 100 {
			return "Process ran into a Timeout", 283
		}

		retryCount++
		time.Sleep(time.Millisecond * 100)
	}

	ret, err := ioutil.ReadAll(res.Reader)

	if err != nil {
		d.Logger.Errorf("Error reading exec output %s", err)
	}
	x := strings.ToValidUTF8(strings.TrimLeft(string(ret), "\x01\x00\x00\x00\x00\x00\x02"), "")
	return strings.TrimLeft(x, "\x17"), exitCode
}

func (d *DockerExecutor) runJplagTest() string {
	d.Logger.Infof("Running Jplag")

	out, _ := d.exec([]string{"/bin/bash", "-c", "java -jar /usr/lib/jplag.jar -l java19 -r /jplag_result -s /jplag"})

	d.Logger.Infof("Completed Jplag")

	return out
}

func (d *DockerExecutor) getJplagResult() (io.ReadCloser, error) {
	file, _, err := d.Cli.CopyFromContainer(d.Ctx, d.Container, "/jplag_result")

	if err != nil {
		d.Logger.Errorf("Error getting jplag result: %s", err.Error())
		return nil, err
	}

	return file, nil
}

func (d *DockerExecutor) deleteContainer() {
	err := d.Cli.ContainerRemove(d.Ctx, d.Container, types.ContainerRemoveOptions{
		RemoveVolumes: true,
		RemoveLinks:	 false,
		Force:				 true,
	})

	if err != nil {
		d.Logger.Errorf("Error removing container: %s", err)
	}
}
