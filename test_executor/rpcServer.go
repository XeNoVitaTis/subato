package main

import (
	"context"
	"github.com/docker/docker/client"
	"github.com/sirupsen/logrus"
	"io/ioutil"
//	"path/filepath"
)

type RPCServer struct {
	Logger *logrus.Logger
}

func (R RPCServer) RunTest(ctx context.Context, set *TestSet) (*TestResult, error) {
	result := TestResult{}

	R.Logger.WithFields(logrus.Fields{
		"User":  "jfass001",
		"Image": set.TestImage,
	}).Infof("Starting testrun")

	D := DockerExecutor{}

	D.Logger = R.Logger
	D.Ctx = context.Background()
	var err error
	D.Cli, err = client.NewEnvClient()
	if err != nil {
		panic(err)
	}
	D.Image = set.TestImage
	D.prepare()

	D.insertFile(set.CodeFqdn, set.Code)

	for _, file := range set.TestFiles {
		D.insertFile( //filepath.Dir(set.CodeFqdn)+"/"+
		             file.FileName, file.Content)
	}

	result = D.runPrepare(result)

	if !result.CompilationFailed {
		if set.IsApprovalCheck {
			result = D.runApprovalTest(result)
		} else {
			result = D.runTest(result)
		}
		if !result.CompilationFailed {
			result = D.runResults(result)
		}
	}

	if set.CheckStyle {
		R.Logger.Infof("check style starts")
		result = D.runCheckStyle(result)
		R.Logger.Infof("check style resulted")
	}

	D.deleteContainer()
	R.Logger.Infof("Run Test ist endlich fertich")

	return &result, nil
}

func (R RPCServer) RunPlagCheck(ctx context.Context, in *PlagCheckRequest) (*PlagCheckResponse, error) {
	response := PlagCheckResponse{}

	R.Logger.Infof("Starting plag check")

	D := DockerExecutor{}

	D.Logger = R.Logger
	D.Ctx = context.Background()
	var err error
	D.Cli, err = client.NewEnvClient()

	if err != nil {
		panic(err)
	}

	D.Image = jplagImage
	D.prepare()

	for _, file := range in.Files {
		D.insertFile("/jplag/"+file.User+"/"+file.Name, file.Content)
	}

	response.Output = D.runJplagTest()
	results, err := D.getJplagResult()

	D.deleteContainer()

	if err != nil {
		return &response, nil
	}

	response.Result, err = ioutil.ReadAll(results)
	if err != nil {
		D.Logger.Errorf("Error writing jplag response: %s", err.Error())
		return &response, nil
	}

	return &response, nil
}
