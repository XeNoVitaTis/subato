package main

import (
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"net"
	"os"
)

var jplagImage = os.Getenv("JPLAG_IMAGE")

func newServer() RPCServer {
	rpc := RPCServer{}

	rpc.Logger = logrus.New()
	rpc.Logger.SetLevel(logrus.DebugLevel)
	return rpc
}

func main() {
	rpcServer := newServer()

	listenString := "0.0.0.0:8081"
	rpcServer.Logger.Infof("Starting server on %s", listenString)

	lis, err := net.Listen("tcp", listenString)
	if err != nil {
		rpcServer.Logger.Fatalf("Failed to listen: %v", err)
	}

	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	RegisterEvaluatorServiceServer(grpcServer, rpcServer)

	err = grpcServer.Serve(lis)
	if err != nil {
		rpcServer.Logger.Fatalf("Error starting RPC server: %v", err)
	}
}
