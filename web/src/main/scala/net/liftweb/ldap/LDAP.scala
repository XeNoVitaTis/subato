package net.liftweb.ldap

import java.io.{InputStream, FileInputStream}
import java.util.{Hashtable, Properties}

import javax.naming.Context
import javax.naming.directory.{BasicAttributes, SearchControls}
import javax.naming.ldap.{LdapName, InitialLdapContext}

import scala.collection.convert.Wrappers.{ JMapWrapper}
//import scala.util.logging.{Logged, ConsoleLogger}


/**
 * Extends SimpleLDAPVendor trait,
 * Allow us to search and bind an username from a ldap server
 * To set the ldap server parameters override the parameters var directly:
 *    SimpleLDAPVendor.parameters = () => Map("ldap.username" -> ...
 * or set the parameters from a properties file or a inputStream
  *    SimpleLDAPVendor.parameters = () => SimpleLDAPVendor.parametersFromFile("/opt/config/ldap.properties")
 *    SimpleLDAPVendor.parameters = () => SimpleLDAPVendor.parametersFromStream(
                this.getClass().getClassLoader().getResourceAsStream("ldap.properties"))
 *
 * The mandatory parameters are :
 *  ldap.url -> LDAP Server url : ldap://localhost
 *  ldap.base -> Base DN from the LDAP Server : dc=company, dc=com
 *  ldap.userName -> LDAP user dn to perform search operations
 *  ldap.password -> LDAP user password
 */
object SimpleLDAPVendor extends SimpleLDAPVendor{ // with ConsoleLogger {
    def parametersFromFile(filename: String) : StringMap = {
        return parametersFromStream(new FileInputStream(filename))
    }

    def parametersFromStream(stream: InputStream) : StringMap = {
        val p = new Properties()
        p.load(stream)

        return convertToStringMap(p.asInstanceOf[Hashtable[String, String]])
    }

    private def convertToStringMap(javaMap: Hashtable[String, String]) = {
        Map.empty ++ new JMapWrapper[String, String](javaMap) {
          //  def underlying = 
        }
    }
}

trait SimpleLDAPVendor extends LDAPVendor

class LDAPVendor extends name.panitz.subato.XmlConfigDependency{ //extends Logged {

    type StringMap = Map[String, String]

    val DEFAULT_URL = config.ldapUrl //"localhost"
    val DEFAULT_BASE_DN = ""
    val DEFAULT_USER = ""
    val DEFAULT_PASSWORD = ""
    val DEFAULT_INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory"

    var parameters = Map[String,String]() //StringMap = () => null

    lazy val initialContext = getInitialContext(parameters)

    def getFullName(userDn:String):String = {
      if (userDn.toLowerCase == "studi") return "Karoline Test"
      if (userDn.toLowerCase == "admin") return "Administrator"
      val ctx = initialContext
      def rolesSearchFilter: String = "(&(objectclass=person)(uid=%s))"
      val filter = rolesSearchFilter.format(userDn)
      //println(filter)
      if (!ctx.isEmpty) {
        val result = ctx.get
          .search(parameters.getOrElse
            ("ldap.base", DEFAULT_BASE_DN),filter, getSearchControls())
         while(result.hasMore()) {
//           println("yes")
           var r = result.next()
           val attrs = r.getAttributes()
           val res = attrs.get("username").get()
           //println(res)
           val mat = attrs.get("mat_no").get()
           return res+" "+mat
         }
      }
      return ""
    }

    def search(filter: String) : List[String] = {
      //  log("--> LDAPSearch.search: Searching for '%s'".format(filter))

        var list = List[String]()
        try{
        val ctx = initialContext
        //println(filter)
        if (!ctx.isEmpty) {
          val result = ctx.get.search(parameters.getOrElse
            ("ldap.base", DEFAULT_BASE_DN),filter,getSearchControls())
          //println(result)
          //println(result.hasMore)
            while(result.hasMore()) {
              var r = result.next()
              //println(r)
              list = list ::: List(r.getName)
              //val attrs = r.getAttributes();
              //val cn = attrs.get("mat_no");
              //println(cn)
//              println(cn.get())
            }
        }
          //println(list)
        }catch{
          case e: Exception => println(e)
        }
        return list
    }

    def bindUser(dn: String, password: String) : Boolean = {
//       // log("--> LDAPSearch.bindUser: Try to bind user '%s'".format(dn))
  //    println("binUser: "+dn)
        var result = false

        try {
            val username = dn + "," + parameters.getOrElse("ldap.base", DEFAULT_BASE_DN)
            var ctx = getInitialContext(parameters ++ Map("ldap.userName" -> username,
                                                            "ldap.password" -> password))
            result = !ctx.isEmpty
            ctx.get.close
        }
        catch {
            case e: Exception => println(e)
        }

       // log("--> LDAPSearch.bindUser: Bind successfull ? %s".format(result))
       // println(getFullName(dn))
        //println(result)
        return result
    }

    // TODO : Allow search controls && attributes without override method ?
    def getSearchControls() : SearchControls = {
        val searchAttributes = new Array[String](2)
        searchAttributes(0) = "username"
        searchAttributes(1) = "mat:no"

        val constraints = new SearchControls()
        constraints.setSearchScope(SearchControls.SUBTREE_SCOPE)
        constraints.setReturningAttributes(searchAttributes)
        return constraints
    }

    private def getInitialContext(props: StringMap) : Option[InitialLdapContext] = {
        // log("--> LDAPSearch.getInitialContext: Get initial context from '%s'".format(props.get("ldap.url")))

        var env = new Hashtable[String, String]()
        env.put(Context.PROVIDER_URL, props.getOrElse("ldap.url", DEFAULT_URL))
        env.put(Context.SECURITY_AUTHENTICATION, "Simple")
        env.put(Context.SECURITY_PRINCIPAL, props.getOrElse("ldap.userName", DEFAULT_USER))
        env.put(Context.SECURITY_CREDENTIALS, props.getOrElse("ldap.password", DEFAULT_PASSWORD))
        env.put(Context.INITIAL_CONTEXT_FACTORY, parameters.getOrElse("ldap.initial_context_factory",
                                                                        DEFAULT_INITIAL_CONTEXT_FACTORY))
        return Some(new InitialLdapContext(env, null))
    }
}
