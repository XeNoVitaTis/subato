package name.panitz.subato.evaluator

import name.panitz.subato.XmlConfigDependency
import name.panitz.subato.ConfigDependency
import name.panitz.subato.model.UnitTestCaseResult
import net.liftweb.common.Full

import scala.xml.XML
import name.panitz.subato.model.{Exercise, MapperModelDependencies, MapperSubmissions, ModelDependencies, Submission}
import name.panitz.subato.FileUtil
import java.io.{BufferedReader, File, FileWriter, InputStreamReader}
import java.util.Date

import net.liftweb.common.Logger
import net.liftweb.http.FileParamHolder

import scala.language.postfixOps

class FileUploadedSolution extends Evaluator
  with XmlConfigDependency
  with MapperModelDependencies
  with Logger {

  override def testSolution(code: String, userDir: String, exercise: Exercise,
                            courseId: Long, termId: Long, fileUpload: FileParamHolder, storeResult: Boolean, isApprovalCheck: Boolean) = {
    init(code, userDir, exercise, courseId, termId)
    time = System.currentTimeMillis
    solutionDir = userDir + "/" + time + "/"
    FileUtil.mkdir(solutionDir + "/src/")

    System.out.println(fileUpload.fileName)
    //Assumes the fileName does not contain dots other than to specify the file ext.
    val fileUploadExtension = fileUpload.fileName.split("\\.")(1)

    val submission
    = submissions
      .create(new Date(time)
        , exercise
        , courseId
        , termId
        , currentUser.uid.openOr("anonymous")
        , fileUploadExtension) //This is a dirty hack to get the filename + file extension into the submission... Maybe provide a seperate field later


    //Check if the user provided a file with the correct file extension
    if (exercise.fileExtention.split(";").contains(fileUploadExtension)) {
      //File extension is correct, save now
      val outStream = new java.io.FileOutputStream(solutionDir + "/uploadedFile." + fileUploadExtension)
      if (fileUpload.length > 0) {
        val read = fileUpload.fileStream
        var i = read.read();
        while(i>=0){
          outStream.write(i);
          i = read.read();
        }
        outStream.flush
        outStream.close
      }
      submission.directory = "" + time
      submission.compilationFailed = false
      submission.compilationOutput = "Danke für die Abgabe. Wir schauen es uns manuell an."
      submission.messages = ""
      submission.tests = 1
      submission.score = 5
      submission.styleCheckOutput = <div/>
      submission.stylecheckErrors = 0
    } else {
      submission.directory = "" + time
      submission.compilationFailed = true
      submission.messages = ""
      submission.compilationOutput = "Fehlerhaftes Dateiformat. Abgegeben: " + fileUploadExtension + " Erwartet: " + exercise.fileExtention
      submission.tests = 1
      submission.score = 0
    }





    if (graded && storeResult) submission.save()
    submission
  }


  def srcFiles(exDir: String): Array[String] = Array.empty[String]

}
