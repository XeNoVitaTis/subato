package name.panitz.subato
 
import java.io.ByteArrayOutputStream
import java.util.Base64 
import javax.crypto.Cipher
import javax.crypto.SecretKey 
import javax.crypto.SecretKeyFactory 
import javax.crypto.spec.PBEKeySpec 
import javax.crypto.spec.PBEParameterSpec 
 
object Encrypt { 
  val s = "witzelbritz"

  def main(args: Array[String]): Unit = {
    val encrypted = encrypt(args(0)); 
    println(encrypted)
  }

  val ITERATION_COUNT = 1000 
  val SALT_LENGTH = 8 
  val ALGORITHM = "PBEWithMD5AndDES";
  val EXTRA_PROVIDER = null; 
 
  def encrypt( password: String) :String = {
    encrypt(s.getBytes(), (password).toCharArray())
  }
   
  def  encrypt( input :Array[Byte],  password:Array[Char]) :String ={
    var salt = new Array[Byte](SALT_LENGTH)
    salt(0) = 'ä'.asInstanceOf[Byte]
    salt(1) = '„'.asInstanceOf[Byte]
    salt(2) = 'đ'.asInstanceOf[Byte]
    salt(3) = 'ſ'.asInstanceOf[Byte]
    salt(4) = '+'.asInstanceOf[Byte]
    salt(5) = '''.asInstanceOf[Byte]
    salt(6) = 'ß'.asInstanceOf[Byte]
    salt(7) = 'f'.asInstanceOf[Byte]
    
    val keyspec = new PBEKeySpec(password, salt, ITERATION_COUNT)
    val skf = SecretKeyFactory.getInstance(ALGORITHM)
    val key = skf.generateSecret(keyspec)
    val baos = new ByteArrayOutputStream()
    val paramspec = new PBEParameterSpec(salt, ITERATION_COUNT)
    val cipher = Cipher.getInstance(ALGORITHM)
    cipher.init(Cipher.ENCRYPT_MODE, key, paramspec) 
//    baos.write(salt)
    baos.write(cipher.doFinal(input))
    keyspec.clearPassword()
    Base64.getEncoder.encodeToString(baos.toByteArray)
  } 
}
