package name.panitz.subato.model.mapper

import name.panitz.subato.model.Progress

import name.panitz.subato.SWrapperL10n
import name.panitz.subato.XmlConfigDependency


import _root_.net.liftweb.mapper._
import net.liftweb.common.{Full, Box}
import net.liftweb.http.{S, RedirectResponse}
import net.liftweb.sitemap.Loc
import net.liftweb.util.ControlHelpers
import scala.xml.Text

import net.liftweb.common.Empty
import net.liftweb.mapper.CRUDify

class Duel  extends LongKeyedMapper[Duel]
//  with MapperModelDependencies
  with IdPK
  with ControlHelpers
  with SWrapperL10n
  with XmlConfigDependency
{

  def getSingleton = Duel

  def this(cId:Long,cardBoxId:Long,uId:String){
    this()
    this.course.set(cId)
    this.cardBox.set(cardBoxId)
    this.player1.set(uId)
  }

  object course extends MappedLongForeignKey(this, Course)
  object cardBox extends MappedLongForeignKey(this, CardBox)

  object player1 extends MappedPoliteString(this, 100)
  object player2 extends MappedPoliteString(this, 100)

  object card1 extends MappedLongForeignKey(this, MultipleCard)
  object card2 extends MappedLongForeignKey(this, MultipleCard)
  object card3 extends MappedLongForeignKey(this, MultipleCard)
  object card4 extends MappedLongForeignKey(this, MultipleCard)
  object card5 extends MappedLongForeignKey(this, MultipleCard)
  object card6 extends MappedLongForeignKey(this, MultipleCard)
  object card7 extends MappedLongForeignKey(this, MultipleCard)
  object card8 extends MappedLongForeignKey(this, MultipleCard)
  object card9 extends MappedLongForeignKey(this, MultipleCard)
  object card10 extends MappedLongForeignKey(this, MultipleCard)
  object card11 extends MappedLongForeignKey(this, MultipleCard)
  object card12 extends MappedLongForeignKey(this, MultipleCard)
  object card13 extends MappedLongForeignKey(this, MultipleCard)
  object card14 extends MappedLongForeignKey(this, MultipleCard)
  object card15 extends MappedLongForeignKey(this, MultipleCard)

  object pl1Card1 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card2 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card3 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card4 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card5 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card6 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card7 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card8 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card9 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card10 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card11 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card12 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card13 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card14 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl1Card15 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }

  object pl2Card1 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card2 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card3 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card4 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card5 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card6 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card7 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card8 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card9 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card10 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card11 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card12 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card13 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card14 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }
  object pl2Card15 extends MappedEnum(this, Progress) {
    override def defaultValue = Progress.NotPlayed
  }

  object score1 extends MappedInt(this){
    override def defaultValue = 0
  }
  object score2 extends MappedInt(this){
    override def defaultValue = 0
  }
  object next1 extends MappedInt(this){
    override def defaultValue = 0
  }
  object next2 extends MappedInt(this){
    override def defaultValue = 0
  }

}

object Duel extends Duel
  with LongKeyedMetaMapper[Duel]
  with LecturerOnlyCRUDify[Long, Duel] {


}
