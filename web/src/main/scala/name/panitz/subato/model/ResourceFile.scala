package name.panitz.subato.model

import net.liftweb.common.Box

import scala.xml.Node

trait ResourceFile extends  IdAndNamed {
  def name:String
  def description:String
  def exercise:Exercise
  def isSrcFile:Boolean
}


private[model] class MapperResourceFile(private val c: mapper.ResourceFile) extends ResourceFile {
  override def id = c.id.get
  override def name:String = c.name.get
  override def description:String = c.description.get
  override def exercise = new MapperExercise(c.exercise.obj.openOrThrowException(""))
  override def isSrcFile: Boolean = false
}

trait ResourceFiles extends Finder[ResourceFile] {
  def createAndSave(name: String,description:String,exId:Long, isSrcFile:Boolean)
}

import name.panitz.subato.model.mapper.{ ResourceFile => MC }

object MapperResourceFiles extends  ResourceFiles {
  override def find(id: Long) = MC.find(id).map(new MapperResourceFile(_))
  override def findAll = MC.findAll.map(new MapperResourceFile(_))

  override def createAndSave(name: String,description:String,exId:Long, isSrcFile:Boolean) = {
    val mc = new MC().name(name).description(description).exercise(exId).isSrcFile(isSrcFile)
    mc.save()
    new MapperResourceFile(mc)
  }
}


