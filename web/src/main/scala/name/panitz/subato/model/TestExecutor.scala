package name.panitz.subato.model

import name.panitz.subato.model.mapper.{TestExecutor => TE}
import net.liftweb.common.{Full, Box}
import _root_.net.liftweb.mapper.By

trait TestExecutors extends Finder[TestExecutor] {
  def createAndSave(name: String): TestExecutor
  def find(name: String): Box[TestExecutor]
}

trait TestExecutor extends IdAndNamed {
  def name: String
  def image: String
  def language: String
}

object MapperTestExecutors extends TestExecutors {
  override def find(id: Long) = TE.find(id).map(new MapperTestExecutor(_))
  override def findAll = TE.findAll.map(new MapperTestExecutor(_))
  override def createAndSave(name:String) = {
    val mc = new TE().name(name)
    mc.save()
    new MapperTestExecutor(mc)
  }
  override def find(name: String) = TE.find(By(TE.name, name)).map(new MapperTestExecutor(_))
}

private[model] class MapperTestExecutor(private val c: mapper.TestExecutor) extends TestExecutor {
  override def id = c.id.get
  override def name = c.name.get
  override def image = c.image.get
  override def language = c.language.get

  override def toString= name
}
