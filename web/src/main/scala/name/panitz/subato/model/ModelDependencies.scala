package name.panitz.subato.model
import name.panitz.subato.model.learnsystem._

trait ModelDependencies {
  protected val currentUser:CurrentUser
  protected val terms:Terms
  protected val testExecutors:TestExecutors
  protected val statistics: Statistics
  protected val courses:Courses
  protected val coursePages:CoursePages
  protected val exercises:Exercises
  protected val exerciseSheets:ExerciseSheets
  protected val exerciseSheetEntries:ExerciseSheetEntries
  protected val submissions:Submissions
  protected val admins:Admins
  protected val lecturers:Lecturers
  protected val tutors:Tutors
  protected val groups:Groups
  protected val userInGroups:UserInGroups
  protected val resourceFiles:ResourceFiles
  protected val resourceFileForCourses:ResourceFileForCourses
  protected val multipleCards:MultipleCards
  protected val cardBoxes:CardBoxes
  protected val answers:Answers
  protected val duels:Duels
  protected val surveys:Surveys
  protected val ontologys:Ontologys
  protected val flashcardCourses: FlashcardCourses
  protected val subscribedFlashcardCourses: SubscribedFlashcardCourses
  protected val learningMaterials: LearningMaterials
  protected val learningProfiles: LearningProfiles
  protected val deletedFlashcardcourses: DeletedFlashcardcourses
  protected val slsCourseInCourseTerms: SLSCourseInCourseTerms
  protected val lessons: Lessons
  protected val hasRatedLessons: HasRatedLessons
  protected val attendances: Attendances
}

trait MapperModelDependencies extends ModelDependencies {
  override protected val currentUser = LdapCurrentUser
  override protected val terms = MapperTerms
  override protected val testExecutors = MapperTestExecutors
  override protected val statistics = MapperStatistics
  override protected val courses = MapperCourses
  override protected val coursePages = MapperCoursePages
  override protected val exercises =  MapperExercises
  override protected val exerciseSheets =  MapperExerciseSheets
  override protected val exerciseSheetEntries = MapperExerciseSheetEntries
  override protected val submissions = MapperSubmissions
  override protected val admins = MapperAdmins
  override protected val lecturers = MapperLecturers
  override protected val tutors = MapperTutors
  override protected val groups = MapperGroups
  override protected val userInGroups = MapperUserInGroups
  override protected val resourceFiles= MapperResourceFiles
  override protected val resourceFileForCourses=MapperResourceFileForCourses
  override protected val multipleCards= MapperMultipleCards
  override protected val cardBoxes= MapperCardBoxes
  override protected val answers= MapperAnswers
  override protected val duels= MapperDuels
  override protected val surveys = MapperSurveys
  override protected val flashcardCourses = MapperFlashcardCourses
  override protected val subscribedFlashcardCourses = MapperSubscribedFlashcardCourses
  override protected val learningMaterials = MapperLearningMaterials
  override protected val learningProfiles = MapperLearningProfiles
  override protected val deletedFlashcardcourses = MapperDeletedFlashcardcourses
  override protected val ontologys = MapperOntologys
  override protected val slsCourseInCourseTerms = MapperSLSCourseInCourseTerms
  override protected val lessons = MapperLessons
  override protected val hasRatedLessons = MapperHasRatedLessons
  override protected val attendances= MapperAttendances
}

trait NullModelDependencies extends ModelDependencies {
  override protected val testExecutors = null
  override protected val currentUser = null
  override protected val terms = null
  override protected val statistics = null
  override protected val courses = null
  override protected val coursePages = null
  override protected val exercises = null
  override protected val exerciseSheets = null
  override protected val exerciseSheetEntries = null
  override protected val submissions = null
  override protected val admins = null
  override protected val lecturers = null
  override protected val tutors = null
  override protected val groups = null
  override protected val userInGroups = null
  override protected val resourceFiles= null
  override protected val resourceFileForCourses=null
  override protected val multipleCards= null
  override protected val cardBoxes= null
  override protected val answers= null
  override protected val duels= null
  override protected val flashcardCourses = null
  override protected val learningMaterials = null
  override protected val learningProfiles = null
  override protected val subscribedFlashcardCourses = null
  override protected val deletedFlashcardcourses = null
  override protected val ontologys = null
  override protected val slsCourseInCourseTerms = null
  override protected val lessons = null
  override protected val hasRatedLessons = null
  override protected val attendances= null
}
