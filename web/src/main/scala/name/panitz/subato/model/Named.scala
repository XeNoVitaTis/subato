package name.panitz.subato.model

trait Named {
  def name:String
}
trait IdAndNamed extends Id with Named
