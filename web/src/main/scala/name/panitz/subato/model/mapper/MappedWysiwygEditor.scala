package name.panitz.subato.model.mapper

import scala.xml.XML
import java.io.FileOutputStream
import scala.xml.Text
import java.io.File
import net.liftweb.common.{ Box, Full, Empty }
import net.liftweb.http.{ SHtml, FileParamHolder }
import net.liftweb.mapper.{ MappedPoliteString, Mapper }
import scala.xml.{ Elem, Node }
// import net.liftweb.util.Log._

private[model] class MappedHTML[T <: Mapper[T]](owner: T, label:()=>String)
  extends MappedPoliteString[T](owner, 1000000) {
	
  override def asHtml: Node= try{
    XML.loadString("<div>" + (if (get==null) "" else get) + "</div>")
  }catch{
    case e:Exception => XML.loadString("<pre><![CDATA[" + (if (get==null) "" else get) + "]]></pre>")
  }

  def hasEntry = get != null && get != ""
  override def _toForm: Box[Elem] =
    Full(<div>{SHtml.textarea(get, this.set(_))}</div>)
}

private[model] class MappedWysiwygEditor[T <: Mapper[T]](owner: T, label:()=>String)
  extends MappedHTML[T](owner, label) {
	
  //display the editor
  //user mce only for elements with id == "mce1", do not use html entities except for those that are in the xml standard
  override def _toForm: Box[Elem] = Full(
    <div>
      <script type="text/javascript" src="/static/3dpartyjscript/tiny_mce/js/tinymce/tinymce.min.js"></script>
      <script type="text/javascript">
        <xml:unparsed>
      // <![CDATA[
      tinymce.init({
	width: 500,
	mode: "exact",
	elements: "mce1",
	// theme: "advanced",
        theme: "modern",
        plugins: [
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen",
    "insertdatetime media nonbreaking save table contextmenu directionality",
    "emoticons template paste textcolor colorpicker textpattern imagetools codesample toc"
  ],
        toolbar1: "undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent ",
        entity_encoding : "numeric",
	entities: "38,amp,34,quot,60,lt,62,gt"});
    // ]]>
        </xml:unparsed>
      </script>
      <label for="mce1">{label()}</label>{textarea}
    </div>)
	
  private def textarea = SHtml.textarea(get, this.set(_), "id" -> "mce1")
}
