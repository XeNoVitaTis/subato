package name.panitz.subato.model.mapper

import net.liftweb.mapper.MappedPoliteString
import net.liftweb.mapper.By
import net.liftweb.mapper.MappedEnum
import net.liftweb.mapper.MappedLongForeignKey
import net.liftweb.mapper.MappedBoolean
import net.liftweb.mapper.MappedString
import net.liftweb.mapper.MappedInt
import net.liftweb.mapper.LongKeyedMetaMapper
import net.liftweb.mapper.LongKeyedMapper
import net.liftweb.mapper.IdPK
import net.liftweb.common.{Full, Box}

import scala.util.Random
import name.panitz.subato.model.mapper.learnsystem.{LearningMaterial}

import scala.xml.Text

import name.panitz.subato.SWrapperL10n
import name.panitz.subato.XmlConfigDependency
import name.panitz.subato.model.MapperModelDependencies

object TopicsSplitter {
  def split(toSplit: String): Seq[String] = toSplit split "/"
  def concate(topics: Seq[String]): String = topics mkString "/"
}

object CardType extends Enumeration {
  type CardType = Value
  val MultipleChoice, FreeText, ValueList, ValueSelection, ClozeText, Programming = Value

  def withNameOpt(s: String): Option[Value] = values.find(_.toString.trim.toLowerCase == s.trim.toLowerCase)
}

object BloomsLevel extends Enumeration {
  type BloomsLevel = Value
  val Remember, Analyse, Evaluate, Create, Understand, Apply = Value

  def withNameOpt(s: String): Option[Value] = values.find(_.toString == s)
}

class MultipleCard
  extends LongKeyedMapper[MultipleCard]
    with SWrapperL10n
    with XmlConfigDependency
    with IdPK {

  def getSingleton = MultipleCard

  def this(cId:Long, cardType:String, text:scala.xml.NodeSeq, explanation:String, name:String, bloomsLevel:String, topics: Seq[String]){
    this()
    this.course.set(cId)
    this.exerciseText.set(text+"")
    this.cardType.set(CardType.withNameOpt(cardType) getOrElse CardType.MultipleChoice)
    this.bloomsLevel.set(BloomsLevel.withNameOpt(bloomsLevel) getOrElse BloomsLevel.Remember)
    this.explanation.set(explanation)
    this.name.set(name)
    this.topics.set(TopicsSplitter.concate(topics))
    this.salt.set(Random.alphanumeric.take(20).mkString)
  }

  def update(cId:Long, cardType:String, text:scala.xml.NodeSeq, explanation:String, name:String, bloomsLevel:String, topics: Seq[String]): Unit = {
    this.course.set(cId)
    this.exerciseText.set(text+"")
    this.cardType.set(CardType.withNameOpt(cardType) getOrElse CardType.MultipleChoice)
    this.bloomsLevel.set(BloomsLevel.withNameOpt(bloomsLevel) getOrElse BloomsLevel.Remember)
    this.explanation.set(explanation)
    this.name.set(name)
    this.topics.set(TopicsSplitter.concate(topics))
    this.save()
  }

  object name extends MappedPoliteString(this, 100) {
    override def displayName = locStr("name")
    override def dbIndexed_? = true
    override def asHtml = Text(get)
  }

  object author extends MappedPoliteString(this, 100) {
    override def displayName = "author"
    override def asHtml = Text(get)
    override def defaultValue = ""
  }

  object topics extends MappedPoliteString(this, 1000) {
    override def displayName = "Topics"
    override def defaultValue = ""
    override def asHtml = Text(get)
  }

  object salt extends MappedPoliteString(this, 20) {

  }

  object course extends MappedLongForeignKey(this, Course) {
    override def dbIndexed_? = true
    override def displayName = locStr("class")

    override def validSelectValues = {
      _root_.name.panitz.subato.model.LdapCurrentUser.uid.map(user =>
        for (iti <- IsLecturerIn.findAll(By(IsLecturerIn.uid, user)))	yield {
          val courseId = iti.course.get;
          val Full(name) = Course.find(courseId).map(_.name.get)
          (courseId, name)
        }
      )
    }
  }
                             
  object explanation extends MappedHTML(this, ()=> locStr("explanation")) {
  }
   
  object exerciseText extends MappedHTML(this, ()=>locStr("exerciseText")) {
  }


  object cardType extends MappedEnum(this, CardType) {
    override def defaultValue = CardType.MultipleChoice
  }

  object bloomsLevel extends MappedEnum(this, BloomsLevel) {
    override def defaultValue = BloomsLevel.Remember
  }

  object isSurveyCard extends MappedBoolean(this)  {
    override def defaultValue = false
  }

  //only used for programming cards
  object exercise extends MappedLongForeignKey(this, Exercise)

  override def delete_! = {
    Answer findAll By(Answer.card,id.get) foreach { _.delete_!}
    CardBoxEntry findAll By(CardBoxEntry.multipleCard,id.get) foreach { _.delete_!}
    LearningMaterial findAll By(LearningMaterial.flashcard, id.get) foreach { _.delete_! }

    val ex = exercise.get
    val result = super.delete_!
    if (result && cardType == CardType.Programming)
      Exercise.find(ex).openOrThrowException("").delete_!
    result
  }
}


import net.liftweb.http.RedirectResponse
import net.liftweb.sitemap.Loc.If

object MultipleCard extends MultipleCard
  with LongKeyedMetaMapper[MultipleCard]
  with LecturerOnlyCRUDify[Long, MultipleCard] {

  override def editMenuLocParams =  super.editMenuLocParams
  override def showAllMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login")) :: super.showAllMenuLocParams
  override def createMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login"))  :: super.createMenuLocParams
  override def deleteMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login"))  :: super.deleteMenuLocParams

  def currentUser = _root_.name.panitz.subato.model.LdapCurrentUser
  private def deleteEditAllowed(b:Box[MultipleCard]) = b match {
    case Full(e) =>
      var x ={
        currentUser.uid match {
          case Full(n) =>
            n==e.author.get
          case _ =>
            false
        }
      }
      x|| User.isLecturerIn(e.course.get)
    case _ => false
  }


}


class Answer  extends LongKeyedMapper[Answer]
  with SWrapperL10n
  with IdPK {

  def getSingleton = Answer

  def this(cId:Long, n:Int, text:String, c:Boolean){
    this()
    this.setFields(cId, n, text, c)
  }

  def update(cId:Long, n:Int, text:String, c:Boolean): Unit = {
    this.setFields(cId, n, text, c)
    this.save
  }

  def setFields(cId:Long, n:Int, text:String, c:Boolean): Unit = {
    this.card.set(cId)
    this.number.set(n)
    this.text.set(text)
    this.isCorrect.set(c)
  }

  object card extends MappedLongForeignKey(this, MultipleCard) {
    override def dbIndexed_? = true
    override def validSelectValues = {
      Full(MultipleCard.findAll.map((c)=> (c.id.get, c.exerciseText.get)))
    }
  }

  object number extends MappedInt(this){
    override def defaultValue = 1
  }

  object isCorrect extends MappedBoolean(this){
  }

  object text extends MappedHTML(this, ()=>locStr("text")) {
  }
}

object Answer extends  Answer
  with LongKeyedMetaMapper[Answer]
  with LecturerOnlyCRUDify[Long, Answer]
  with MapperModelDependencies{

  override def editMenuLocParams =  super.editMenuLocParams
  override def showAllMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login")) :: super.showAllMenuLocParams
  override def createMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login"))  :: super.createMenuLocParams
  override def deleteMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login"))  :: super.deleteMenuLocParams

  private def deleteEditAllowed(b:Box[Answer]) = b match {
    case Full(e) =>
      var c = multipleCards.find(e.card.get).openOrThrowException("")
      var x ={
        currentUser.uid match {
          case Full(n) =>
            n == c.author
          case _ =>
            false
        }
      }
      x|| User.isLecturerIn(c.course.id)
    case _ => false
  }


}


class CardBox
  extends LongKeyedMapper[CardBox]
    with SWrapperL10n
    with XmlConfigDependency
    with IdPK {

  def getSingleton = CardBox

  def this(cId:Long, uid:String, name:String, description:String){
    this()
    this.course.set(cId)
    this.name.set(name)
    this.uid.set(uid)
    this.description.set(description)
    this.salt.set(Random.alphanumeric.take(20).mkString)
  }

  def update(newName: String, newDescription: String): Unit = {
    this.name set newName
    this.description set newDescription
    this.save()
  }

  object uid extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
    override def _toForm = Full(<span>{this.get}</span>    )
  }

  object salt extends MappedPoliteString(this, 20) {

  }

  object name extends MappedPoliteString(this, 100)

  object description extends MappedPoliteString(this, 300) {
    override def displayName = locStr("description")
  }

  object course extends MappedLongForeignKey(this, Course) {
    override def dbIndexed_? = true
    override def displayName = locStr("class")

    override def validSelectValues = {
      _root_.name.panitz.subato.model.LdapCurrentUser.uid.map(user =>
        for (iti <- IsLecturerIn.findAll(By(IsLecturerIn.uid, user)))	yield {
          val courseId = iti.course.get;
          val Full(name) = Course.find(courseId).map(_.name.get)
          (courseId, name)
        }
      )
    }
  }

  object isPrivate extends MappedBoolean(this){
    override def defaultValue = true
  }

}


object  CardBox extends CardBox
  with LongKeyedMetaMapper[CardBox]
  with LecturerOnlyCRUDify[Long, CardBox] {

  override def editMenuLocParams =  super.editMenuLocParams
}



class CardBoxEntry extends LongKeyedMapper[CardBoxEntry]
  with IdPK
  with MapperModelDependencies
  with SWrapperL10n
{
  def getSingleton = CardBoxEntry

  def this(boxId:Long, cardId:Long){
    this()
    this.cardBox.set(boxId)
    this.multipleCard.set(cardId)
  }


  object cardBox extends MappedLongForeignKey(this, CardBox) {
    override def dbIndexed_? = true

    override def validSelectValues
    = currentUser.uid.map(user =>
      (for (iti <- IsLecturerIn.findAll(By(IsLecturerIn.uid, user)))
        yield {
          val courseId = iti.course.get;
          CardBox.findAll.filter((x)=>x.course.get==courseId).map(x => (x.id.get, x.name.get+" ()"))
        }).flatten)

    override def asHtml() = {
      Text(CardBox.find(By(CardBox.id, this.get))  map {(x)=>x.name.get } openOr "")
    }

    def courseID = CardBox.find(By(CardBox.id, this.get)) map {_.course.get} openOr -1
  }

  object multipleCard  extends MappedLongForeignKey(this, MultipleCard) {
    override def dbIndexed_? = true
    override def validSelectValues
    = Full(MultipleCard
      .findAll
      .map(x => (x.id.get, x.exerciseText.get.substring(0,Math.min(30,x.exerciseText.get.length))+"" ))
    )

  }

}

object CardBoxEntry extends CardBoxEntry
  with LongKeyedMetaMapper[CardBoxEntry]
  with LecturerOnlyCRUDify[Long, CardBoxEntry] {


}
