package name.panitz.subato.model.learnsystem

import java.time.{LocalDateTime => LDT}

import FinalGrade._
import name.panitz.subato.model.{Finder, Id, MapperModelDependencies, Card}
import name.panitz.subato.model.mapper.learnsystem.{FlashcardCourseDateFormatter => FCDF, MasteryLevel, SubscribedFlashcardCourse => SFCC}
import name.panitz.subato.model.mapper.learnsystem.MasteryLevel._
import net.liftweb.common.{Box, Full, Empty}
import net.liftweb.mapper.By

case class EphemeralSubscribedFlashcardCourse(val courseId: Long) extends SubscribedFlashcardCourse with MapperModelDependencies {

  var grade: FinalGrade                          = NULL
  val materials: Seq[SubscribedLearningMaterial] = allMaterials_
  var isTrainingCompleted_? : Boolean            = false
  var userMustBeCongratulated: Boolean           = false
  var courseCompleted: Boolean                   = false
  var examCompleted: Boolean                     = false
  var inExam: Boolean                            = false

  override def id: Long = 0

  override def finalGrade: FinalGrade = grade

  override def toFlashcardCourse: Box[(FlashcardCourse, SubscribedFlashcardCourse)] = {
    flashcardCourses find courseId map { (_, this) }
  }

  override def flashcardCourseId: Long = courseId

  override def isCompleted_? : Boolean = courseCompleted

  override def isLinkedTo(course: FlashcardCourse): Boolean = course.id == courseId

  private def allMaterials_ : Seq[SubscribedLearningMaterial] = {
    learningMaterials
      .of (courseId)
      .map { EphemeralSubscribedLearningMaterial(_) }
  }

  override def allMaterials: Seq[SubscribedLearningMaterial] = materials

  override def material(cardId: Long): Box[SubscribedLearningMaterial] = {
    materials find {
      _.learningMaterial match {
        case Full(lm) => lm.flashcardId == cardId
        case _        => false
      }
    }
  }

  override def nextMaterialsToLearn: Seq[SubscribedLearningMaterial] = {
    if (isCompleted_?) {
      materials
    } else {
      isInExamModus_? match {
        case true => examMaterials
        case _    => if (trainingsMaterials.isEmpty){
          inExam = true
          examMaterials
        }else{ trainingsMaterials filter { _.masteryLevel != End } }
      }
    }
  }

  override def ownerId: String = ""

  def isInExamModus_? : Boolean = inExam

  private def examMaterials: Seq[SubscribedLearningMaterial] = {
    materials.foldRight (Seq.empty[SubscribedLearningMaterial]) { case (next, result) =>
      {
        if (next.isExamMaterial_?) next +: result else result
      }
    }
  }

  private def trainingsMaterials: Seq[SubscribedLearningMaterial] = {
    materials.foldRight (Seq.empty[SubscribedLearningMaterial]) { case (next, result) =>
      {
        if (!next.isExamMaterial_?) next +: result else result
      }
    }
  }

  override def lastLearningSession: LDT = LDT.now
  
  override def userHasBeenCongratulated(): Unit = userMustBeCongratulated = false

  private def markCompletionByNeed(): Unit = {
    if (trainingHasJustBeenCompleted_?) {
      markTrainingCompletion()
      tryToMarkCourseAsCompleted()
    } else if (examCanBeEvaluated_?) {
      evaluateExamResults()
    }
  }

   private def markTrainingCompletion(): Unit = {
    isTrainingCompleted_? = true
  }

  override def userMustBeCongratulated_? : Boolean = {
    markCompletionByNeed()
    userMustBeCongratulated
  }

  private def trainingHasJustBeenCompleted_? : Boolean = {
    (!isTrainingCompleted_?) && areAllMaterialsLearned_?
  }

  private def areAllMaterialsLearned_? : Boolean = {
    trainingsMaterials forall { x => x.masteryLevel == End }
  }

  private def examCanBeEvaluated_? : Boolean = {
    isInExamModus_? &&
    examMaterials.forall { _.masteryLevel != NotExaminedYet }
  }

  private def tryToMarkCourseAsCompleted(): Unit = {
    if (courseHasExam_?) {
      inExam = true
    } else {
      courseCompleted         = true
      examCompleted           = true
      userMustBeCongratulated = true
      grade                   = GRADE_A
    }
  }

  def courseHasExam_? : Boolean = materials exists { _.isExamMaterial_? }

  private def evaluateExamResults(): Unit = {
    val em = examMaterials
    val correctlyAnsweredCards = em filter { _.masteryLevel == End }
    val score = 100.0 * (correctlyAnsweredCards.size.toDouble / em.size)
    if (score >= 50) {
      courseCompleted         = true
      examCompleted           = true
      userMustBeCongratulated = true
      inExam                  = false
      grade                   = MapperCompletedFlashcardCourses.toGradeAndStars(score)._1
    } else {
      correctlyAnsweredCards foreach { _.degradeMasteryLevel }
    }
  }


}


case class EphemeralSubscribedLearningMaterial(val material: LearningMaterial) extends SubscribedLearningMaterial {

  var level: MasteryLevel = if (material.isExamMaterial_?) NotExaminedYet else One

  override def id: Long = -1
   
  override def learningMaterial: Box[LearningMaterial] = Full(material)

  override def learningMaterialId: Long = material.id

  override def masteryLevel: MasteryLevel = level

  override def degradeMasteryLevel: Unit = {
    def isProgrammingMaterial_? = material.flashcard map { _.isProgrammingCard_? } getOrElse false

    if (material.isExamMaterial_?) {
      level = ExamFailed
    } else if ((!learningCompleted_?) && (!isProgrammingMaterial_?)) {
      level = One
    }
  }

  override def upgradeMasteryLevel: Unit = {
    if (material.isExamMaterial_?) {
      level = End
    } else {
      level = level match {
        case One   => Two
        case Two   => Three
        case Three => Four
        case Four  => Five
        case _     => End
      }
    }
  }

}
