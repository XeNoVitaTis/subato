package name.panitz.subato.model.mapper

import net.liftweb.mapper.MappedPoliteString
import net.liftweb.http.UnauthorizedResponse
import net.liftweb.mapper.By
import net.liftweb.http.RedirectResponse
import net.liftweb.sitemap.Loc.If
import net.liftweb.sitemap.Loc
import net.liftweb.sitemap.Menu
import net.liftweb.common.Full
import net.liftweb.mapper.MappedTextarea
import net.liftweb.mapper.MappedString
import net.liftweb.mapper.MappedInt
import net.liftweb.mapper.LongKeyedMetaMapper
import net.liftweb.mapper.CRUDify
import net.liftweb.mapper.LongKeyedMapper
import net.liftweb.mapper.IdPK

class Term
    extends LongKeyedMapper[Term]
    with IdPK {

  def getSingleton = Term

  object year extends MappedInt(this){
//    override def displayName = locStr("attempts")		
    override def dbIndexed_? = true
  }

  object  addendum extends MappedPoliteString(this, 100) {
    //override def dbIndexed_? = true
  }


//  def exercises = Exercise.findAll(By(Exercise.course, id.get))
//  def exerciseSheets = ExerciseSheet.findAll(By(ExerciseSheet.course, id))
}

object Term extends Term
    with LongKeyedMetaMapper[Term]
    with AdminOnlyCRUDify[Long, Term] {
	
/*  private def deleteIsLecturerIn(c:Course) {
    IsLecturerIn.bulkDelete_!!(By(IsLecturerIn.course, c))
  }
  private def unsetCourseInExercises(c:Course) {
    for (e <- c.exercises) {
      e.course(-1).save
    }
  }
  override def beforeDelete = deleteIsLecturerIn _ :: unsetCourseInExercises _ :: super.beforeDelete 
 */
}
