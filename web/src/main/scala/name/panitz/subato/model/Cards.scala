package name.panitz.subato.model

import net.liftweb.common._
import java.util.Date

import net.liftweb.mapper._
import name.panitz.subato.model.mapper.{BloomsLevel, CardType, Answer => MA, CardBox => MCB, MultipleCard => MC, TopicsSplitter}
import name.panitz.subato.snippet.{AbstractCardsSnippet, AbstractDuelSnippet, AbstractSlsCourseSnippet, AbstractCardSnippet}
import name.panitz.subato._
import name.panitz.subato.model.Progress._

import net.liftweb.util.Helpers._
import scala.xml.{Elem, Node, NodeSeq}

trait MultipleCards extends Finder[Card] {
  def create(cId:Long, cardType:String, text:scala.xml.NodeSeq, explanation:String, name:String, bloomsLevel:String, topic: Seq[String]):Card
  def mapToModel(c:mapper.MultipleCard):Card
  def findDuelForCourse(cid:Long):List[Card]
  def courseHasEnoughDuelCards(cid:Long):Boolean
}

object Util {
  def children(x:Node):NodeSeq=x.child
  def trimDivHTML(html:Node) = if (html.label=="div") children(html) else html
}



object MapperMultipleCards extends MultipleCards  with MapperModelDependencies {
  override def findAll = MC.findAll.map( mapToModel(_))
  override def find(id:Long) = MC.find(id).map( mapToModel(_))
  import mapper.CardType._
  override def findDuelForCourse(cid:Long)
  = scala.util.Random.shuffle(MC.findAll(By(MC.course,cid),NotBy(MC.cardType,FreeText),NotBy(MC.isSurveyCard,true)).take(15).map(mapToModel(_)))


  override def courseHasEnoughDuelCards(cid:Long):Boolean=MC.count(By(MC.course,cid),NotBy(MC.cardType,FreeText))>15

  override def mapToModel(c:mapper.MultipleCard): Card = {
    c.cardType.get match{
      case mapper.CardType.MultipleChoice => new MapperMultipleCard(c)
      case mapper.CardType.ValueList => {new MapperItemListCard(c)}
      case mapper.CardType.FreeText =>  {new MapperFreeTextCard(c)}
      case mapper.CardType.ValueSelection => {new MapperValueSelectionCard(c)}
      case mapper.CardType.ClozeText => {new MapperClozeTextCard(c)}
      case mapper.CardType.Programming => {new MapperProgrammingCard(c)}
    }
  }

  override def create(cId:Long, cardType:String, text:NodeSeq, explanation:String, name:String, bloomsLevel:String, topics: Seq[String]): Card = {
    mapToModel( new MC(cId, cardType, text, explanation, name, bloomsLevel, topics))
  }

}

trait Submittable {
  def submitted(snip:AnswerHandler): Any
}

trait Card extends IdAndNamed with Submittable{
  def initialize():Unit
  def id:Long
  def cardType: String
  def cardTypeCatsString:String
  def isProgrammingCard_? : Boolean
  def answers:List[Answer]
  def genericAnswers:List[Answer]
  def exerciseText:NodeSeq
  def genericExerciseText:NodeSeq
  def explanation:NodeSeq
  def genericExplanation:NodeSeq
  import mapper.BloomsLevel._
  def bloomsLevel: BloomsLevel
  def bloomsLevelCatsString:String
  def exerciseId:Long
  def exercise:Exercise
  def exerciseId_=(b:Long)
  def isSurveyCard:Boolean
  def isSurveyCard_=(b:Boolean)
  def topics: Seq[String]
  def ontology: Seq[Ontology]
  def course:Course
  def snippet(snip:AnswerHandler,cardBoxId:Long):net.liftweb.util.CssSel
  def resultSnippet(snip:AbstractCardSnippet,cardBoxId:Long):net.liftweb.util.CssSel
  def complainMailTo(snip: AbstractCardSnippet, lc:Card,correctAnswer: String ):net.liftweb.util.CssSel
  def duelResultSnippet(card:Card,snip:AbstractDuelSnippet,duelId:Long,nextPage:String):net.liftweb.util.CssSel
  def explanationSnippet():net.liftweb.util.CssSel
  def env(): Map[String,Any]
  def exist_?(): Boolean
  def save()
  def update(cId:Long, cardType:String, text:NodeSeq, explanation:String, name:String, bloomsLevel:String, topics: Seq[String])
  def asXML:scala.xml.Node
  def asCatsXML:scala.xml.Node
  def salt: String
  def toQTI:scala.xml.Node
  def delete()
  def deleteAnswers()
  def isDuelCard:Boolean
  def author: String
  def setAuthor(name: String): Unit

  def editUrl:String
  def previewUrl:String

  def isGeneric:Boolean

}

abstract class AbstractCard(s:MC) extends Card with SWrapperL10n{
  override def id = s.id.get

  override def name = s.name.get

  override def author = s.author.get

  override def setAuthor(name: String): Unit = {
    if (author.isEmpty) s.author(name)
  }

  override def cardType: String = s.cardType.get.toString
  override def cardTypeCatsString: String = s.cardType.get match{
      case CardType.MultipleChoice =>"SingleChoice" 
      case CardType.ValueList =>"ValueEntry" 
      case CardType.FreeText =>  "TextEntry"
      case CardType.ValueSelection =>"MultipleChoice" 
      case CardType.ClozeText => "ClozeText"  //not supported????
      case CardType.Programming => "Programming" 
    }



  override def isProgrammingCard_? = s.cardType.get == CardType.Programming

  override def bloomsLevel = s.bloomsLevel.get
  override def bloomsLevelCatsString= bloomsLevel match{
    case BloomsLevel.Remember  =>"Knowledge"
    case BloomsLevel.Analyse   =>"Analyse"
    case BloomsLevel.Evaluate  =>"Evaluate"
    case BloomsLevel.Create    =>"Synthesis"
    case BloomsLevel.Understand=>"Comprehension"
    case BloomsLevel.Apply     =>"Application"
  }



  override def exerciseId = s.exercise.get

  def initialize={
    newEnv();
    val trans = ScalaEvaluator.rewrite(env)
    try{
      val rewritten = trans transform s.exerciseText.asHtml
      theExerciseText = rewritten
    }catch{
      case _:java.util.NoSuchElementException => theExerciseText = s.exerciseText.asHtml
    }
    try{
      val rewritten = trans transform s.explanation.asHtml
      theExplanation = rewritten
    }catch{
      case _:java.util.NoSuchElementException => theExerciseText = s.exerciseText.asHtml
    }

    theAnswers = for (ans <- genericAnswers) yield {
      try{
        ans.setText(<div>{trans transform ans.genericText}</div>);
      }catch{
        case e:java.util.NoSuchElementException => println(e)
      }
      ans
    }
  }

  override def genericExerciseText:NodeSeq=s.exerciseText.asHtml
  var theExerciseText:NodeSeq = null
  override def exerciseText ={
    if (theExerciseText==null){ initialize()}
    theExerciseText
  }

  override def genericExplanation:NodeSeq=s.explanation.asHtml
  var theExplanation:NodeSeq = null
  override def explanation ={
    if (theExplanation==null){ initialize()}
    theExplanation
  }


  override def genericAnswers=MA.findAll(By(mapper.Answer.card,s.id.get)).map(new MapperAnswer( _ ) )
  var theAnswers:List[Answer]=null
  override def answers= {
    if(theAnswers==null) initialize()
    theAnswers
  }


  var theEnv: Map[String,Any]=null
  def newEnv(): Map[String,Any]={
    var res = scala.collection.mutable.Map[String,Any]();
    try{
      for (v <- genericExerciseText \\ "var"){
        val mi = Integer.parseInt(v.attribute("min").get.toString)
        val ma = Integer.parseInt(v.attribute("max").get.toString)
        res+= (v.attribute("name").get.toString -> (scala.util.Random.nextInt(ma-mi)+mi))
      }
    }catch{
      case e: Throwable => println("Got some other kind of exception: "+e)
    }
    val vars = res.toMap
    for (v <- genericExerciseText \\ "result"){
      res+= (v.attribute("name").get.toString -> (ScalaEvaluator.evaluate(v.attribute("definition").get.text,vars)))
    }
    theEnv = res.toMap
    theEnv
  }

  //it is important that this is done after the default initialization of the fields
  //but it will force initialization everywhere. Can we do without?????
  //Where was the problem. Seems to work better on demand.
  //initialize()


  override def env(): Map[String,Any]={
    if (theEnv == null){
      newEnv
    }
    theEnv
  }

  override def exercise:Exercise = MapperExercises.find(s.exercise.get).openOrThrowException("")

  override def course:Course = MapperCourses.find(s.course.get).openOrThrowException("")
  //  override def explanation = s.explanation.asHtml
  override def salt: String = s.salt.get
  override def save() = s.save

  override def isSurveyCard = s.isSurveyCard.get

  override def exist_?(): Boolean = {
    try {
      (MapperMultipleCards.find(id): Option[_]).nonEmpty
    } catch {
      case _: Exception => false
    }
  }

  override def submitted(snip:AnswerHandler): Any = {
    if(! exist_?) snip.abort("/")
  }

  override def update(cId:Long, cardType:String, text: NodeSeq, explanation:String, name:String, bloomsLevel:String, topics: Seq[String]): Unit = {
    s.update(cId, cardType, text, explanation, name, bloomsLevel, topics)
  }

  override def topics: Seq[String] = ontology.map(_.name)
    //TopicsSplitter.split(s.topics.get) filter { _.nonEmpty }

  override def ontology: Seq[Ontology] = mapper.OntologyEntry.findAll(
      By(mapper.OntologyEntry.flashcard, id)
     ).map(entry =>(MapperOntologys.find(entry.ontology.get) openOrThrowException "" ))

  override def deleteAnswers(): Unit = MA.findAll(By(MA.card, id)).foreach(_.delete_!)

  def statistic: StatisticData = MapperStatistics.findCardStatistic(id)
  def maybeExercise: Box[Exercise] = MapperExercises.find(s.exercise.get)
  def exerciseNode: NodeSeq = if (!maybeExercise.isEmpty) maybeExercise.openOrThrowException("").asXML else NodeSeq.Empty
  def exerciseCatsNode: NodeSeq = if (!maybeExercise.isEmpty) maybeExercise.openOrThrowException("").asXML else NodeSeq.Empty
  def exerciseOrQuestionNode:NodeSeq = {
    val exerciseN = exerciseNode
    if(exerciseN.nonEmpty) <question></question> +: exerciseN else <question>{genericExerciseText}</question>
  }


  override def complainMailTo(snip: AbstractCardSnippet, lc:Card,correctAnswer: String ) ={
    var text = "Hallo,\ndie folgende Lernkarte in Subato scheint mir Fehlerhaft:\n\nFrage:\n"+
      lc.name+"\n\n"+lc.exerciseText+"\n\n\nLösung:\n"+correctAnswer+"\n\n\n"
    var encoded=java.net.URLEncoder.encode(text, "UTF-8").replace("+", "%20")
    var subject="Problem mit Lernkarte "+lc.id+": "+lc.name+" in Subato"
    var encodedSubject=java.net.URLEncoder.encode(subject, "UTF-8").replace("+", "%20")
    "#complain" #> <a href={"mailto:flashcards@subato.org?subject="+encodedSubject+"&body="+encoded}>Lernkarte als fehlerhaft melden</a>
  }

  override def asXML =
  <card bloomsLevel={bloomsLevel.toString}  type={s.cardType.get.toString}  uid={id.toString} sec={SecureExportImport.hash(id.toString, s.salt.get)}>
  <name>{name}</name>
  <author>{author}</author>
  <topics>{ontology.map(x => <topic>{x}</topic>)}</topics>
  <statistic goodAnswers={statistic.goodAnswers+""}
      almostCorrectAnswers={statistic.almostCorrectAnswers+""}
      wrongAnswers={statistic.wrongAnswers+""}
      averageTime={statistic.averageSolutionTime+""}
  />
  {if (isGeneric) <env>{for (xy <- env) yield <entry k={""+xy._1} v={""+xy._2}></entry>}</env>}
  {exerciseOrQuestionNode}
      <answers>
        {for (a<-genericAnswers) yield a.asXML}
      </answers>
    <explanation>{genericExplanation}</explanation>
  </card>


  override def asCatsXML = {
    import scala.xml.Text
    val metadata = 
      <metadata bloomsLevel={bloomsLevelCatsString}>
        <author>{author}</author>
        <ontologies>{ontology.map(x => <ontology>{x}</ontology>)}</ontologies>
        <contributors></contributors>
        <publisher>subato.org</publisher>
        <itemStatus></itemStatus>
        <twins></twins>
      </metadata>

    val exerciseN = exerciseCatsNode
    if(exerciseN.nonEmpty)
    <exercise id  = {id.toString}
              lang= {exercise.language.toString}
              extension= {exercise.fileExtention}
              correctAnswers={statistic.goodAnswers+""}
              wrongAnswers={statistic.wrongAnswers+""}
              averageTime={statistic.averageSolutionTime+""}
    >
      <name>{name}</name>
      {metadata}
      <question>{exercise.exerciseText}</question>
      <solutionFQCN>{exercise.solutionFQCN}</solutionFQCN>
      <solutionTemplate>{exercise.solutionTemplate}</solutionTemplate>
      <solution>{exercise.solution}</solution>
      {exercise.getTestFiles()}
      <explanation>{genericExplanation}</explanation>
    </exercise>
    else
    <item id  = {id.toString}
          type= {cardTypeCatsString}
          correctAnswers={statistic.goodAnswers+""}
          wrongAnswers={statistic.wrongAnswers+""}
          averageTime={statistic.averageSolutionTime+""}
    >
      <name>{name}</name>
      {metadata}
      {if (isGeneric) Seq(Text("\n      "), <varMap>{for (xy <- env) yield Seq(Text("\n        "), <entry key={""+xy._1} value={""+xy._2}></entry>)}
      </varMap>)}
      <question>{genericExerciseText}</question>
      <answers>{for (a<-genericAnswers) yield Seq(scala.xml.Text("\n        "),a.asCatsXML)}
      </answers>
      <explanation>{genericExplanation}</explanation>
    </item>
  }
  override def toQTI = <assessmentItem
    xmlns="http://www.imsglobal.org/xsd/imsqti_v2p1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.imsglobal.org/xsd/imsqti_v2p1 http://www.imsglobal.org/xsd/imsqti_v2p1.xsd"
    toolName="Subato"
    toolVersion="1.0"
    />

  override def exerciseId_= (exid:Long) {
    s.exercise.set(exid)
  }

  override def isSurveyCard_= (b:Boolean) {
    s.isSurveyCard.set(b)
  }


  override def delete()= {
    for (d <- MapperDuels.findContainsCard(id)){
      d.delete
    }
    MapperStatistics.deleteStatisticOf(id)
    s.delete_!
  }

  override def explanationSnippet():net.liftweb.util.CssSel = {
    "#explanation" #> {
      if (genericExplanation.text.trim.nonEmpty) {
        <div><b>{loc("explanation")}</b><div>{explanation}</div></div>
      } else {
        scala.xml.Text("")
      }
    }
  }
  override def isDuelCard:Boolean = true
  override def editUrl:String = "/multiplecard/edit/"+id
  override def previewUrl:String = "/ShowSingleCard/"+id

  override def isGeneric:Boolean = !((genericExerciseText \\ "result").isEmpty && (genericExerciseText \\ "var").isEmpty)
}

class MapperMultipleCard(s:MC) extends AbstractCard(s) with SWrapperL10n{

  override def toQTI = {
    val correctAnswers = answers.filter(_.isCorrect)
    val multipleString = if (correctAnswers.size > 1) "multiple" else "single"
    val partialScore = 1.0 / correctAnswers.size
    val identifier={"subatoCard"+id}
    <assessmentItem
    xmlns="http://www.imsglobal.org/xsd/imsqti_v2p1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.imsglobal.org/xsd/imsqti_v2p1 http://www.imsglobal.org/xsd/imsqti_v2p1.xsd"
    identifier={identifier}
    title={name}
    adaptive="false"
    timeDependent="false"
    toolName="Subato"
    toolVersion="1.0"
    >
      <responseDeclaration identifier="RESPONSE" cardinality={multipleString} baseType="identifier">
        <correctResponse>
          {
          for (answer<-correctAnswers) yield
            <value>{identifier+"choice"+answer.number}</value>
          }
        </correctResponse>
        <mapping lowerBound="0" upperBound="1" defaultValue="0">
          {
          for (answer<-answers) yield
              <mapEntry mapKey={identifier+"choice"+answer.number}
                        mappedValue={if (answer.isCorrect) partialScore+"" else -partialScore+""}/>

          }
        </mapping>
      </responseDeclaration>
      <outcomeDeclaration identifier="SCORE" cardinality="single" baseType="float">
        <defaultValue>
          <value>0.0</value>
        </defaultValue>
      </outcomeDeclaration>
      <outcomeDeclaration identifier="FEEDBACK" cardinality="single" baseType="identifier"/>
      <itemBody>
        {
        exerciseText
        }
        <choiceInteraction responseIdentifier="RESPONSE" shuffle="false" maxChoices={if (correctAnswers.size==1)"1"else "0"}>
          {
          for (answer<-answers) yield
            <simpleChoice identifier={identifier+"choice"+answer.number} fixed="false">
              {answer.text}
            </simpleChoice>
          }
        </choiceInteraction>
      </itemBody>
      <responseProcessing template="http://www.imsglobal.org/question/qti_v2p1/rptemplates/match_correct"/>
      <modalFeedback outcomeIdentifier="FEEDBACK" identifier={identifier+"ModalChoice0"} showHide="show">{explanation}</modalFeedback>
      <modalFeedback outcomeIdentifier="FEEDBACK" identifier={identifier+"ModalChoice1"} showHide="hide">{explanation}</modalFeedback>
    </assessmentItem>
  }

  var choosedAnswer: Option[Answer] = None

  import net.liftweb.http.SHtml
  override def snippet(snip: AnswerHandler,cardBoxId:Long) ={
    var as :List[String] = List()

    def submitted() = super.submitted(snip)

    def isSolutionCorrectSubmitted = {
      submitted()
      snip.solutionSubmitted(WordListSolution(as))
      snip.processResult(id,Correct)
    }

    def wrongSolutionSubmitted = {
      submitted()
      snip.solutionSubmitted(WordListSolution(as))
      snip.processResult(id,Incorrect)
    }

    "#selectionItem *" #>
      scala.util.Random.shuffle(
        answers.map(a =>
          SHtml.button( a.text
            , if (a.isCorrect) { () => {as = a.text.text :: as; choosedAnswer = Some(a); isSolutionCorrectSubmitted } }
            else { () => {as = a.text.text :: as; choosedAnswer = Some(a); wrongSolutionSubmitted }}
            , ("type","submit"), ("class", "btn")))) &
      "#question" #> exerciseText &
      "#name *" #> name
  }

  override def duelResultSnippet(lc:Card,snip:AbstractDuelSnippet,duelId:Long,nextPage:String) ={
    val correct = snip.lastResult.get
    val correctAnswer = answers find { _.isCorrect }
    lazy val Some(userAnswer) = choosedAnswer

    "#question *" #> {if (correct==Correct)
      loc("correctAnswer") else loc("incorrectAnswer")} &
      "#correctAnswer"#>  <div><b>{loc("theCorrectAnswer")}</b><div>{if (correctAnswer.isDefined) correctAnswer.get.text else scala.xml.Text("")}</div></div> &
      "#givenAnswer"#>  (if(!(correct == Correct)) <div><b>{loc("yourAnswer")}</b><div>{userAnswer.text}</div></div> else scala.xml.Text("")) &
      "#selectionItem *" #> List( <a href={nextPage}>{loc("goOn")}</a>) &
      explanationSnippet()//"#explanation *" #> <div><b>{loc("explanation")}</b><div>{lc.explanation}</div></div>
  }

  override def resultSnippet(snip: AbstractCardSnippet, cardBoxId: Long) ={
    val correct = snip.isSolutionCorrect_?
    val Some(lc) = snip.getLastCard
    val correctAnswer = answers find { _.isCorrect }
    lazy val Some(userAnswer) = choosedAnswer

    "#question *" #> {if (correct)
      loc("correctAnswer")
    else loc("incorrectAnswer")} &
      "#correctAnswer" #> <div><b>{loc("theCorrectAnswer")}</b><div>{if (correctAnswer.isDefined) correctAnswer.get.text else scala.xml.Text("")}</div></div> &
      "#givenAnswer"#> (if(!correct) <div><b>{loc("yourAnswer")}</b><div>{userAnswer.text}</div></div> else scala.xml.Text("")) &
      snip.setNextCardLink(snip.QUESTION_RELATIVE_URL + cardBoxId, loc("goOn").toString) &
      explanationSnippet() &//"#explanation *" #> <div><b>{loc("explanation")}</b><div>{lc.explanation}</div></div>
    complainMailTo(snip,lc,correctAnswer.get.text+"")
  }

  override def save() = s.save

}

class MapperProgrammingCard(val s:MC)
  extends AbstractCard(s)
    with SWrapperL10n
    with name.panitz.subato.XmlConfigDependency{

  override def name = this.exercise.name
  override def answers = List()
  override def exerciseText = this.exercise.exerciseText

  import net.liftweb.http.SHtml
  override def snippet(snip: AnswerHandler,cardBoxId:Long)= {

    def submitted(): Any = {
      //snip.redirectTo("/CardResult/"+cardBoxId)
      //snip.partialCorrectSolutionSubmitted
    }
    def textField(a:String)={
      super.submitted(snip)
      snip.solutionSubmitted(TextSolution(a))
      val submission = exercise.evaluator.testSolution(a,"",exercise,0,0,null)
      val correct = !submission.compilationFailed && (submission.errors+submission.failures == 0)
      snip.solutionSubmitted(ProgrammingSolution(submission))

//      FileUtil.deleteDir(new java.io.File(userDir))

      if (correct)
        snip.processResult(id, Correct)
      else snip.processResult(id, Incorrect)
    }

    "#selectionItem *" #> {
      val ex = exercise
      val supported = List("java","python","javascript","c","xml","cpp","html")
      val ll = ex.language.toString.toLowerCase
      val lang = if (supported.contains(ll))ll else "basic"
      List(
       <script language="javascript" type="text/javascript">
        editAreaLoader.init(
         {{id : "code"	                // textarea id
         ,syntax: "{lang}"	                // syntax to be uses for highglBiting
         ,start_highlight: true	        // to display with highlight mode on start-up
         ,allow_resize: "both"
         ,replace_tab_by_spaces: 2
         }}
        );
      </script>, SHtml.textarea(exercise.solutionTemplate, textField, "id" -> "code") , SHtml.button(loc("submit"), submitted, "class" -> "btn", "type" -> "submit", "onclick" -> "startSpinner()")) }&
      "#question" #> exercise.exerciseText&
      "#name *" #> name
  }

  override def duelResultSnippet(lc:Card,snip:AbstractDuelSnippet,duelId:Long,nextPage:String) ={
    val correct = snip.lastResult.get
    val Some(ProgrammingSolution(submission)) = snip.submission.>=
    val ex = lc.exercise

    "#question *" #> {if (correct==Correct)
      loc("correctAnswer") else loc("incorrectAnswer")} &
      "#correctAnswer *"#> <div><b>{loc("theCorrectAnswer")}</b><pre>{ex.solution}</pre></div> &
      "#givenAnswer *" #>  <div><b>{loc("yourAnswer")}</b><div><pre>{submission.solution}</pre></div></div> &
      "#selectionItem *" #>
        List( <a href={nextPage}>{loc("goOn")}</a>) &
      "#explanation *" #> <div>Details
        <div>{
          if (submission.compilationFailed){
            <div><b>Compilation failed</b>
              <div style="width: 100%; overflow-x: auto;"><pre>{submission.compilationOutput}</pre></div>
            </div>
          }else{
            <div>
              <b>Compilation successfull</b><br />
              <b>Test results</b><br />
              <b>Tests: </b> {submission.tests} <b>Failures: </b> {submission.failures} <b>Errors: </b> {submission.errors}<br />
              <!-- <div>{submission.unittests}</div> -->
            </div>
          }
          }</div>
        <div>{if (explanation.text.trim!="")
          <div><b>{loc("explanation")}</b><div>{explanation}</div></div> else scala.xml.Text("")}</div>
      </div>
  }

  override def resultSnippet(snip: AbstractCardSnippet, cardBoxId: Long) ={
    val correct = snip.isSolutionCorrect_?
    val Some(lc) = snip.getLastCard
    val Some(submission) = snip.getSubmission

    val ex = lc.exercise
    "#question *" #> {if (correct)
      loc("correctAnswer") else loc("incorrectAnswer")} &
      "#correctAnswer *"#> <div><b>{loc("theCorrectAnswer")}</b><pre class="line-numbers"><code class={"language-" +ex.brush}>{ex.solution}</code></pre></div> &
      "#givenAnswer *" #>  <div><b>{loc("yourAnswer")}</b><div><pre class="line-numbers"><code class={"language-" +ex.brush}>{submission.solution}</code></pre></div></div> &
      snip.setNextCardLink(snip.QUESTION_RELATIVE_URL + cardBoxId, loc("goOn").toString) &
      "#explanation *" #> <div>Details
        <div>{
          if (submission.compilationFailed){
            <div><b>Compilation failed</b>
              <div style="width: 100%; overflow-x: auto;"><pre>{submission.compilationOutput}</pre></div>
            </div>
          }else{
            <div>
              <b>Compilation successfull</b><br />
              <b>Test results</b><br />
              <b>Tests: </b> {submission.tests} <b>Failures: </b> {submission.failures} <b>Errors: </b> {submission.errors}<br />
              <!-- <div>{submission.unittests}</div> -->
            </div>
          }
          }</div>
        <div>{if (explanation.text.trim!="")
          <div><b>{loc("explanation")}</b><div>{explanation}</div></div> else scala.xml.Text("")}</div>
      </div>&
      complainMailTo(snip,lc,ex.solution)

  }

  override def editUrl:String = "/exercise/edit/"+exercise.id
}

class MapperFreeTextCard(val s:MC) extends AbstractCard(s) with SWrapperL10n{
  import net.liftweb.http.SHtml

  override def snippet(snip: AnswerHandler, cardBoxId: Long)= {

    def submitted(): Any = {
      super.submitted(snip)
      snip.processResult(id, Incorrect, false)
    }

    def textField(a: String) = {
      snip.solutionSubmitted(TextSolution(a))
    }

    "#selectionItem *" #>
      List(SHtml.textarea("", textField, "class" -> "form-control", "id" -> "textField"), SHtml.button(loc("submit"), submitted, "class" -> "btn", "type" -> "submit")) &
      "#question" #> exerciseText &
      "#name *"   #> name
  }

  override def duelResultSnippet(lc:Card,snip:AbstractDuelSnippet,duelId:Long,nextPage:String) ={
    val correct = snip.lastResult.get

    "#selectionItem *" #>
      List( <a href={nextPage}>{loc("goOn")}</a>) &
      "#explanation *" #> lc.explanation
  }

  override def resultSnippet(snip: AbstractCardSnippet, cardBoxId: Long) ={
    lazy val Some(rs) = snip.getPlayedCardResultIndex.get(cardBoxId)

    def resetResultIndex() = {
      snip.playedCardResultIndex(snip.getPlayedCardResultIndex + (cardBoxId -> (snip.getBox :: rs )))
      snip.eval
    }
    def correctSolution(): Any = {
      if (snip.canProcessCardResult_?) {
        snip.processResult(id, Correct, snip.QUESTION_RELATIVE_URL + cardBoxId)
      } else {
        snip.isSolutionCorrect(true)
        resetResultIndex
        MapperStatistics.incGoodAnswerNum(id, snip.computeNeededTime)
        snip.redirectTo(snip.QUESTION_RELATIVE_URL + cardBoxId)
      }
    }
    def incorrectSolution(): Any = {
      if (snip.canProcessCardResult_?) {
        snip.processResult(id, Incorrect, snip.QUESTION_RELATIVE_URL + cardBoxId)
      } else {
        snip.isSolutionCorrect(false)
        snip.isSolutionPartialCorrect(false)
        resetResultIndex
        MapperStatistics.incWrongAnswerNum(id, snip.computeNeededTime)
        snip.redirectTo(snip.QUESTION_RELATIVE_URL + cardBoxId)
      }
    }
    def partialCorrectSolution(): Any = {
      if (snip.canProcessCardResult_?) {
        snip.processResult(id, PartialCorrect, snip.QUESTION_RELATIVE_URL + cardBoxId)
      } else {
        snip.isSolutionCorrect(false)
        snip.isSolutionPartialCorrect(true)
        resetResultIndex
        MapperStatistics.incAlmostCorrectAnswerNum(id, snip.computeNeededTime)
        snip.redirectTo(snip.QUESTION_RELATIVE_URL + cardBoxId)
      }
    }

    val Some(lc) = snip.getLastCard
    "#item-container [style]" #> "" &
      "#selectionItem *" #>
        List( SHtml.button( loc("mySolutionWasCorrect") , correctSolution, "class" -> "btn", "type" -> "submit")
          , SHtml.button( loc("mySolutionWasIncorrect") , incorrectSolution, "type" -> "submit", "class" -> "btn")
          , SHtml.button( loc("mySolutionWasPartialCorrect") , partialCorrectSolution, "type" -> "submit", "class" -> "btn")
        ) &
      explanationSnippet() &
      "#correctAnswer *" #> <div><b>{loc("theCorrectAnswer")}</b><div>{lc.answers.head.text}</div></div> &
      "#givenAnswer *"   #> <div><b>{loc("yourAnswer")}</b><div>{snip.getTextAnswer}</div></div>&
      complainMailTo(snip,lc,lc.answers.head.text+"")

  }

  override def isDuelCard:Boolean = false

}


class MapperItemListCard(s:MC) extends AbstractCard(s)  with SWrapperL10n{
  import net.liftweb.http.SHtml
  override def snippet(snip: AnswerHandler,cardBoxId:Long)= {
    var as :List[String] = List()
    def submitted():Any = {
      super.submitted(snip)
      as = as.map((x) => x.trim.toLowerCase.replaceAll("\\s",""))
      val results =  answers
        .map((a) => a.text.text.trim.toLowerCase.replaceAll("\\s",""))
        .foldLeft(0)((r,a) => r + {if (as.contains(a)) 1 else 0})

      snip.solutionSubmitted(WordListSolution(as))

      if (results==answers.size) snip.processResult(id,Correct)
      else if (results!=answers.size && results>0) snip.processResult(id,PartialCorrect)
      else snip.processResult(id,Incorrect)
    }
    def addToList(a:String)={
      as = a +: as
    }

    "#selectionItem *" #>
      (scala.util.Random.shuffle(answers).map((a)=>SHtml.textarea("", addToList, ("style", "height: 28px"), "rows" -> "1", "class" -> "form-control form-control-sm singleRow"))
        :+ SHtml.button(loc("submit"),submitted, "type" -> "submit", "class" -> "btn"))&
      "#question" #> exerciseText&
      "#name *" #> name
  }

  override def duelResultSnippet(lc:Card,snip:AbstractDuelSnippet,duelId:Long,nextPage:String) ={
    val correct = snip.lastResult.get
    val Some(WordListSolution(ws)) = snip.submission.>=
    "#question *" #> {if (correct==Correct) loc("correctAnswer") else loc("incorrectAnswer")} &
      "#selectionItem *" #> List( <a href={nextPage}><div>{loc("goOn") }</div></a>) &
      explanationSnippet() &
      "#correctAnswer *"#> <div><b>{loc("theCorrectAnswer")}</b><div><ul>{lc.answers.map((a) => <li>{a.text}</li>)}</ul></div></div> &
      "#givenAnswer"#> (if (correct!=Correct) <div><b>{loc("yourAnswer")}</b><div><ul>{ws.map((a) => <li>{a}</li>)}</ul></div></div> else scala.xml.Text(""))
  }

  override def resultSnippet(snip: AbstractCardSnippet,cardBoxId:Long) ={
    val correct = snip.isSolutionCorrect_?
    val Some(lc) = snip.getLastCard
    val correctHTML =  <div><b>{loc("theCorrectAnswer")}</b><div><ul>{lc.answers.map((a) => <li>{a.text}</li>)}</ul></div></div>
    "#question *" #> {if (correct) loc("correctAnswer") else loc("incorrectAnswer")} &
      snip.setNextCardLink(snip.QUESTION_RELATIVE_URL + cardBoxId, loc("goOn").toString) &
      explanationSnippet() &
      "#correctAnswer *"#> correctHTML &
      "#givenAnswer"#> (if (!snip.isSolutionCorrect_?) <div><b>{loc("yourAnswer")}</b><div><ul>{snip.getTextAnswers.map((a) => <li>{a}</li>)}</ul></div></div> else scala.xml.Text(""))&
      complainMailTo(snip,lc,correctHTML+"")

  }

  override def toQTI={
    val identifier={"subatoCard"+id}
    val cardinalityString = if (answers.size > 1) "multiple" else "single"
    val partialScore = 1.0/answers.size
    <assessmentItem xmlns="http://www.imsglobal.org/xsd/imsqti_v2p1"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsi:schemaLocation="http://www.imsglobal.org/xsd/imsqti_v2p1 http://www.imsglobal.org/xsd/imsqti_v2p1.xsd"
                    identifier="textEntry"
                    title={name} adaptive="false" timeDependent="false"    toolName="Subato"
                    toolVersion="1.0">
      <responseDeclaration identifier="RESPONSE" cardinality={cardinalityString} baseType="string">
        <correctResponse>{
          for (answer<-answers)yield <value>{answer.text.text}</value>
          }</correctResponse>
        <mapping defaultValue="0"> {
          for (answer<-answers) yield <mapEntry mapKey={answer.text.text} mappedValue={partialScore+""} />
          } </mapping>
      </responseDeclaration>
      <outcomeDeclaration identifier="SCORE" cardinality="single" baseType="float"/>
      <outcomeDeclaration baseType="identifier" cardinality="single" identifier="FEEDBACK"/>
      <itemBody>
        {
        exerciseText
        }
        {
          <extendedTextInteraction responseIdentifier="RESPONSE" expectedLines="1" maxStrings={answers.size+""}/>
        }
      </itemBody>
      <responseProcessing
      template="http://www.imsglobal.org/question/qti_v2p1/rptemplates/map_response"/>
      <modalFeedback outcomeIdentifier="FEEDBACK" identifier={identifier+"choice0"} showHide="hide">{explanation}</modalFeedback>
    </assessmentItem>
  }
}


class MapperClozeTextCard(s:MC) extends AbstractCard(s) with SWrapperL10n{
  override def answers  = MA.findAll(By(mapper.Answer.card,s.id.get),OrderBy(mapper.Answer.number,Ascending)).map(new MapperAnswer( _ ) )

  import net.liftweb.http.SHtml
  override def snippet(snip: AnswerHandler,cardBoxId:Long)= {
    var as :List[String] = List()
    def submitted():Any = {
      super.submitted(snip)
      as = as.reverse.map(x => x.trim.toLowerCase)
      val abs = as.zip(answers.map(a => a.text.text.trim.toLowerCase))
      val results = abs.foldLeft(0)((r,ab) => r + {if (ab._1 == ab._2) 1 else 0})

      snip.solutionSubmitted(WordListSolution(as filter {! _.isEmpty}))

      if (results == answers.size) snip.processResult(id, Correct)
      else snip.processResult(id, Incorrect)

    }
    def addToList(a:String) = {
      as = a +: as
    }

    val cssT
    = answers
      .zipWithIndex
      .foldLeft("#xxxx *" #> "")((r,ai) =>  r & ("#cloze"+ai._2+" *") #> SHtml.text("", addToList, "rows" -> "1", "class" -> "cloze"))

    "#question *" #>  cssT.apply(exerciseText) &
      "#selectionItem *" #> SHtml.button(loc("submit"), submitted, "type" -> "submit", "class" -> "btn")&
      "#name *" #> name
  }

  override def duelResultSnippet(lc:Card,snip:AbstractDuelSnippet,duelId:Long,nextPage:String) = {
    val isCorrect_? = snip.lastResult.get == Correct
    val Some(WordListSolution(ws)) = snip.submission.>=

    "#question *" #> {if (isCorrect_?) loc("correctAnswer") else loc("incorrectAnswer")} &
      "#selectionItem *" #> List( <a href={nextPage}><div>{loc("goOn") }</div></a>) &
      explanationSnippet() &
      "#correctAnswer *"#> <div><b>{loc("theCorrectAnswer")}</b><div><ul>{lc.answers.filter((a) => a.isCorrect).map((a) => <li>{a.text}</li>)}</ul></div></div> &
      "#givenAnswer"#> (if (!isCorrect_?) <div><b>{loc("yourAnswer")}</b><div><ul>{ws.reverse.map((a) => <li>{a}</li>)}</ul></div></div> else scala.xml.Text(""))

  }

  override def resultSnippet(snip: AbstractCardSnippet,cardBoxId:Long) ={
    val isCorrect_? = snip.isSolutionCorrect_?
    val Some(lc) = snip.getLastCard
    val correctHTML= <div><b>{loc("theCorrectAnswer")}</b><div><ul>{lc.answers.filter((a) => a.isCorrect).map((a) => <li>{a.text}</li>)}</ul></div></div>
    "#question *" #> {if (isCorrect_?) loc("correctAnswer") else loc("incorrectAnswer")} &
      snip.setNextCardLink(snip.QUESTION_RELATIVE_URL + cardBoxId, loc("goOn").toString) &
      explanationSnippet() &
      "#correctAnswer *"#> correctHTML &
      "#givenAnswer"#> (if (!isCorrect_?) <div><b>{loc("yourAnswer")}</b><div><ul>{snip.getTextAnswers.reverse.map((a) => <li>{a}</li>)}</ul></div></div> else scala.xml.Text(""))&
      complainMailTo(snip,lc,correctHTML+"")

  }
}

class MapperValueSelectionCard(s:MC) extends MapperMultipleCard(s) with SWrapperL10n{
  import net.liftweb.http.SHtml
  override def snippet(snip: AnswerHandler,cardBoxId:Long)= {
    var correctAnsers = 0
    var as :List[String] = List()

    def submitted():Any = {
      super.submitted(snip)
      snip.solutionSubmitted(WordListSolution(as))
      val maxPoints = answers.count(_.isCorrect)//.filter(_.isCorrect).size
      if (correctAnsers==maxPoints){
        snip.processResult(id, Correct)
      }else if (correctAnsers > maxPoints/2){
        snip.processResult(id, PartialCorrect)
      }else{
        snip.processResult(id, Incorrect)
      }
    }

    def addToList(a:String)={
      //snip.textAnswers.set(a +: snip.textAnswers.get)
      as = a +: as
    }

    // snip.isSolutionCorrect.set(true)
    "#selectionItem *" #>
      ( scala.util.Random.shuffle(answers).map( (a) => <label class="selectionValue">{
        SHtml.checkbox(false,(c) => {
          if (c) addToList(a.text.text)
          if (c && a.isCorrect){correctAnsers = correctAnsers+1}
          if (c && !a.isCorrect){correctAnsers = correctAnsers-1}
          //if ((c&& !a.isCorrect) || (!c&&a.isCorrect)) snip.isSolutionCorrect.set(false)
        })
        }{a.text.flatMap(_.child)}</label>)
        :+ SHtml.button(loc("submit"), submitted, ("type", "submit"), ("class", "btn")))&
      "#question" #> exerciseText&
      "#name *" #> name
  }

  override def duelResultSnippet(lc:Card,snip:AbstractDuelSnippet,duelId:Long,nextPage:String) ={
    val correct = snip.lastResult.get
    val Some(WordListSolution(ws)) = snip.submission.>=
    "#question *" #> {if (correct==Correct) loc("correctAnswer")
    else if (correct==PartialCorrect)  loc("partiallyCorrectAnswer")
    else loc("incorrectAnswer")} &
      "#selectionItem *" #> List( <a href={nextPage}><div>{loc("goOn") }</div></a>) &
      explanationSnippet() &
      "#correctAnswer *"#> <div><b>{loc("theCorrectAnswer")}</b><div><ul>{lc.answers.filter((a) => a.isCorrect).map((a) => <li>{a.text}</li>)}</ul></div></div> &
      "#givenAnswer "#> (if (correct!=Correct) <div><b>{loc("yourAnswer")}</b><div><ul>{ws.map((a) => <li>{a}</li>)}</ul></div></div>  else scala.xml.Text(""))
  }


  override def resultSnippet(snip: AbstractCardSnippet, cardBoxId:Long) ={
    val correct = snip.isSolutionCorrect_?
    //snip.isSolutionCorrect(false)
    val Some(lc) = snip.getLastCard
    val correctHTML = <div><b>{loc("theCorrectAnswer")}</b><div><ul>{lc.answers.filter((a) => a.isCorrect).map((a) => <li>{a.text}</li>)}</ul></div></div>
    "#question *" #> {if (correct) loc("correctAnswer")
    else if (snip.isSolutionPartialCorrect_?)  loc("partiallyCorrectAnswer")
    else loc("incorrectAnswer")} &
      snip.setNextCardLink(snip.QUESTION_RELATIVE_URL + cardBoxId, loc("goOn").toString) &
      explanationSnippet() &
      "#correctAnswer *"#> correctHTML &
      "#givenAnswer "#> (if (!snip.isSolutionCorrect_?) <div><b>{loc("yourAnswer")}</b><div><ul>{snip.getTextAnswers.map((a) => <li>{a}</li>)}</ul></div></div>  else scala.xml.Text(""))&
      complainMailTo(snip,lc,correctHTML+"")

  }
}

trait Answers extends Finder[Answer] {
  def create(cId:Long, n:Int, text:String, c:Boolean):Answer

}

object MapperAnswers extends Answers with MapperModelDependencies {
  override def findAll = MA.findAll.map(new MapperAnswer(_))
  override def find(id:Long) = MA.find(id).map(new MapperAnswer(_))
  override def create(cId:Long, n:Int,text:String, c:Boolean):Answer=
    new MapperAnswer( new MA(cId,n,text,c))
}

trait  Answer  extends IdAndNamed {
  def id:Long
  def name:String
  def number:Int
  def text:Node
  def card:Card
  def setText(text:Node)
  def genericText:Node
  def isCorrect:Boolean
  def save(): Unit
  def asXML:Node
  def asCatsXML:Node
  def update(cId:Long, n:Int, text:String, c:Boolean): Unit
}

class MapperAnswer(private val s:MA) extends Answer with MapperModelDependencies{
  override def id: Long = s.id.get
  override def number = s.number.get
  override def name = ""
  override def card:Card=multipleCards.find(s.card.get).openOrThrowException("")
  override def text = {if(theText==null)theText = s.text.asHtml;theText}
  var theText:Node=null
  def setText(text:Node)= theText=text
  override def genericText = s.text.asHtml
  override def isCorrect = s.isCorrect.get
  override def save() = s.save
  override def asXML =
    ((x:NodeSeq) =>if (isCorrect)
      <answer correct="true">{x}</answer>
    else
      <answer>{x}</answer>
      )(Util.trimDivHTML(text))
  override def asCatsXML =
    ((x:NodeSeq) =>if (isCorrect)
      <answer correct="true"><text>{x}</text></answer>
    else
      <answer><text>{x}</text></answer>
      )(Util.trimDivHTML(text))

  override def update(cId:Long, n:Int, text:String, c:Boolean): Unit = s.update(cId, n, text, c)
}

trait CardBox extends IdAndNamed {
  def id:Long
  def name:String
  def uid:String
  def isPrivate:Boolean
  def isPublic:Boolean
  def description:String
  def course: Course
  def cards:List[Card]
  def isPrivate_=(b: Boolean)
  def salt: String
  def save()
  def update(newName: String, newDescription: String)
  def asXML:scala.xml.Node
  def delete()
  def toQTI:scala.xml.Node
  def hasEnoughDuelCards:Boolean
}


class MapperCardBox(private val s:MCB) extends CardBox with MapperModelDependencies{

  override def id = s.id.get
  override def name = s.name.get
  override def uid = s.uid.get
  override def isPrivate = s.isPrivate.get
  override def isPublic = !isPrivate && (lecturers.contains(uid, course) || admins.contains(uid))
  override def course=courses.find(s.course.get).openOrThrowException("")
  override def description = s.description.get
  override def salt: String = s.salt.get
  override def cards = {
    mapper
      .CardBoxEntry
      .findAll(By(mapper.CardBoxEntry.cardBox,s.id.get))
      .map((mce) => mapper.MultipleCard.find(mce.multipleCard.get))
      .map((x)=>multipleCards.mapToModel(x.openOrThrowException("")))
  }

  override def isPrivate_= (b:Boolean) {
    s.isPrivate.set(b)
  }

  override def hasEnoughDuelCards:Boolean= {
    val result = cards.filter(_.isDuelCard).size >= 15
    result
  }
  override def save() = s.save

  def update(newName: String, newDescription: String): Unit = {
    s.update(newName, newDescription)
  }

  override def asXML = <cardBox uid={id.toString} sec={SecureExportImport.hash(id.toString, s.salt.get)} update={true.toString}>
    <boxName>{name}</boxName>
    <description>{description}</description>
    <cards>{
      cards.map(_.asXML)
      }</cards>
  </cardBox>

  override def toQTI = <cardBox>
    <boxName>{name}</boxName>
    <description>{description}</description>
    <items>{
      cards.map(_.toQTI)
      }</items>
  </cardBox>


  override def delete(){
    cards.foreach((c) => c.delete)
    s.delete_!
  }
}

trait CardBoxes extends Finder[CardBox] {
  def create(cId:Long, uid:String, name:String, description:String):CardBox
  def findAllForCourse(cid:Long):List[CardBox]
  def findAllPublic:List[CardBox]
  def findAllForCourseAndUser(cid:Long,uiud:String):List[CardBox]
}

object MapperCardBoxes extends CardBoxes with MapperModelDependencies {
  override def findAll = mapper.CardBox.findAll.map(new MapperCardBox(_))
  override def findAllForCourse(cid:Long) = mapper.CardBox.findAll(By(mapper.CardBox.course,cid)).map(new MapperCardBox(_))

  override def findAllForCourseAndUser(cid:Long,uid:String)
  = mapper.CardBox.findAll(By(mapper.CardBox.course,cid),By(mapper.CardBox.uid,uid)).map(new MapperCardBox(_))
  override def find(id:Long) = mapper.CardBox.find(id).map(new MapperCardBox(_))
  override def findAllPublic = mapper.CardBox.findAll(By(mapper.CardBox.isPrivate,false)).map(new MapperCardBox(_))
  override def create(cId:Long, uid:String, name:String, description:String):CardBox =
    new MapperCardBox( new MCB(cId, uid,name,description))
}

trait AnswerHandler {
  def processResult(cardId: Long, p: Progress)
  def processResult(cardId: Long, p: Progress, actualizeStatistics_? : Boolean): Unit = {}
  def solutionSubmitted(sol:Solution):Unit
  def abort(redirectTo: String): Any = null
}

trait Solution
case class TextSolution(val text:String) extends Solution
case class ProgrammingSolution(val submission:Submission) extends Solution
case class WordListSolution(val words:List[String]) extends Solution


import scala.tools.nsc.Settings
import scala.tools.nsc.interpreter.IMain

object ScalaEvaluator {
  import scala.reflect.runtime.universe
  import scala.tools.reflect.ToolBox
  val tb = universe.runtimeMirror(getClass.getClassLoader).mkToolBox()

  def evaluate(clazz:String,env:Map[String,Any]) :Any= {
    val vars = env.foldLeft("")((r,x) => r+"val "+x._1+" = "+x._2+";")
    val expression = vars+clazz
    tb.eval(tb.parse(expression))
  }
  import scala.xml.transform.RewriteRule
  import scala.xml.transform.RuleTransformer

  import scala.xml.Text
  def rewrite(env:Map[String,Any]) = new RuleTransformer(new RewriteRule {
    override def transform(n: Node): Seq[Node] = {
      n match {
        case elem: Elem if elem.label == "v" => {
          val name = elem.attribute("name").get
          return(Text(""+env(name(0).toString)))
        }
        case elem: Elem if elem.label == "variableBindings" => NodeSeq.Empty
        case o =>o
      }
    }
  })

}
