package name.panitz.subato.model

import net.liftweb.mapper.By
import name.panitz.subato.model.mapper.IsTutorIn
import net.liftweb.common.Full

import net.liftweb.common.Box

import mapper.{IsTutorIn => MTutor}

trait Tutor extends Id {
  def name:String
  def course:Box[Course]
  def courseId:Long
  def term:Box[Term]
  def termId:Long
}


trait Tutors {
  def findByCourseAndTerm(courseId:Long,termId:Long):List[Tutor]
  def contains(uid:String, course:Course):Boolean
  def contains(uid:String, courseId:Long):Boolean
  def contains(uid:String, courseId:Long, termId:Long):Boolean
  def contains(uid:String):Boolean
  def createAndSave(name:String, course:Course,term:Term):Tutor
}


private class MapperTutor(private val mg:IsTutorIn) extends Tutor {
  def id = mg.id.get
  def name = mg.uid.get
  def course = mg.course.obj.map(new MapperCourse(_))
  def courseId = mg.course.get
  def term = mg.term.obj.map(new MapperTerm(_))
  def termId = mg.term.get

  override def equals(that:Any) = {
    if (that.isInstanceOf[Tutor]) {
      that.asInstanceOf[Tutor].name == this.name
    } else {
      false
    }
  }
}


private[model] object MapperTutors extends Tutors{
  override def createAndSave(name:String, course:Course,term:Term):Tutor = {
    val mg = new MTutor().uid(name).course(course.id).term(term.id)
      mg.save()
    val res = new MapperTutor(mg)
    res
  }

  override def contains(uid:String, courseId:Long) = {
    IsTutorIn.find(By(IsTutorIn.uid, uid), By(IsTutorIn.course, courseId)) match {
      case Full(_) => true
      case _ => false
    }
  }
  override def contains(uid:String, courseId:Long, termId:Long) = {
    IsTutorIn.find(By(IsTutorIn.uid, uid), By(IsTutorIn.course, courseId), By(IsTutorIn.term, termId)) match {
      case Full(_) => true
      case _ => false
    }
  }
  
  override def contains(uid:String, course:Course) = contains(uid, course.id)
  override def contains(uid:String) = IsTutorIn.count(By(IsTutorIn.uid, uid)) > 0

  override def findByCourseAndTerm(courseId:Long,termId:Long)
    = IsTutorIn.findAll(By(IsTutorIn.course, courseId),By(IsTutorIn.term, termId)).map(new MapperTutor(_))

}
