package name.panitz.subato.model.learnsystem

import net.liftweb.common.{Box, Full}
import net.liftweb.mapper.By
import name.panitz.subato.model.{Id, Finder}
import name.panitz.subato.model.mapper.learnsystem.{LearningProfile => LP, CompletedFlashcardCourse => CFC}

object FinalGrade extends Enumeration {
  type FinalGrade = Value

  val NULL    = Value(0)
  val GRADE_C = Value(1)
  val GRADE_B = Value(2)
  val GRADE_A = Value(3)

  def toStars(grade: FinalGrade): Int = {
    grade match {
      case NULL    => 0
      case GRADE_C => 1
      case GRADE_B => 2
      case _       => 3
    }
  }
}
import FinalGrade._


trait LearningProfile extends Id {
  def hintsShouldBeDisplayed_? : Boolean
  def deactivateDisplayHints: Unit
  def earnedStars: Int
  def updateEarnedStars(flashcardCourseId: Long, score: Double): Unit
  def getCompletionGradeOf(flashcardCourseId: Long): FinalGrade
}

trait LearningProfiles extends Finder[LearningProfile] {
  def findOrCreate(uid: String): Option[LearningProfile]
}

class MapperLearningProfile(profile: LP) extends LearningProfile {

  override def id: Long = profile.id.get

  override def hintsShouldBeDisplayed_? : Boolean = !this.profile.hintsAlreadyRead.get

  override def deactivateDisplayHints: Unit = {
    profile.hintsAlreadyRead.set(true)
    profile.save
  }

  override def earnedStars: Int = {
    MapperCompletedFlashcardCourses.findAll(id).foldLeft (0) { (res, n) => res + n.earnedStars }
  }

  override def updateEarnedStars(flashcardCourseId: Long, score: Double): Unit = {
    MapperCompletedFlashcardCourses.createOrUpdate(id, flashcardCourseId, score)
  }

  override def getCompletionGradeOf(flashcardCourseId: Long): FinalGrade = {
    val maybeCompletion = MapperCompletedFlashcardCourses.find(id, flashcardCourseId)
    maybeCompletion match {
      case Full(c) => c.grade
      case _       => NULL
    }
  }

}

object MapperLearningProfiles extends LearningProfiles {

  private def createNewProfile(uid: String): LP = {
    val np = new LP(uid)
    np.save
    np
  }

  override def find(id: Long): Box[LearningProfile] = LP.find(id).map (new MapperLearningProfile(_))

  override def findOrCreate(uid: String): Option[LearningProfile] = {
    if (uid.trim.isEmpty) {
      None
    } else {
      val lp = LP.find(By(LP.uid, uid)).getOrElse(createNewProfile(uid))
      Some(new MapperLearningProfile(lp))
    }
  }

  override def findAll: List[LearningProfile] = LP.findAll.map(new MapperLearningProfile(_))

}

private trait CompletedFlashcardCourse {
  def earnedStars: Int
  def grade: FinalGrade
  def updateCompletionScore(newScore: Double): Unit
  def save(): Unit
  def delete(): Unit
  def score: Double
}

private class MapperCompletedFlashcardCourse(cfc: CFC) extends CompletedFlashcardCourse {

  def earnedStars: Int = {
    getGradeAndStars match {
      case (_, stars) => stars
    }
  }

  def grade: FinalGrade = {
    getGradeAndStars match {
      case (g, _) => g
    }
  }

  def score: Double = cfc.completionScore.get

  private def getGradeAndStars: (FinalGrade, Int) = {
    val score = cfc.completionScore.get
    MapperCompletedFlashcardCourses.toGradeAndStars(score)
  }

  def updateCompletionScore(newScore: Double): Unit = {
    val oldScore = cfc.completionScore.get
    val isNewScoreBetter_? = oldScore < newScore
    if (isNewScoreBetter_?) {
      cfc.completionScore set newScore
      save
    }
  }

  def save(): Unit = cfc.save

  def delete(): Unit = cfc.delete_!
}

private object MapperCompletedFlashcardCourses {

  def findAll(profileId: Long): Seq[CompletedFlashcardCourse] = {
    val profiles = CFC.findAll(By(CFC.profile, profileId))
    profiles map {new MapperCompletedFlashcardCourse(_)}
  }

  def find(profileId: Long, flashcardCourseId: Long): Box[CompletedFlashcardCourse] = {
    val entry = CFC.find(By(CFC.profile, profileId), By(CFC.flashcardCourse, flashcardCourseId))
    entry map { new MapperCompletedFlashcardCourse(_) }
  }

  def createOrUpdate(profileId: Long, flashcardCourseId: Long, score: Double): Unit = {
    find(profileId, flashcardCourseId) match {
      case Full(a) => a.updateCompletionScore(score)
      case _       => new CFC(profileId, flashcardCourseId, score).save
    }
  }

  def toGradeAndStars(score: Double): (FinalGrade, Int) = {
    if (score < 50) (NULL, 0)
    else if ((score >= 50) && (score < 70)) (GRADE_C, 1)
    else if ((score >= 70)  && (score < 90)) (GRADE_B, 2)
    else (GRADE_A, 3)
  }

}
