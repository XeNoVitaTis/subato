package name.panitz.subato.model.mapper

import java.io.FileOutputStream
import scala.xml.Text
import java.io.File
import net.liftweb.common.{ Box, Full, Empty }
import net.liftweb.http.{ SHtml, FileParamHolder }
import net.liftweb.mapper.{ MappedString, Mapper }
import scala.xml.{ Elem, Node }
// import net.liftweb.util.Log._

private[model] class UploadField[T <: Mapper[T]](owner: T, dir: =>File) extends MappedString[T](owner, 1000) {

  //save the file to the dir and save the filename to the db
  def setFromUpload(fileHolder: Box[FileParamHolder]) = {
    fileHolder match {
      case Full(file) => {
        val fileName = System.currentTimeMillis + "-" + file.fileName
        if (!dir.exists) {
         dir.mkdir()
        }
	val data = file.file //TODO: use streams for better performance
	val out = new FileOutputStream(dir.getAbsolutePath + "/" + fileName)
	out.write(data)
	out.close()
	if (get != null && get.trim != "") {
	  new File(get).delete()
	}
	super.set(fileName)

      }
      case _ =>
    }
  }
  override def set(s:String) = get //disable setting of the value by other classes
  override def asHtml: Node = Text(if (get == null) "" else get)
  //display file upload field
  override def _toForm: Box[Elem] = Full(SHtml.fileUpload(fu => setFromUpload(Full(fu))))
}
