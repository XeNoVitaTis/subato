package name.panitz.subato.model.mapper
import net.liftweb.mapper.{Schemifier => SM}
import net.liftweb.mapper.MapperRules


trait Schemifier{
  def schemify()={
    MapperRules.createForeignKeys_? = _ => true
    
    SM.schemify(true, SM.infoF _
      , User
      , name.panitz.subato.model.mapper.TestExecutor
      , name.panitz.subato.model.mapper.Attendance
      , name.panitz.subato.model.mapper.Exercise
      , name.panitz.subato.model.mapper.Admin
      , name.panitz.subato.model.mapper.IsLecturerIn
      , name.panitz.subato.model.mapper.IsTutorIn
      , name.panitz.subato.model.mapper.Course
      , name.panitz.subato.model.mapper.CoursePage
      , name.panitz.subato.model.mapper.SLSCourseInCourseTerm
      , name.panitz.subato.model.mapper.Term
      , name.panitz.subato.model.mapper.ExerciseSheet
      , name.panitz.subato.model.mapper.ExerciseSheetEntry
      , name.panitz.subato.model.mapper.Submission
      , name.panitz.subato.model.mapper.Group
      , name.panitz.subato.model.mapper.UserInGroup
      , name.panitz.subato.model.mapper.ResourceFile
      , name.panitz.subato.model.mapper.ResourceFileForCourse
      , name.panitz.subato.model.mapper.Lesson
      , name.panitz.subato.model.mapper.HasRatedLesson
      , name.panitz.subato.model.mapper.Answer
      , name.panitz.subato.model.mapper.MultipleCard
      , name.panitz.subato.model.mapper.CardBox
      , name.panitz.subato.model.mapper.CardBoxEntry
      , name.panitz.subato.model.mapper.Duel
      , name.panitz.subato.model.mapper.Survey
      , name.panitz.subato.model.mapper.SurveyEntry
      , name.panitz.subato.model.mapper.SurveyEntryAnswer
      , name.panitz.subato.model.mapper.StatisticData
      , name.panitz.subato.model.mapper.learnsystem.FlashcardCourse
      , name.panitz.subato.model.mapper.learnsystem.LearningMaterial
      , name.panitz.subato.model.mapper.learnsystem.LearningProfile
      , name.panitz.subato.model.mapper.Ontology
      , name.panitz.subato.model.mapper.OntologyEntry
      , name.panitz.subato.model.mapper.Granted
      , name.panitz.subato.model.mapper.Points
      , name.panitz.subato.model.mapper.learnsystem.SubscribedFlashcardCourse
      , name.panitz.subato.model.mapper.learnsystem.SubscribedLearningMaterial
      , name.panitz.subato.model.mapper.learnsystem.CompletedFlashcardCourse
      , name.panitz.subato.model.mapper.learnsystem.DeletedFlashcardCourse

    )

    //SM.destroyTables_!!(SM.infoF _, name.panitz.subato.model.mapper.learnsystem.Learning)
  }
}
