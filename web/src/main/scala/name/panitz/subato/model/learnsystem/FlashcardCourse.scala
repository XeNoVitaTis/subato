package name.panitz.subato.model.learnsystem

import java.time.LocalDateTime

import scala.xml.{Elem, Node, NodeSeq, XML}

import FinalGrade._
import name.panitz.subato.model.{Card, CurrentUser, Finder, Id, IdAndNamed, SecureExportImport}
import name.panitz.subato.model.mapper.{TopicsSplitter => TS}
import name.panitz.subato.model.mapper.learnsystem.{FlashcardCourse => FCC, FlashcardCourseDateFormatter => FCDF}
import name.panitz.subato.snippet.ClienSideRefresher
import net.liftweb.common._
import net.liftweb.http._
import net.liftweb.http.js.JsCmd
import net.liftweb.mapper.By
import net.liftweb.util.CssSel
import net.liftweb.util.Helpers._


trait FlashcardCourse extends IdAndNamed {
  def title: String
  def description: NodeSeq
  def cards: Seq[Card]
  def isPrivate_? : Boolean
  final def isPublic_? : Boolean = ! isPrivate_?
  def ownerId: String
  def author: String
  def setAuthor(name: String): Unit
  def creationTime: LocalDateTime
  def lastUpdate: LocalDateTime
  def togglePrivacy: Unit
  def save: Unit
  def isCurrentlySubscribed_? : Boolean
  def learningMaterials: Seq[LearningMaterial]
  def formatForDisplay (user: CurrentUser, rfr: ClienSideRefresher, subscription: SubscribedFlashcardCourse = null): Node
  def salt(): String
  def update(newTitle: String, newDescription: String, topics: Seq[String], isPrivate: Boolean  = true): FlashcardCourse
  def delete(by: String = ownerId): Unit
  def asXML(): Node
  def asCatsXML(): Node
  def topics: Seq[String]
  def isOwner(uid: String): Boolean
  def abilityToToggleSubscription(userID: String, subscription: SubscribedFlashcardCourse, rfr: ClienSideRefresher): GUIDJsExp
  private[learnsystem] val DESCRIPTION_MAX_LENGTH_ON_UI = 85
  private[learnsystem] val TITLE_MAX_LENGTH_ON_UI       = 50
  private[learnsystem] val TOPIC_MAX_LENGTH_ON_UI       = 20
  private[learnsystem] val AUTHOR_NAME_MAX_LENGTH_ON_UI = 15
}

class MapperFlashcardCourse(private val flashcardCourse: FCC) extends FlashcardCourse {

  override def author: String = flashcardCourse.author.get

  override def setAuthor(name: String): Unit = {
    if (author.isEmpty) flashcardCourse.author(name)
  }

  override def topics: Seq[String] = {
    val result = TS.split(flashcardCourse.topics.get)
    if (result.isEmpty) Seq("Unknown topic")
    else result
  }

  override def id: Long = flashcardCourse.id.get

  override def salt(): String = flashcardCourse.salt.get

  override def update(newTitle: String, newDescription: String, topics: Seq[String], isPrivate: Boolean): FlashcardCourse = {
    flashcardCourse.update(newTitle, newDescription, topics, isPrivate)
    this
  }

  override def cards: Seq[Card] = {
    val maybeCards = learningMaterials map { _.flashcard }
    val fullCards = maybeCards filter { _.isDefined }
    fullCards map { _.openOrThrowException("") }
  }

  private def examCards: Seq[Card] = {
    val maybeCards = learningMaterials filter { _.isExamMaterial_? } map { _.flashcard }
    val fullCards = maybeCards filter { _.isDefined }
    fullCards map { _.openOrThrowException("") }
  }

  private def trainingCards: Seq[Card] = {
    val maybeCards = learningMaterials filter { !_.isExamMaterial_? } map { _.flashcard }
    val fullCards = maybeCards filter { _.isDefined }
    fullCards map { _.openOrThrowException("") }
  }

  override def name: String = this.title

  override def title: String = flashcardCourse.title.get

  override def isPrivate_? : Boolean = flashcardCourse.isPrivate.get

  override def ownerId: String = flashcardCourse.owner.get

  override def description: NodeSeq = flashcardCourse.description.asHtml

  override def creationTime: LocalDateTime = LocalDateTime.parse(flashcardCourse.creationTime.get, FCDF.get)

  override def lastUpdate: LocalDateTime = LocalDateTime.parse(flashcardCourse.lastUpdate.get, FCDF.get)

  override def togglePrivacy: Unit = {
    flashcardCourse.isPrivate.set(!flashcardCourse.isPrivate.get)
    flashcardCourse.save
  }

  override def save: Unit = {
    flashcardCourse.lastUpdate.set(LocalDateTime.now.format(FCDF.get))
    flashcardCourse.save
  }

  override def delete(by: String =  ownerId): Unit = {
    val subscribedCourses = MapperSubscribedFlashcardCourses.linkedTo(id)
    val cards_ = cards
    MapperDeletedFlashcardcourses.createAndSave(id, title, ownerId)
    subscribedCourses foreach { _.delete }
    learningMaterials foreach { _.delete }
    val otherLms = MapperLearningMaterials.findAll
    for (card <- cards_) {
      if (! (otherLms exists { _.flashcardId == card.id })) card.delete
    }
    flashcardCourse.delete_!
  }

  override def learningMaterials: Seq[LearningMaterial] = MapperLearningMaterials.of(id)

  override def isCurrentlySubscribed_? : Boolean = MapperSubscribedFlashcardCourses.existFor(id)

  override def formatForDisplay(user: CurrentUser, rfr: ClienSideRefresher, subscription: SubscribedFlashcardCourse = null): Node = {
    val uid = user.uid.getOrElse("")
    val isUserUnknown_? =  uid == ""
    lazy val isUserLoggedIn_? = user.loggedIn
    lazy val isUserAdmin_? = isUserLoggedIn_? && user.isAdmin
    lazy val isUserOwner_? = isUserLoggedIn_? && (uid == this.ownerId)

    if (isUserUnknown_?) formatForUnknowUser
    else if (isUserAdmin_?) formatForAdmin(uid, rfr, subscription)
    else if (isUserOwner_?) formatForOwner(subscription, rfr)
    else formatForLoggedInUser(uid, rfr, subscription)
  }

  private def addOwnerAndCardsNumberTag(currentUserName: String): CssSel = {
    var ownerBadgeContent   = ""
    val materialsSize = learningMaterials.size
    if (currentUserName.trim.compareToIgnoreCase(this.ownerId) == 0) {
      ownerBadgeContent = "Owner"
    } else {
      ownerBadgeContent = StringUtils.limitStr(this.ownerId, AUTHOR_NAME_MAX_LENGTH_ON_UI)
    }

    "#owner-badge *"              #> ownerBadgeContent   &
    "#owner-badge [title]"        #> s"Owner: ${this.ownerId} | Author: ${this.author}" &
    "#cards-number-badge *"       #> s"cards: $materialsSize" &
    "#cards-number-badge [title]" #> s"this course consists of $materialsSize cards."  
  }

  private def formatForAdmin(userId: String, rfr: ClienSideRefresher, subscription: SubscribedFlashcardCourse = null) = {
    val subscribed_? = subscription != null
    val grade = if (subscribed_?) subscription.finalGrade else NULL
    val isAuthor_? = subscribed_? && (this.ownerId == userId)

    def addSubscriptionButtonIfNonAuthor: CssSel = {
     if (! isAuthor_?) {
        "#subscriptionBtn *"         #> (if (subscribed_?) "unsubscribe"        else "subscribe") &
        "#subscriptionBtn [class+]"  #> (if (subscribed_?) "btn-outline-danger" else "btn-outline-success") &
        "#subscriptionBtn [onclick]" #> abilityToToggleSubscription(userId, subscription, rfr)
      } else {
        rmItemOnUI("subscribeOrUnsubscribe")
      } 
    }

    def addAbilityToTogglePrivacyIfOwner: CssSel = {
      if (isAuthor_?) addAbilityToTogglePrivacy(rfr)
      else "#privacy [onclick]" #> ""
    }

    val transform: CssSel = {
      "#fgStars"                      #> stars(grade) &
      "#road-to-course-view [href]"   #> (if (subscribed_?) s"/SlsCourse/${this.id}" else "#") &
      "#road-to-course-view [class+]" #> (if (!subscribed_?) "not-active" else "") &
      "#deletionBtn [onclick]"        #> abilityToDeleteFlashCardCourse(rfr, userId) &
      "#exportBtn [href]"             #> s"/exportSlsCourse/${this.id}/history" &
      "#exportCatsBtn [href]"         #> s"/exportSlsCourseForCats/${this.id}/history" &
      "#exportPDFBtn [href]"          #> s"/exportSlsCourseAsCardPDF/${this.id}/history" &
      "#editBtn [href]"               #> s"/flashcardcourse/edit/${this.id}" &
      addSubscriptionButtonIfNonAuthor  &
      addAbilityToTogglePrivacyIfOwner  &
      addOwnerAndCardsNumberTag(userId) &
      addCommonData
    }

    val pattern = MapperFlashcardCourses.flashcardCourseDisplayPattern
    transform(pattern).head
  }

  private def abilityToDeleteFlashCardCourse(rfr: ClienSideRefresher, deleter: String) = {
    new net.liftweb.http.js.JsCmds.Confirm("Wirklich Löschen?",
      (SHtml ajaxInvoke { () => this.delete(deleter) ; rfr.refresh & rfr.activeComplexTooltips }).cmd
    )
  }

  private def rmItemOnUI(htmlId: String): CssSel = s"#$htmlId" #> ""

  private def formatForOwner(subscription: SubscribedFlashcardCourse, rfr: ClienSideRefresher): Node = {
    val transform: CssSel = {
      "#fgStars"                    #> stars(subscription.finalGrade) &
      "#road-to-course-view [href]" #> s"/SlsCourse/${this.id}" &
      "#deletionBtn [onclick]"      #> abilityToDeleteFlashCardCourse(rfr, subscription.ownerId) &
      "#exportBtn [href]"           #> s"/exportSlsCourse/${this.id}/history" &
      "#exportCatsBtn [href]"       #> s"/exportSlsCourseForCats/${this.id}/history" &
      "#exportPDFBtn [href]"        #> s"/exportSlsCourseAsCardPDF/${this.id}/history" &
      "#editBtn [href]"             #> s"/flashcardcourse/edit/${this.id}" &
      addAbilityToTogglePrivacy(rfr) &
      addOwnerAndCardsNumberTag(this.ownerId) &
      rmItemOnUI("subscriptionBtn") &
      addCommonData
    }

    try {
      val pattern = MapperFlashcardCourses.flashcardCourseDisplayPattern
      transform(pattern).head
    } catch {
      case _:Exception => <div>Datenbankfehler?????</div>
    }
  }

  private def formatForLoggedInUser(userId: String, rfr: ClienSideRefresher, subscription: SubscribedFlashcardCourse = null): Node = {
    val subscribed_? = subscription != null
    val grade = if (subscribed_?) subscription.finalGrade else NULL
    val transform: CssSel = {
      "#subscriptionBtn *"          #> (if (subscribed_?) "unsubscribe"        else "subscribe") &
      "#subscriptionBtn [class+]"   #> (if (subscribed_?) "btn-outline-danger" else "btn-outline-success") &
      "#fgStars"                    #> stars(grade) &
      "#road-to-course-view [href]" #> (if (subscribed_?) s"/SlsCourse/${this.id}" else "#") &
      "#road-to-course-view [class+]"#> (if (!subscribed_?) "not-active" else "") &
      "#subscriptionBtn [onclick]"  #> abilityToToggleSubscription(userId, subscription, rfr) &
      "#exportPDFBtn [href]"          #> s"/exportSlsCourseAsCardPDF/${this.id}/history" &
      rmItemOnUI("deletionBtn") &
      rmItemOnUI("exportBtn") &
      rmItemOnUI("exportCatsBtn") &
      rmItemOnUI("editBtn") &
      addCommonData &
      addOwnerAndCardsNumberTag(userId)
    }
    val pattern = MapperFlashcardCourses.flashcardCourseDisplayPattern
    transform(pattern).head
  }

  private def addCommonData: CssSel = {
    val name_ = StringUtils.unknowIfEmpty(this.name)
    val topics_ = StringUtils.unknowIfEmpty(topics mkString ", ")
    "#courseName *"       #> s"${StringUtils.limitStr(this.name, TITLE_MAX_LENGTH_ON_UI)} / " &
    "#courseThemes *"     #> topicHtmlNodes &
    "#courseName [title]" #> s"""<div><b>Title:</b><br/>${name_}<br/>
                                  <br/><b>Topics:</b><br/>${topics_}</div>""" &
    "#courseDescription"  #> <div class="expander" onClick="expand(this)">{this.description}</div> &
    "#privacy *"          #> privacy
  }

  def abilityToToggleSubscription(userID: String, subscription: SubscribedFlashcardCourse, rfr: ClienSideRefresher): GUIDJsExp = {
     (subscription != null) match {
      case true => SHtml ajaxInvoke { () => deleteSubscription(userID, subscription.id, rfr) }
      case _    => SHtml ajaxInvoke { () => createSubscription(userID, rfr) }
     }
  }

  private def addAbilityToTogglePrivacy(refresher: ClienSideRefresher): CssSel = {
    var tooltipContent = if (isPrivate_?) "This Course is accessible only by you" else "This Course is accessible by every user"
    tooltipContent += "\nClick to toggle the accessibility." 
    "#privacy [onclick]" #> (SHtml ajaxInvoke { () => togglePrivacy ; refresher.refresh & refresher.activeComplexTooltips }) &
    "#privacy [title]"   #> tooltipContent
  }

  private def stars(grade: FinalGrade): NodeSeq = {
    def emptyStar = MapperFlashcardCourses.emptyStarPattern
    def fullStar  = MapperFlashcardCourses.fullStarPattern
    grade match {
      case NULL    => Seq(emptyStar, emptyStar, emptyStar)
      case GRADE_C => Seq(fullStar,  emptyStar, emptyStar)
      case GRADE_B => Seq(fullStar,  fullStar,  emptyStar)
      case _       => Seq(fullStar,  fullStar,  fullStar)
    }
  }

  private def privacy(): Node = {
    if (isPrivate_?) MapperFlashcardCourses.lockPattern
    else             MapperFlashcardCourses.globePattern
  }

  private def formatForUnknowUser(): Node = {
    val transform: CssSel = {
      "#subscriptionBtn"          #> <a href={"SlsCourse/"+this.id}>Learn</a> &
     // "#subscriptionBtn [class+]"   #> "btn-outline-success" &
      "#fgStars"                    #> stars(NULL) &
      "#road-to-course-view [href]" #> s"/SlsCourse/${this.id}" &
      "#exportPDFBtn [href]"        #> s"/exportSlsCourseAsCardPDF/${this.id}/history" &
      rmItemOnUI("deletionBtn") &
      rmItemOnUI("exportBtn") &
      rmItemOnUI("exportCatsBtn") &
      rmItemOnUI("editBtn") &
      addOwnerAndCardsNumberTag("") &
      addCommonData
    }

    val pattern = MapperFlashcardCourses.flashcardCourseDisplayPattern
    transform(pattern).head
  }

  private def topicHtmlNodes: Seq[Elem] = {
    topics take 3  map {x => <span class="badge badge-pill badge-warning">{StringUtils.limitStr(x, TOPIC_MAX_LENGTH_ON_UI)}</span>}
  }

  private def createSubscription(subscriber: String, rfr: ClienSideRefresher): JsCmd = {
    MapperSubscribedFlashcardCourses createAndSave (subscriber, id)
    rfr.refresh & rfr.activeComplexTooltips
  }

  private def deleteSubscription(subscriber: String, id: Long, rfr: ClienSideRefresher): JsCmd = {
    MapperSubscribedFlashcardCourses find id match {
      case Full(sfcc) => sfcc.delete
      case _          =>
    }
    rfr.refresh & rfr.activeComplexTooltips 
  }
  override def isOwner(uid: String): Boolean = ownerId == uid

  override def asCatsXML(): Node =
<itemBox id={id.toString}>
  <name>{name}</name>
  <author>{author}</author>
  <description>{description}</description>
  <items>{ trainingCards.map((item)=>Seq(scala.xml.Text("\n\n    "), item.asCatsXML)) }
  </items>
</itemBox>

  override def asXML(): Node = {

    <cardBox uid={id.toString} sec={SecureExportImport.hash(id.toString, flashcardCourse.salt.get)} update={true.toString}>
      <boxName>{name}</boxName>
      <author>{author}</author>
      <description>{description}</description>
      <exam>
        { examCards map { _.asXML } }
      </exam>
      <cards>
        { trainingCards map {_.asXML} }
      </cards>
    </cardBox>
  }

}

trait FlashcardCourses extends Finder[FlashcardCourse] {
  def createAndSave(title: String, isPrivate: Boolean, ownerId: String, desc: NodeSeq, topics: Seq[String], creation: LocalDateTime, lastUp: LocalDateTime): FlashcardCourse
  def createAndSave(title: String, isPrivate: Boolean, ownerId: String, desc: String, topics: Seq[String], creation: LocalDateTime, lastUp: LocalDateTime): FlashcardCourse
  def findAllCreatedBy(ownerId: String): List[FlashcardCourse]
  def findAllSubscribedBy(subscriber: String): Seq[SubscribedFlashcardCourse]
  def findAllAccessibleBy(uid: String): Seq[FlashcardCourse]
  val flashcardCourseDisplayPattern: Node = XML.loadFile("./webapps/subato/FlashcardCourseRow.html")
  val emptyStarPattern: Node = XML.loadFile("./webapps/subato/EmptyStar.html")
  val fullStarPattern:  Node = XML.loadFile("./webapps/subato/FullStar.html")
  val globePattern:     Node = XML.loadFile("./webapps/subato/Globe.html")
  val lockPattern:      Node = XML.loadFile("./webapps/subato/Lock.html")
}

object MapperFlashcardCourses extends FlashcardCourses {

  override def find(id: Long): Box[FlashcardCourse] = FCC.find(id).map(new MapperFlashcardCourse(_))
  override def findAll: List[FlashcardCourse] = FCC.findAll.map(new MapperFlashcardCourse(_))
  override def findAllAccessibleBy(uid: String): Seq[FlashcardCourse] = {
    findAll filter { x => (x.ownerId == uid) || (!x.isPrivate_?) }
  }

  override def createAndSave(title: String, isPrivate: Boolean, ownerId: String, desc: NodeSeq, topics: Seq[String], creation: LocalDateTime, lastUp: LocalDateTime) = {
    val result = new FCC(title: String, isPrivate, ownerId, desc, topics, creation, lastUp)
    result.save
    new MapperFlashcardCourse(result)
  }
  override def createAndSave(title: String, isPrivate: Boolean, ownerId: String, desc: String, topics: Seq[String], creation: LocalDateTime, lastUp: LocalDateTime) = {
    val result = new FCC(title: String, isPrivate, ownerId, desc, topics, creation, lastUp)
    result.save
    new MapperFlashcardCourse(result)
  }


  override def findAllCreatedBy(ownerId: String): List[FlashcardCourse] = {
    FCC.findAll(By(FCC.owner, ownerId)) map { new MapperFlashcardCourse(_) }
  }

  override def findAllSubscribedBy(subscriber: String): Seq[SubscribedFlashcardCourse] = {
    MapperSubscribedFlashcardCourses.allBy(subscriber)
  }

}

