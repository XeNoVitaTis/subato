package name.panitz.subato.model.learnsystem

import name.panitz.subato.model.mapper.learnsystem.{DeletedFlashcardCourse => DFC}
import net.liftweb.mapper.By

trait DeletedFlashcardcourse {
  def title: String
  def name: String = title
  def save: Unit
  def delete: Unit
}

class MapperDeletedFlashcardcourse(del: DFC) extends DeletedFlashcardcourse {

  override def title: String = del.title.get

  override def save: Unit = del.save

  override def delete: Unit = del.delete_!

}


trait DeletedFlashcardcourses {
  def namesOfDeletionsThatConcern(user: String): Seq[String]
  private[learnsystem] def createAndSave(flashcardCourse: Long, title: String, deleter: String): Unit
}

object MapperDeletedFlashcardcourses extends DeletedFlashcardcourses{

  override def namesOfDeletionsThatConcern(user: String): Seq[String] = {
    val deletions = DFC findAll By(DFC.userToNotify, user) map {new MapperDeletedFlashcardcourse(_)}
    val result = deletions map {_.title}
    deletions foreach { _.delete }
    result
  }

  override def createAndSave(flashcardCourse: Long, title: String, deleter: String): Unit = {
    MapperSubscribedFlashcardCourses
      .linkedTo(flashcardCourse)
      .filter { _.ownerId != deleter }
      .foreach { x => new MapperDeletedFlashcardcourse(new DFC(flashcardCourse, title, x.ownerId)).save }
  }

}

object StringUtils {
  def limitStr(str: String, lim: Int): String = {
    if (str.size > lim) s"${str.substring(0, lim - 3)}..."
    else str
  }

  def unknowIfEmpty(value: String): String = if (value.trim.isEmpty) "Unknown" else value.trim
}
