package name.panitz.subato.model

import java.security.MessageDigest
import java.util.Base64

trait AbstractSecureExportImport {
  type Data = String
  def hash(Sensible: Data, salt: String): Data
  def verify(hashed: Data, imported: Data, salt: String): Boolean
}

object SecureExportImport extends AbstractSecureExportImport {
  override def hash(sensible: Data, salt: String): String = {
    try {
      val toHash: Array[Byte] = (sensible + salt) getBytes "UTF-8"
      val base64Encoded: Array[Byte] = Base64.getEncoder.encode(MessageDigest.getInstance("SHA-256").digest(toHash))
      new String(base64Encoded)
    } catch {
      case _: Exception => "not updatable"
    }
  }

  override def verify(hashed: Data, imported: Data, salt: String): Boolean = hash(imported, salt) == hashed

}