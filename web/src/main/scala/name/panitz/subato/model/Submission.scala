package name.panitz.subato.model

import net.liftweb.common._
import java.util.Date
import net.liftweb.mapper._
import name.panitz.subato.model.mapper.{Submission => MS}
import net.liftweb.util.Helpers._
import scala.xml.Node
import name.panitz.subato._

trait Submissions extends Finder[Submission] {
  def find(uid:String, ex:Exercise,cId:Long,tId:Long):List[Submission]
  def find(uid:String, exId:Long,cId:Long,tId:Long):List[Submission]
  def find(ex:Exercise,cId:Long,tId:Long):List[Submission]
  def find(exId:Long,cId:Long,tId:Long):List[Submission]
  def find(ex:Exercise,tId:Long):List[Submission]
  def create(time:Date, exercise:Exercise,cId:Long,tId:Long, uid:String, code:String):Submission
  def findLatest(uid:String, exId:Long):Box[Submission]
  def findSorted(uid:String, exId:Long):List[Submission]
  def find(uid:String, exId:Long):List[Submission]
  def findGraded(uid:String, exId:Long):Box[Submission]
  def count(uid:String, exercise:Exercise,cId:Long,tId:Long):Long
  def commentsAvailable(uid:String, exercise:Exercise):Int
  def findBest(uid:String, cId:Long, exId:Long, termId:Long):Box[Submission]

  def findBestTested(uid:String, cId:Long, exId:Long, termId:Long):Box[Submission]
}

trait Submission extends Id {
  def uid:String
  def directory:String
  def courseID:Long
  def termID:Long
  def datetime:Date
  def solution:String

  def compilationFailed:Boolean
  def compilationOutput:String
  def styleCheckOutput:Node
  def messages:String
  def unittests:Node
  def comment:Node
  def hasComment:Boolean

  def save()
  def errors_=(errors:Int)
  def stylecheckErrors_=(stylecheckErrors:Int)
  def failures_=(failures:Int)
  def tests_=(tests:Int)
  def allocations_=(allocations:Int)
  def frees_=(frees:Int)
  def score_=(scaore:Int)
  def messages_=(messages:String)
  def unittests_=(unittests:Node)
  def compilationFailed_=(failed:Boolean)
  def compilationOutput_=(output:String)
  def styleCheckOutput_=(output:Node)
  def directory_=(directory:String)


  def graded:Boolean

  def tests:Int
  def failures:Int
  def errors:Int
  def stylecheckErrors:Int
  def allocations:Int
  def frees:Int
  def score:Long

  def asHtml(ex:Exercise):Node


}

object MapperSubmissions extends Submissions with MapperModelDependencies {
  override def find(uid:String, ex:Exercise,cId:Long,tId:Long) =
    find(uid, ex.id,cId,tId) 

  override def find(uid:String, exId:Long,cId:Long,tId:Long) = {
    MS.findAll(
      By(MS.uid, uid),
      By(MS.exercise, exId),
    //  By(MS.course, cId),
      By(MS.term, tId),
      OrderBy(MS.datetime, Descending)) map {
      new MapperSubmission(_)
    }
  }

  override def find(ex:Exercise,cId:Long,tId:Long) = find(ex.id,cId,tId)

  override def find(exId:Long,cId:Long,tId:Long):List[Submission]={
      MS.findAll(
      By(MS.exercise, exId),
      By(MS.course, cId),
      By(MS.term, tId)
      ) map {
      new MapperSubmission(_)
    }
  }

  
  override def find(ex:Exercise,tId:Long) = {
    MS.findAll(
      By(MS.exercise, ex.id),
      By(MS.term, tId)
      ) map {
      new MapperSubmission(_)
    }
  }


  override def count(uid:String, exercise:Exercise,cId:Long,tId:Long):Long = {
    MS.count(By(MS.uid, uid), By(MS.exercise, exercise.id),By(MS.course, cId),By(MS.term, tId))
  }

  override def commentsAvailable(uid:String, exercise:Exercise):Int = {
    (MS.find(By(MS.uid, uid), By(MS.exercise, exercise.id)).filter(_.comment.hasEntry)
      .size) 
  }


  override def find(id:Long) = MS.find(id).map(new MapperSubmission(_))

  override def findAll = MS.findAll.map(new MapperSubmission(_))

  override def create(time:Date, exercise:Exercise,cId:Long,tId:Long, uid:String, code:String) = {
    val graded = exercise.submissionMode match {
      case SubmissionMode.Graded => count(uid, exercise,cId,tId) < exercise.attempts 
      case _ => false
    }
    new MapperSubmission(time, exercise, uid, code, graded,cId,tId)
  }

  override def findSorted(uid:String, exId:Long) = {
     MS.findAll(
       By(MS.uid, uid),
       By(MS.exercise, exId),
       OrderBy(MS.datetime, Ascending)).map(new MapperSubmission(_))
  }

  override def find(uid:String, exId:Long) = {
    MS.findAll(
       By(MS.uid, uid),
       By(MS.exercise, exId)
     ).map(new MapperSubmission(_))
  }

  override def findLatest(uid:String, exId:Long):Box[Submission] = {
    tryo(findSorted(uid, exId).head)
  }

  override def findBest(uid:String, cId:Long, exId:Long, termId:Long):Box[Submission] = {
    tryo(new MapperSubmission(
      MS.findAll(
        By(MS.uid, uid),
        //By(MS.course, cId),
        By(MS.exercise, exId),
        By(MS.term, termId),
        OrderBy(MS.score, Descending)).head
    ))
  }

  override def findBestTested(uid:String, cId:Long, exId:Long, termId:Long):Box[Submission] = {
    tryo(
      MS.findAll(
        By(MS.uid, uid),
        //By(MS.course, cId),
        By(MS.exercise, exId),
        By(MS.term, termId)
      ).map(new MapperSubmission(_)).maxBy((s) => (s.tests - s.failures - s.errors) * 10000 +s.score)) 
  }


  override def findGraded(uid:String, exId:Long):Box[Submission] = {
    MS.find(By(MS.uid, uid), By(MS.exercise, exId)) map {
      new MapperSubmission(_)
    }				
  }
}

class MapperSubmission(private val s:MS) extends Submission with SWrapperL10n{
  def this(time:Date, exercise:Exercise, uid:String, code:String, graded:Boolean,courseId:Long,termId:Long) {
    this(new MS(time, exercise.id, uid, code, graded,courseId,termId))
  }
  override def toString()=uid+" "+compilationOutput+" "+messages+" "+tests+" "+failures+" "+errors+" "+unittests

  override def uid = s.uid.get
  override def directory: String  = s.directory.get
  
  override def id = s.id.get
  override def save() = s.save()
  override def datetime = s.datetime.get
  override def solution = s.solution.get

  override def compilationFailed = s.compilationFailed.get
  override def compilationOutput = s.compilationOutput.get
  override def styleCheckOutput = s.styleCheckOutput.asHtml
  override def messages = s.messages.get

  override def unittests = s.unittests.asHtml
  override def comment = s.comment.asHtml

  override def hasComment = s.comment.hasEntry

  override def graded = s.graded.get
  override def tests = s.tests.get
  override def failures = s.failures.get
  override def errors = s.errors.get
  override def stylecheckErrors = s.stylecheckErrors.get 
  override def allocations = s.allocations.get
  override def frees = s.frees.get
  override def score = s.score.get
  override def courseID = s.course.get
  override def termID = s.term.get

  override def errors_=(errors:Int) {
    s.errors.set(errors)
  }
  override def stylecheckErrors_=(stylecheckErrors:Int) {
    s.stylecheckErrors.set(stylecheckErrors)
  }
  override def failures_=(failures:Int) {
    s.failures.set(failures)
  }
  override def tests_=(tests:Int) {
    s.tests.set(tests)
  }
  override def allocations_=(allocations:Int) {
    s.allocations.set(allocations)
  }
  override def frees_=(frees:Int) {
    s.frees.set(frees)
  }

  override def score_=(score:Int) {
    s.score.set(score)
  }


  override def messages_=(messages:String) {
    s.messages.set(messages)
  }

  override def directory_=(directory: String): Unit = s.directory.set(directory)

  override def unittests_=(unittests:Node) {
    s.unittests.set(unittests.toString)
  }

  override def compilationFailed_=(failed:Boolean) {
    s.compilationFailed.set(failed)
  }
  override def compilationOutput_=(messages:String) {
    s.compilationOutput.set(messages)
  }

  override def styleCheckOutput_=(messages:Node) {
    s.styleCheckOutput.set(messages.toString)
  }


  override def asHtml(ex:Exercise) =
    <div>
      <h2>{uid+" "+datetime}</h2>
       <h3><div>
         {
           if(ex.isFileUploadExercise){
             var fileExtension = ex.fileExtention
             //This is a hack for supporting multiple file extensions in file upload
             if (solution != null && solution != "") {
               fileExtension = solution
             }
             if (!compilationFailed) {
               <a href={"/serveUploadedSolution/"+ex.id+"/"+uid+"/"+directory+"/uploadedFile/"+fileExtension}>Abgabe</a>
                 <div class="parent"><h2 class="child" style="background: #00FF00;">{compilationOutput}</h2></div>
             }else {
               <div class="parent"><h2 class="child" style="background: #FF2222;color: #FFFFFF;">{compilationOutput}</h2></div>
             }

           }else if (!compilationFailed){
             if (tests==0){
               <div class="parent"><h2 class="child" style="background: #FF2222;color: #FFFFFF;">Fehlgeschlagen: keine Tests laufen</h2></div>
             }else if(errors+failures == 0){
               <div class="parent"><h2 class="child" style="background: #00FF00;">Alle Tests laufen erfolgreich</h2></div>
             }else if(tests-errors-failures > tests/2){
               <div class="parent"><h2 class="child" style="background: #FFFF00;">Fast: mehr als die Hälfte der Tests laufen</h2></div>
             }else {
               <div class="parent"><h2 class="child" style="background: #FF2222;color: #FFFFFF;">So gut wie keine Tests funktionieren</h2></div>
             }
           } else {
             <div class="parent"><h2 class="child" style="background: #AA0000;color: #FFFFFF; ">Fehlgeschlagen: kompiliert nicht</h2></div>
           }
         }</div></h3><br /><br />
          { if (!ex.isFileUploadExercise){
            <div>
              <b>{loc("test")}s: </b>{tests}<br />
              <b>{loc("failures")}: </b>{failures}<br />
              <b>{loc("errors")}: </b>{errors}<br />
              <b>{loc("styleCheckErrors")}: </b>{stylecheckErrors}<br />
              <b>{loc("allocations")}: </b>{allocations}<br />
              <b>{loc("frees")}: </b>{frees}<br />
              <b>{loc("score")}: </b>{score}<br /><br />
              <h3>{loc("compilationOutput")}</h3>
              <div style="width: 100%; overflow-x: auto;"><pre>{compilationOutput}</pre></div>
              <h3>{loc("messages")}</h3>
              <div>{messages}</div><br /><br />
              <h3>{loc("unitTests")}</h3>
              <div>{unittests}</div><br /><br />
              {
                if (ex.hasStyleCheck){
                  <div>
                    <h3>Style Checks</h3>
                    <b>Style Problems:</b> <span id="stylecheckerrors">{stylecheckErrors}</span><br />
                    <div>{styleCheckOutput}</div>
                  </div>
                }
              }
              <h3>{loc("solution")}</h3>
                <pre class="line-numbers"><code class={"language-" +ex.brush}>{solution}</code></pre>
            </div>
          }}
          <div style="background-color: yellow;">{
              if (comment!=null) comment
           }</div>
          </div>

}


case class UnitTestCaseResult(testCase:String,messages:scala.xml.Node){}
