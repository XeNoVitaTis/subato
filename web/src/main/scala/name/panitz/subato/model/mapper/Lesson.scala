package name.panitz.subato.model.mapper

import scala.xml._
import net.liftweb.common._
import net.liftweb.mapper._
import net.liftweb.util.Helpers._

import java.text.DateFormat
import java.util.Date
import net.liftweb.common.Box._

import name.panitz.subato.model.MapperCoursePage

import net.liftweb.http.{ SHtml }

class Lesson extends LongKeyedMapper[Lesson] with IdPK {
  def getSingleton = Lesson

  object coursePage extends MappedLongForeignKey(this, CoursePage) {
    override def dbIndexed_? = true
    override def validSelectValues = Full(CoursePage.findAll.map(x => (x.id.get, new MapperCoursePage(x).name)))
    override def asHtml() = {
      Text(CoursePage.find(By(CoursePage.id, this.get)) map {new MapperCoursePage(_).name} openOr "")
    }
  }

  object content extends MappedWysiwygEditor(this,()=>"page content")

  object date extends DateTimeHelper(this,()=>"date") 

  object star1 extends MappedInt(this)
  object star2 extends MappedInt(this)
  object star3 extends MappedInt(this)
  object star4 extends MappedInt(this)
  object star5 extends MappedInt(this)

}


import net.liftweb.sitemap.Loc.Hidden

object Lesson extends Lesson with LongKeyedMetaMapper[Lesson]
    with LecturerOnlyCRUDify[Long, Lesson] {
  override def showAllMenuLoc = Empty
  override def createMenuLocParams = Hidden :: super.createMenuLocParams

}
