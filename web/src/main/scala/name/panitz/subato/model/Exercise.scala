package name.panitz.subato.model

import scala.xml.Node
import scala.xml.XML
import scala.xml.Elem
import scala.xml.NodeSeq
import name.panitz.subato.model.mapper.{Exercise => ME}
import net.liftweb.common._
import net.liftweb.mapper.By

import name.panitz.subato.evaluator._
import name.panitz.subato.XmlConfigDependency

object SubmissionMode extends Enumeration {
  type SubmissionMode = Value
  val Graded, Ungraded = Value
}

object Language extends Enumeration {
  type Language = Value
  val Java, C, Haskell, LiterateHaskell, Scala, Kotlin, Ruby, Python, Cpp, Go, Unspecified, FileUpload = Value

  def withNameOpt(s: String): Option[Value] = values.find(_.toString == s)
}
trait Exercises extends Finder[Exercise] {
  def create(cId:Long, lang:String, name:String, descr:String,txt:String,fqn:String, solTmpl:String,solution:String, teId: Long):Exercise
  def create(cId:Long, lang:String, extension:String,name:String, descr:String,txt:String,fqn:String, solTmpl:String,solution:String, teId: Long):Exercise
  def findAll(cId:Long):List[Exercise];
}

object MapperExercises extends Exercises {
  override def find(id:Long):Box[Exercise] = ME.find(id).map(new MapperExercise(_))
  override def findAll:List[Exercise] = ME.findAll.map(new MapperExercise(_))
  override def findAll(cId:Long):List[Exercise]
    = ME.findAll(By(ME.course, cId)).map(new MapperExercise(_))

  override def create(cId:Long, lang:String, name:String, descr:String,txt:String,fqn:String, solTmpl:String,solution:String, testFiles:String, teId: Long)=
    new MapperExercise(cId, lang, name, descr,txt,fqn, solTmpl,solution, testFiles, teId)

  override def create(cId: Long, lang: String, name: String, descr: String, txt: String, fqn: String, solTmpl: String, solution: String, teId: Long): Exercise =
    new MapperExercise(cId, lang, name, descr,txt,fqn, solTmpl,solution, "", teId)
}


trait Exercise extends IdAndNamed  {
  def name:String
  def owner:String
  def description:String
  def course:Course
  def exerciseText:Node
  def solutionTemplate:String
  def solutionFQCN:String
  def solution:String
  def solutionIsPublic: Boolean
  def lambdaUserAllowed: Boolean
  def isFileUploadExercise:Boolean
  def filename:String
//  def submissions:List[Submission]
  import SubmissionMode._
  def submissionMode:SubmissionMode
  import Language._
  def language:Language
  def isGraded:Boolean
  def attempts:Int
  def brush:String
  def fileExtention:String
  def hasStorageCheck:Boolean
  def hasStyleCheck:Boolean
  def resourceFiles:List[ResourceFile]
  def files:List[String]
  def evaluator:Evaluator
  def latestTerm:Term
  def testFilesXML:String
  def asXML:Node
  def save()
  def delete()
  def getTestFiles():Node
  def setTestFiles(files:String)
  def testExecutor: TestExecutor
  def testExecutor_=(c:TestExecutor)

}

private[model] trait AbstractMapperExercise extends Exercise with XmlConfigDependency {
  protected val e: ME
  protected val courses: Courses
  protected val testExecutors: TestExecutors

  override def id = e.id.get

  override def name = e.name.get

  override def owner = e.owner.get

  override def description = e.description.get

  override def course
  = if (!e.course.obj.isEmpty)
    new MapperCourse(e.course.obj.openOrThrowException(null))
  else null

  override def testExecutor
  = if (!e.testExecutor.obj.isEmpty)
    new MapperTestExecutor(e.testExecutor.obj.openOrThrowException(null))
  else null

  override def testExecutor_=(c:TestExecutor) ={ e.testExecutor.set(c.id)}


  override def exerciseText = e.exerciseText.asHtml

  override def solution = e.solution.get

  override def solutionIsPublic = e.solutionIsPublic.get


  override def lambdaUserAllowed = e.lambdaUserAllowed.get
  override def isFileUploadExercise = e.language.get==Language.FileUpload 
  override def solutionTemplate = e.solutionTemplate.get 
  override def solutionFQCN = e.solutionFQCN.get 
//  override def submissions = e.results.map(new MapperSubmission(_))
  override def submissionMode = e.submissionMode.get  match {
      case SubmissionMode.Graded => SubmissionMode.Graded 
      case SubmissionMode.Ungraded => SubmissionMode.Ungraded
      case _ => SubmissionMode.Ungraded
  }

  override def language = e.language.get match {
    case Language.Java => Language.Java
    case Language.Go => Language.Go
    case Language.C => Language.C
    case Language.Haskell => Language.Haskell
    case Language.LiterateHaskell => Language.LiterateHaskell
    case Language.Python => Language.Python
    case Language.Kotlin => Language.Kotlin
    case Language.Unspecified => Language.Unspecified
    case Language.FileUpload => Language.FileUpload
    case _ => Language.Java
  }

  override def brush = e.language.get match {
    case Language.Java => "java"
    case Language.C => "cpp"
    case Language.Haskell => "hs"
    case Language.LiterateHaskell => "hs"
    case Language.Python => "python"
    case _ => "java"
  }

  override def fileExtention = e.language.get match {
    case Language.Go => "go"
    case Language.Java => "java"
    case Language.C => "c"
    case Language.Haskell => "hs"
    case Language.LiterateHaskell => "lhs"
    case Language.Python => "py"
    case Language.Kotlin => "kt"
    case Language.Scala => "scala"
    case Language.FileUpload => e.theFileExtention.get
    case _ => e.theFileExtention.get
  }

  override def testFilesXML = e.testFiles.get

  override def hasStorageCheck = e.language.get match {
    case Language.C => true
    case _ => false
  }

  override def hasStyleCheck = e.language.get match {
    case Language.Java => true
    case _ => false
  }

  override def resourceFiles: List[ResourceFile] = e.resourceFiles.map(new MapperResourceFile(_))

  override def files: List[String] = { //
    val files = getTestFiles()
    try{
      (files \ "class").seq.map(x=>((x \@ "name") + "___" + (x \@ "lang"))).toList  }catch {
      case _ : Exception => List()
    }
  }

  override def attempts = if (e.attempts.get == 0) 5 else e.attempts.get

  override def isGraded = submissionMode == SubmissionMode.Graded

  override def filename = e.filename.get

  override def asXML =
    <exercise>
      <name>
        {name}
      </name>
      <description>
        {description}
      </description>{val ttt = e.exerciseText.asHtml.child.head
    if (ttt.label == "text") ttt else <text>
      {ttt}
    </text>}<solutionTemplate>
      {e.solutionTemplate.get}
    </solutionTemplate>
      <solutionFQCN>
        {e.solutionFQCN.get}
      </solutionFQCN>
      <extension>
        {fileExtention}
      </extension>
      <lang>
        {language}
      </lang>
      <solution>
        {solution}
      </solution>
      {scala.xml.XML.loadString(testFilesXML)}
    </exercise>

  import java.io._


  val sourceExtensions = List(".java", ".hs", ".py", ".lhs", ".c", ".cpp")

  def isSource(f: File) = !sourceExtensions.filter((ex) => f.getName.endsWith(ex)).isEmpty

  def getSourceFiles(folder: File): List[File] = {
    try {
      folder
        .listFiles
        .toList
        .map((f) => if (f.isDirectory) {
          getSourceFiles(f)
        } else {
          if (isSource(f)) List(f) else List()
        })
        .flatten
    } catch {
      case e: Exception => List()
    }
  }

  override def getTestFiles(): Node = {
    if (isFileUploadExercise)
      return <testclasses></testclasses>

    scala.xml.XML.loadString(testFilesXML)
  }

  override def setTestFiles(files: String) = {
    e.testFiles.set(files)
  }


  override def evaluator: Evaluator = {
    if (isFileUploadExercise) new FileUploadedSolution
    else new DockerEvaluator
  }

  override def latestTerm:Term = {
    try{
    val result =
     mapper.ExerciseSheetEntry
      .findAll(By(mapper.ExerciseSheetEntry.exercise,e.id.get))
      .map((entry)=>new MapperExerciseSheetEntry(entry).sheet.term)
      .sortWith( (t1,t2) => t1.year>t2.year || (t1.year==t2.year && t1.addendum>t2.addendum))
      .head
      result
    }catch{
      case e:Exception => MapperTerms.findAll.sortWith( (t1,t2) => t1.year>t2.year || (t1.year==t2.year && t1.addendum>t2.addendum)).head
    }
  }



  override def save() = e.save
  override def delete() = e.delete_!
}

class MapperExercise(private val me:ME) extends AbstractMapperExercise {
  def this(cId:Long, lang:String, name:String, descr:String,txt:String,fqn:String, solTmpl:String,solution:String, testFilesXML:String, teId: Long){
    this(new ME(cId,lang,name,descr,txt,fqn,solTmpl,solution,SubmissionMode.Graded, testFilesXML, teId))
  }
  def this(cId:Long, lang:String, extension:String,name:String, descr:String,txt:String,fqn:String, solTmpl:String,solution:String, testFilesXML:String, teId: Long){
    this(new ME(cId,lang,extension,name,descr,txt,fqn,solTmpl,solution, testFilesXML, teId))
  }

  override val e = me
  override val courses = MapperCourses
  override val testExecutors = MapperTestExecutors
}
