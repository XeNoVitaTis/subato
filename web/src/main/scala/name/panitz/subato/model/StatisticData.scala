package name.panitz.subato.model

import name.panitz.subato.model.mapper.{StatisticData => SD}
import name.panitz.subato.SWrapperL10n
import net.liftweb.common.Box
import net.liftweb.mapper.By
import net.liftweb.util.CssSel
import net.liftweb.util.Helpers._

import scala.language.postfixOps
import scala.xml.{Elem, NodeSeq}
import scala.math._

trait Statistics extends Finder[StatisticData] {
  def findCardStatistic(cardRef: Long): StatisticData
  def createAndSave(referencedCard: Long, good: Int, almost: Int, wrong: Int): SD
  def deleteStatisticOf(cardId: Long): Unit
  def incWrongAnswerNum(cardId: Long, neededSeconds: Long)
  def incGoodAnswerNum(cardId: Long, neededSeconds: Long)
  def incAlmostCorrectAnswerNum(cardId: Long, neededSeconds: Long)
  def layoutOf(cardId: Long, resolutionTime: Long): CssSel
  def layoutOf(cardId: Long, suffixOfDiagrammIds: String = "", style: String=""): CssSel
}

trait StatisticData extends Id {
  override def id: Long
  def flashcardId: Long
  def goodAnswerPercent: Double
  def almostGoodAnswerPercent: Double
  def wrongAnswerPercent: Double
  def attempts: Int
  def isEmpty_? : Boolean
  def averageTime: Double
  def delete: Unit
  def evaluate: Double

  def goodAnswers :Int
  def almostCorrectAnswers:Int
  def wrongAnswers:Int
  def averageSolutionTime:Double

}

trait StatisticBox extends Id {
  override def id: Long
  def link: NodeSeq
}

object MapperStatistics extends Statistics with SWrapperL10n {

  override def layoutOf(cardId: Long, resolutionTime: Long): CssSel = {
    val statisticalData  = this findCardStatistic cardId
    val simpleLayout     = layoutOf(statisticalData, "", "")

    if (statisticalData.isEmpty_?) {
      simpleLayout & deleteTimeLayout
    } else {
      simpleLayout & addTimeLayout(statisticalData, resolutionTime)
    }
  }

  override def layoutOf(cardId: Long, suffixOfDiagrammIds: String = "", style: String=""): CssSel = {
    val statisticalData  = this findCardStatistic cardId
    val simpleLayout     = layoutOf(statisticalData, suffixOfDiagrammIds, style)

    if (statisticalData.isEmpty_?) {
      simpleLayout & deleteTimeLayout
    } else {
      simpleLayout & addTimeLayout(statisticalData)
    }
  }

  private def deleteTimeLayout: CssSel = "#neededTime" #> <div></div>

  private def addTimeLayout(statisticalData: StatisticData): CssSel = {
    val timeOutputPattern = loc("cardTime").toString
    val avrTime           = s"${statisticalData.averageTime.round} Sec."
    val needTime          = ""
    val timeOutput        = timeOutputPattern.replace("{{avr}}", avrTime).replace("{{need}}", needTime) 

    "#statisticRoot [title]" #> timeOutput 
  }

  private def addTimeLayout(statisticalData: StatisticData, resolutionTime: Long): CssSel = {
    val timeOutputPattern = loc("cardTime").toString
    val avrTime           = s"${statisticalData.averageTime.round} Sec."
    val needTime          = s"$resolutionTime Sec."
    val timeOutput        = timeOutputPattern.replace("{{avr}}", avrTime).replace("{{need}}", needTime)

    "#statisticRoot [title]" #> timeOutput
  }

  private def layoutOf(statisticalData: StatisticData, suffixOfDiagrammIds: String, style: String): CssSel = {
    val barMaxLength = 225
    val barMinLength = 45
    val timeToolTipMaxLength = 400
    val goodAnsPercentage = statisticalData.goodAnswerPercent
    val almostCorrectPercentage = statisticalData.almostGoodAnswerPercent
    val wrongAnsPercentage = statisticalData.wrongAnswerPercent
    def toPercentStr(percent: Double): String  = {
      val result = if (percent == 0) "" else f"${percent}%.1f"
      if (result.endsWith("0")) result.substring(0, result.length() - 2) + "%" else if (result.nonEmpty) result + "%" else result
    }
    
    def toBarLength(percent: Double): Int = if (percent == 0) 0 else barMinLength + (barMaxLength * percent / 100).round.toInt
    def txtTranslation(barLength: Int, offset: Int): Int = if (barLength == 0) 0 else offset + (barLength/2) - (barMinLength/2) 

    val greenBarLength = toBarLength(goodAnsPercentage)
    val yellowBarLength = toBarLength(almostCorrectPercentage)
    val redBarLength = toBarLength(wrongAnsPercentage)


    "#goodAnsProgress [width]"          #> greenBarLength &
    "#goodAnsPercentage [x]"            #> txtTranslation(greenBarLength, 0) &
    "#goodAnsPercentage *"              #> toPercentStr(goodAnsPercentage)  &
    "#almostCorrectAnsProgress [width]" #> yellowBarLength &
    "#almostCorrectAnsProgress [x]"     #> greenBarLength &
    "#almostCorrectAnsPercentage [x]"   #> txtTranslation(yellowBarLength, greenBarLength) &
    "#almostCorrectAnsPercentage *"     #> toPercentStr(almostCorrectPercentage) &
    "#badAnsProgress [width]"           #> redBarLength &
    "#badAnsProgress [x]"               #> (greenBarLength + yellowBarLength) & 
    "#badAnsPercentage [x]"             #> txtTranslation(redBarLength, greenBarLength + yellowBarLength) &
    "#badAnsPercentage *"               #> toPercentStr(wrongAnsPercentage) &
    "#svgContainer [width]"             #> (greenBarLength + yellowBarLength + redBarLength) &
    "#statisticRoot [style+]"           #> style &
    "#goodAnsAnim [id]"                 #> s"goodAnsAnim$suffixOfDiagrammIds" &
    "#almostCorrectAnsAnim [id]"        #> s"almostCorrectAnsAnim$suffixOfDiagrammIds" &
    "#badAnsAnim [id]"                  #> s"badAnsAnim$suffixOfDiagrammIds" &
    "#goodAnsProgress [fill]"           #> s"url(#goodAnsAnim$suffixOfDiagrammIds)" &
    "#almostCorrectAnsAnim [fill]"      #> s"url(#almostCorrectAnsAnim$suffixOfDiagrammIds)" &
    "#badAnsAnim [fill]"                #> s"url(#badAnsAnim$suffixOfDiagrammIds)"
  }

  private def getCardStatisticFromDB(cardId: Long): SD = {
    val candidates  = SD.findAll(By(SD.flashcardId, cardId))
    lazy val byNeed = new SD(cardId);

    if (candidates.nonEmpty) {
      candidates.head
    } else {
      byNeed.save()
      byNeed
     }
  }

  override def incAlmostCorrectAnswerNum(cardId: Long, neededTime: Long): Unit = {
    val statistic = this getCardStatisticFromDB cardId
    statistic incAlmostCorrectAnswer neededTime
  }

  override def incGoodAnswerNum(cardId: Long, neededTime: Long): Unit = {
    val statistic = this getCardStatisticFromDB cardId
    statistic incGoodAnswer neededTime
  }

  override def incWrongAnswerNum(cardId: Long, neededTime: Long): Unit = {
    val statistic = this getCardStatisticFromDB cardId
    statistic incWrongAnswer neededTime
  }

  override def find(id: Long): Box[MapperStatistic] = SD.find(id).map(new MapperStatistic(_))

  override def findAll: List[MapperStatistic] = SD.findAll.map(new MapperStatistic(_))

  override def findCardStatistic(cardRef: Long): StatisticData = {
    new MapperStatistic(this.getCardStatisticFromDB(cardRef))
  }

  override def deleteStatisticOf(cardId: Long): Unit = SD.findAll(By(SD.flashcardId, cardId)) foreach { _.delete_! }

  override def createAndSave(referencedCard: Long, good: Int = 0, almost: Int = 0, wrong: Int = 0): SD = {
    val ms = new SD()
    ms.goodAnswers(good)
    ms.wrongAnswers(wrong)
    ms.almostCorrectAnswers(almost)
    ms.flashcardId(referencedCard)
    ms.save()
    ms
  }
}

private[model] class MapperStatistic(private val std: SD) extends StatisticData {
  override def id: Long = std.id.get
  override def flashcardId: Long = std.flashcardId.get
  override def attempts: Int = std.goodAnswers.get + std.almostCorrectAnswers.get + std.wrongAnswers.get

  override def goodAnswers =  std.goodAnswers.get.toInt
  override def almostCorrectAnswers =  std.almostCorrectAnswers.get.toInt
  override def wrongAnswers =  std.wrongAnswers.get.toInt
  override def averageSolutionTime =  std.averageSolutionTime.get.toDouble

  
  override def goodAnswerPercent: Double = {
    if (isEmpty_?) 0 else f"${100 * std.goodAnswers.get.toDouble / attempts}%.1f".replace(",", ".").toDouble
  }

  override def almostGoodAnswerPercent: Double = {
    if (isEmpty_?) 0 else f"${100 * std.almostCorrectAnswers.get.toDouble / attempts}%.1f".replace(",", ".").toDouble
  }

  override def wrongAnswerPercent: Double = {
    var result = 0.0
    if (! isEmpty_?) result = 100.0 - (goodAnswerPercent + almostGoodAnswerPercent)
    if (result < 0.0) 0.0 else result
  }

  override def isEmpty_? : Boolean = attempts == 0
  override def averageTime: Double = f"${std.averageSolutionTime.get}%.1f".replace(",", ".").toDouble
  override def delete: Unit = std.delete_!

  override def evaluate: Double = {
    val responseCorrectnessWeight = goodAnswerPercent + (almostGoodAnswerPercent * 2) + (wrongAnswerPercent * 3)
    val responseTimeWeight = if (averageTime != 0) log(averageTime) else 0
    val totalPlayTimesWeight = if (attempts != 0) log(attempts) else 0
    responseTimeWeight * responseTimeWeight * totalPlayTimesWeight
  }
}

class MapperStatisticBox(private val cbi: Long, private val cbn: String) extends StatisticBox {
  override def id: Long = cbi
  override def link: NodeSeq = <a href={"/CardBox/" + cbi}>{cbn}</a>
}
