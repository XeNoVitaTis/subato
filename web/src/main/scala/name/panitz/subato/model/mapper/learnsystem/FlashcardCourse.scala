package name.panitz.subato.model.mapper.learnsystem

import net.liftweb.mapper._
import name.panitz.subato.SWrapperL10n
import net.liftweb.util.{FieldError, FieldIdentifier}
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import scala.xml.Text
import scala.xml.NodeSeq
import name.panitz.subato.model.mapper.{MappedWysiwygEditor, Course, LecturerOnlyCRUDify, User, TopicsSplitter => TS}
import scala.util.Random

object FlashcardCourseDateFormatter {

  def get: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy'T'HH:mm")

  def validateDateFormat(identifier: FieldIdentifier) (date: String): List[FieldError] = {
    try {
      LocalDateTime.parse(date, FlashcardCourseDateFormatter.get)
      List[FieldError]()
    } catch {
      case _: Exception => List(FieldError(identifier, "bad format for date"))
    }
  }

}

// TODO: Was ist der Unterschied zwischen Ownerid und User
class FlashcardCourse extends LongKeyedMapper[FlashcardCourse] with IdPK with SWrapperL10n {

  def getSingleton = FlashcardCourse

  def this(title: String, isPrivate: Boolean, ownerId: String, desc: String, topics: Seq[String], creation: LocalDateTime, lastUpdate: LocalDateTime) {
    this()
    this.title set title
    this.isPrivate set isPrivate
    this.owner set ownerId
    this.description set desc
    this.topics set TS.concate(topics)
    this.creationTime set creation.format(FlashcardCourseDateFormatter.get)
    this.lastUpdate   set lastUpdate.format(FlashcardCourseDateFormatter.get)
    this.salt set Random.alphanumeric.take(20).mkString
  }
  def this(title: String, isPrivate: Boolean, ownerId: String, desc: NodeSeq, topics: Seq[String], creation: LocalDateTime, lastUpdate: LocalDateTime) {
    this(title, isPrivate, ownerId, desc+"", topics, creation, lastUpdate)
  }



  def update(newTitle: String, newDescription:String, newTopics: Seq[String], isPrivate: Boolean): Unit = {
    this.title set newTitle
    this.description set newDescription
    this.topics set TS.concate(newTopics)
    this.lastUpdate set LocalDateTime.now.format(FlashcardCourseDateFormatter.get)
    this.isPrivate set isPrivate
    save
  }

  def update(newTitle: String, newDescription:NodeSeq, newTopics: Seq[String], isPrivate: Boolean): Unit = {
    update(newTitle, newDescription+"", newTopics, isPrivate)
  }


  object author extends MappedPoliteString(this, 128) {
    override def defaultValue = ""
  }

  object owner extends MappedPoliteString(this, 128) {
    override def dbIndexed_? = true
  }

  object salt extends MappedPoliteString(this, 20) {
    override def defaultValue: String = Random.alphanumeric.take(20).mkString
  }

  object topics extends MappedPoliteString(this, 1000) {
    override def defaultValue = ""
  }

  object isPrivate extends MappedBoolean(this) {
    override def defaultValue = true
  }

  object title extends MappedPoliteString(this, 512)

  object description extends MappedWysiwygEditor(this, () => locStr("text"))

  object creationTime extends MappedPoliteString(this, 16) {
    override def defaultValue = LocalDateTime.now.format(FlashcardCourseDateFormatter.get)
    override def validations = FlashcardCourseDateFormatter.validateDateFormat(this) _ :: Nil
  }

  object lastUpdate extends MappedPoliteString(this, 16) {
    override def defaultValue = LocalDateTime.now.format(FlashcardCourseDateFormatter.get)
    override def validations = FlashcardCourseDateFormatter.validateDateFormat(this) _ :: Nil
  }

  def updateLastModificationTime: Unit = {
    this.lastUpdate.set(LocalDateTime.now.format(FlashcardCourseDateFormatter.get))
    this.save
  }

  object subscriberNumber extends MappedInt(this) {
    override def defaultValue = 0
  }

  def incrementSubscriberNumber: Unit = {
    this.subscriberNumber.set(this.subscriberNumber.get + 1)
    this.subscriptionNumber.set(this.subscriptionNumber.get + 1)
    this.save
  }

  def decrementSubscriberNumber: Unit = {
    val isDecrementable_? = this.subscriberNumber.get > 0

    if (isDecrementable_?) {
      this.subscriberNumber.set(this.subscriberNumber.get - 1)
      this.save
    }
  }

  object completionNumber extends MappedLong(this) {
    override def defaultValue = 0
  }

  def incrementCompletionNumber(neededSeconds: Double): Unit = {
    this.completionNumber.set(this.completionNumber.get + 1)
    this.completionTimeAverage.set(this computeNewCompletionTimeAverage neededSeconds)
    this.save
  }

  private def computeNewCompletionTimeAverage(seconds: Double): Double = {
    import name.panitz.subato.model.mapper.Calculator

    Calculator.average(this.completionTimeAverage.get, seconds, this.subscriptionNumber.get)
  }

  object completionTimeAverage extends MappedDouble(this) {
    override def defaultValue = 0
  }
  
  object subscriptionNumber extends MappedLong(this) {
    override def defaultValue = 0
  }

}


import net.liftweb.http.RedirectResponse
import net.liftweb.sitemap.Loc.If
import net.liftweb.common.{Full, Box}
object FlashcardCourse
    extends FlashcardCourse
    with LongKeyedMetaMapper[FlashcardCourse]
/* current only lecturers are able to create flashcardcourse */
    with LecturerOnlyCRUDify[Long, FlashcardCourse] {

  override def editMenuLocParams =  super.editMenuLocParams
  override def showAllMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login")) :: super.showAllMenuLocParams
  override def createMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login"))  :: super.createMenuLocParams
  override def deleteMenuLocParams
  = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login"))  :: super.deleteMenuLocParams

  def currentUser = _root_.name.panitz.subato.model.LdapCurrentUser
  private def deleteEditAllowed(b:Box[FlashcardCourse]) = b match {
    case Full(e) =>
      var x ={
        currentUser.uid match {
          case Full(n) =>
            n==e.author.get
          case _ =>
            false
        }
      }
      x|| User.isAdmin
    case _ => false
  }



} 



class SubscribedFlashcardCourse extends LongKeyedMapper[SubscribedFlashcardCourse] with IdPK {

  def getSingleton = SubscribedFlashcardCourse

  def this(subscriber: String, requiredCourse: Long) {
    this()
    this.subscriber.set(subscriber)
    this.flashcardCourse.set(requiredCourse)
    this.subscriptionTime.set(LocalDateTime.now.format(FlashcardCourseDateFormatter.get))
  }

  def markAsCompleted: Unit = {
    this.isCompleted set true
    this.save
  }

  def markAsUncompleted: Unit = {
    this.isCompleted set false
    this.save
  }

  def incrementCompeletions : Unit = {
    this.completionNumber set (this.completionNumber.get + 1)
    this.save
  }

  object userMustBeCongratulated extends MappedBoolean(this) {
    override def defaultValue = false
  }

  object trainingCompleted extends MappedBoolean(this) {
    override def defaultValue = false
  }

  object subscriber extends MappedPoliteString(this, 128) {
    override def dbIndexed_? = true
  }

  object flashcardCourse extends MappedLongForeignKey(this, FlashcardCourse) {
    override def dbIndexed_? = true
  }

  object inExam extends MappedBoolean(this) {
    override def defaultValue = false
  }

  object lastVisitedBox extends MappedEnum(this, MasteryLevel) {
    override def defaultValue = MasteryLevel.One
  }

  object isCompleted extends MappedBoolean(this) {
    override def defaultValue = false
  }

  object completionNumber extends MappedInt(this) {
    override def defaultValue = 0
  }

  object subscriptionTime extends MappedPoliteString(this, 16) {
    override def defaultValue = LocalDateTime.now.format(FlashcardCourseDateFormatter.get)
    override def validations = FlashcardCourseDateFormatter.validateDateFormat(this) _ :: Nil
  }

  object lastLearningSession extends MappedPoliteString(this, 16) {
    override def defaultValue = LocalDateTime.now.format(FlashcardCourseDateFormatter.get)
    override def validations = FlashcardCourseDateFormatter.validateDateFormat(this) _ :: Nil
  }

}

object SubscribedFlashcardCourse extends SubscribedFlashcardCourse with LongKeyedMetaMapper[SubscribedFlashcardCourse] {}


class DeletedFlashcardCourse extends LongKeyedMapper[DeletedFlashcardCourse] with IdPK with SWrapperL10n {

  def getSingleton = DeletedFlashcardCourse

  def this(flashcardCourse: Long, title: String, notify: String) {
    this()
    this.title set title
    this.userToNotify set notify
  }

  object title extends MappedPoliteString(this, 512)

  object userToNotify extends MappedPoliteString(this, 128) {
    override def dbIndexed_? = true
  }

}

object DeletedFlashcardCourse extends DeletedFlashcardCourse with LongKeyedMetaMapper[DeletedFlashcardCourse] {}
