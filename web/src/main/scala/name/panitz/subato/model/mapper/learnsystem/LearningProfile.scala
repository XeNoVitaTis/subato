package name.panitz.subato.model.mapper.learnsystem

import net.liftweb.mapper.{By, IdPK, LongKeyedMapper, LongKeyedMetaMapper, MappedLongForeignKey, MappedPoliteString, MappedBoolean,
  MappedDateTime, MappedString, MappedDouble, MappedInt, MappedLong}
import name.panitz.subato.SWrapperL10n
import net.liftweb.util.{FieldError, FieldIdentifier}
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import scala.xml.Text

class LearningProfile extends LongKeyedMapper[LearningProfile] with IdPK {

  def getSingleton = LearningProfile

  def this(userID: String) {
    this()
    this.uid.set(userID)
  }
 
  object hintsAlreadyRead extends MappedBoolean(this) {
    override def defaultValue = false
  }

  object uid extends MappedPoliteString(this, 1024) {
    override def dbIndexed_? = true
  }

  object learningType extends MappedDouble(this) {}

  def detetiorateLearningType: Unit = {

  }

  def improveLearningType: Unit = {

  }

}

class CompletedFlashcardCourse extends LongKeyedMapper[CompletedFlashcardCourse] with IdPK {
  def getSingleton = CompletedFlashcardCourse

  def this(profileId: Long, flashcardCourseId: Long, score: Double) {
    this()
    profile         set profileId
    flashcardCourse set flashcardCourseId
    completionScore set score
  }

  object profile extends MappedLongForeignKey(this, LearningProfile) {
    override def dbIndexed_? = true
  }

  object flashcardCourse extends MappedLong(this) {
    override def dbIndexed_? = true
  }

  object completionScore extends MappedDouble(this) {}

}

object CompletedFlashcardCourse extends CompletedFlashcardCourse with LongKeyedMetaMapper[CompletedFlashcardCourse]{}

object LearningProfile extends LearningProfile with LongKeyedMetaMapper[LearningProfile]{}
