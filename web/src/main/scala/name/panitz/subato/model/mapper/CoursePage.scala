package name.panitz.subato.model.mapper

import net.liftweb.mapper.MappedPoliteString
import net.liftweb.http.UnauthorizedResponse
import net.liftweb.mapper.By
import net.liftweb.http.RedirectResponse
import net.liftweb.sitemap.Loc.If
import net.liftweb.sitemap.Loc
import net.liftweb.sitemap.Menu
import net.liftweb.common.Full
import net.liftweb.mapper.MappedTextarea
import net.liftweb.mapper.MappedString
import net.liftweb.mapper.MappedBoolean
import net.liftweb.mapper.LongKeyedMetaMapper
import net.liftweb.mapper.MappedLongForeignKey
import net.liftweb.mapper.CRUDify
import net.liftweb.mapper.LongKeyedMapper
import net.liftweb.mapper.IdPK

class CoursePage
    extends LongKeyedMapper[CoursePage]
    with IdPK {

  def getSingleton = CoursePage
	
  object description extends MappedWysiwygEditor(this, ()=>"description")

  object isPublic extends MappedBoolean(this) {
    override def defaultValue = false
  }

  object course extends MappedLongForeignKey(this, Course) {
    override def dbIndexed_? = true

    override def validSelectValues
      = Full(Course.findAll.map(x => (x.id.get, x.name.get)))
  }
  object term extends MappedLongForeignKey(this, Term) {
    override def dbIndexed_? = true
    override def validSelectValues
      = Full(Term.findAll.map(x => (x.id.get, x.addendum.get+" "+x.year.get)))

  }

  object lecturer extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
  }
}

object CoursePage extends CoursePage
    with LongKeyedMetaMapper[CoursePage]
    with LecturerOnlyCRUDify[Long, CoursePage] {
  import net.liftweb.common.Empty
  import net.liftweb.sitemap.Loc.Hidden
//  override def showAllMenuLoc = Empty
  override def createMenuLocParams = Hidden :: super.createMenuLocParams
}
