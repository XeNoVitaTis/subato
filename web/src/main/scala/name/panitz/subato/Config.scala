package name.panitz.subato
import scala.language.postfixOps

import scala.xml._

trait Config {
  def ldapUrl:String
  def users:Seq[(String, String)]
  def usersBaseDir:String
  def exBaseDir:String
  def courseBaseDir:String
  def baseDir:String
  def randomInt:Int
}

trait ConfigDependency {
  protected def config:Config
}

trait XmlConfigDependency extends ConfigDependency {
  override protected def config:Config = XmlConfig
}

private object XmlConfig extends Config {			
  private var _ldapUrl = ""
  override def ldapUrl = _ldapUrl
	
  private var _users:Seq[(String, String)] = Nil
  override def users = _users
	
  private var _usersBaseDir = ""
  override def usersBaseDir = _usersBaseDir
	
  private var _exBaseDir = ""
  override def exBaseDir = _exBaseDir

  private var _courseBaseDir = ""
  override def courseBaseDir = _courseBaseDir


  private var _baseDir = ""
  override def baseDir = _baseDir

  private val _random = new scala.util.Random()
  override def randomInt = _random.nextInt


  //init
  val xml =     XML.loadFile("/var/opt/subato/config.xml")
//    XML.loadFile(sys.env("CONFIG_FILE"))
  
  _ldapUrl = xml \ "ldapUrl" text;

  _baseDir = xml \ "baseDir" text;
  _usersBaseDir = xml \ "usersBaseDir" text;
  _exBaseDir = xml \ "exBaseDir" text;
  _courseBaseDir = xml \ "courseBaseDir" text;



  _users = xml \ "users" \ "user" map (user => {
    (user.attribute("uid").get(0).text,user.attribute("password").get(0).text)
  })
}
