package name.panitz.subato

import scala.xml.Text
import scala.xml.NodeSeq
import net.liftweb.http.S

trait L10n {
  protected def loc(key:String):NodeSeq
  protected def locStr(key:String):String
}

trait SWrapperL10n extends L10n {
  protected override def locStr(key:String) = S.loc(key).map(_.text).openOr(key)
  protected override def loc(key:String) = S.loc(key).openOr(Text(key))	
}
