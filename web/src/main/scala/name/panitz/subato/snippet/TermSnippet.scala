package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._

import scala.xml.{NodeSeq, Text}

class TermSnippet(val c:Box[Term])
    extends AbstractTermSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val term = c
}

trait AbstractTermSnippet {
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val term:Box[Term]
	
  def summary={
    "#title" #> "Semester" &
    "#name *" #>
      (terms
        .findAll.sortWith((x, y) ⇒ x.year>y.year ||(x.year==y.year&&x.addendum>y.addendum)))
      .map((t) => {
        val coursesWithPages = t.courses
          .filter((c) => ( !coursePages.find(c.id,t.id).filter(x=>x.isPublic).isEmpty ))
        val coursesWithSheets = t.courses
          .filter((c) => ((   !c.exerciseSheets(t.id).filter(_.isPublic).isEmpty
                            ||  currentUser.isTutorIn(c.id,t.id) )
                          && coursePages.find(c.id,t.id).filter(x=>x.isPublic).isEmpty ))
          

        
        if (!coursesWithPages.isEmpty || !coursesWithSheets.isEmpty){
           <h3>{t.name}</h3><ul>
        {
          coursesWithPages
           .map((c) => {
             <li><a href={"/CoursePage/"+coursePages.find(c.id,t.id)(0).id}>{c.name}</a></li>}
          )
        }
        {
          coursesWithSheets
           .map((c) => {
             <li><a href={"/Course/"+c.id+"/"+t.id}>{c.name}</a>
              {
                if (currentUser.isLecturerIn(c.id,t.id)){
                  val coursesWithPossiblePrivatePages = coursePages.find(c.id,t.id)
                  <div >
                  <a href={"/ListSheetsForCourseAndTerm/"+c.id+"/"+t.id}>{loc("editSheets")}</a>
                  <a href={"/ListGroupsForCourseAndTerm/"+c.id+"/"+t.id}>{loc("editGroups")}</a>
                  <a href={"/ListTutorsForCourseAndTerm/"+c.id+"/"+t.id}>{loc("editTutors")}</a>
                  { if (!coursesWithPossiblePrivatePages.isEmpty)
                    try {
                      <a href={"/EditCoursePage/"+coursesWithPossiblePrivatePages(0).id}>{loc("editPrivateCoursePage")}</a>
                    }catch{
                      case _:Exception => Text("")
                    }
                    else
                     <a href={"/CreateCoursePageForCourseAndTerm/"+c.id+"/"+t.id}>{loc("createCoursePage")}</a>
                  }</div>
                }
              }
             </li>}
          )
        }
        </ul>}
        else <span />
      })
  }

  def showTerm={
    term match {
      case Full(t) => {
        "#title" #> (t.name) &
        "#course *" #>
        t.courses
          .filter((c) => !c.exerciseSheets(t.id).filter(_.isPublic).isEmpty)
          .map((c) => <a href={"/Course/"+c.id+"/"+t.id}><h3>{c.name}</h3></a><br/><div>{c.description}</div> )
      }
      case _ => loc("noTermSpecified")
    }
  }
}
