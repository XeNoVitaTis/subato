package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}

import scala.xml.{NodeSeq, Text}

class TutorSnippet(val c:Box[Course])
    extends AbstractTutorSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  def this(e:(String,Box[Course])) = {
    this(e._2)
    this.termId = e._1
  }

  override protected var termId:String = null
  override protected val params = ParamsImpl
  override protected val course = c

}

trait AbstractTutorSnippet {
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val course:Box[Course]
  protected var termId:String

  def createTutor={
    var name = ""
    var term:Term=null
    var theCourse:Course=null

    def solutionSubmitted(){
      val create = tutors.createAndSave(name,theCourse,term)
      import net.liftweb.http.S
      S.redirectTo("/ListTutorsForCourseAndTerm/"+theCourse.id+"/"+termId)
    }
    
    (course,terms.find(termId.toLong)) match {
      case (Full(c),Full(t)) => {
        term=t
        theCourse=c
        if (currentUser.isLecturerIn(c)){
          "#course *" #> (c.name+"") &
          "#term *" #> ("("+t.name+")") &
          "#createTutor *" #> loc("createTutorFor") &
          "#name *" #> SHtml.text("", (text)=> name=text)&
          "#hiddenTerm * " #> SHtml.hidden(()=>()) &
          "#hiddenCourse *" #> SHtml.hidden(()=>())&
          "#create *" #> 
             SHtml.button(loc("create"),solutionSubmitted _,("type","submit"))  //,("onclick","startSpinner()"))
        }else{
          "#courseName *" #> (c.name +" "+loc("tutors"))
        }

      }
      case _ =>  "*" #> <span>{loc("noGroupSpecified")}</span>
    }
  }



  def listTutors={
    (course,terms.find(termId.toLong)) match {
      case (Full(c),Full(t)) => {
        "#course *" #> (c.name+"") &
        "#term *" #> ("("+t.name+")") &
        "#create" #> <a href={"/CreateTutorForCourseAndTerm/"+c.id+"/"+t.id}>{loc("createTutor")}</a> &
        "#row *" #>
          tutors.findByCourseAndTerm(c.id,termId.toLong)
            .map(group => <tr>
              <td>{group.name}</td>
              <td><a href={"/istutorin/edit/"+group.id}>{loc("edit")}</a></td>
              <td><a href={"/istutorin/delete/"+group.id}>{loc("delete")}</a></td>
              </tr>
            )
      }
      case _ => loc("noCourseSpecified")
    }
  }
}
