package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}
import scala.xml.{NodeSeq, Text}
import net.liftweb.common.Logger
import net.liftweb.http.SessionVar

class ProposeCardSnippet(val c:Box[ Course ])
    extends AbstractProposeCardSnippet
    with SWrapperL10n
    with XmlConfigDependency 
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val course = c
}

trait AbstractProposeCardSnippet extends StatefulSnippet
    with name.panitz.subato.XmlConfigDependency
    with Logger{
  this:ModelDependencies
  with L10n  =>

  protected val params:Params
  protected val course:Box[ Course]
  protected def config:Config

  override def dispatch = {
    case "Form" => showForm
  }

  import name.panitz.subato.model.mapper.CardType
  def showForm = {
    (course) match {
      case Full(ex) => {
        var answers:List[(String,Boolean)]=List()
        var question=""
        var explanation=""
        var cardType=CardType.MultipleChoice

        def submitted = {
        }

        val answer = <tr>
        <td>{SHtml.textarea( "", s => { explanation = s }, "style" -> "height: 1em; width: 100%;")}</td>
        <td>{SHtml.checkbox( false,s=> {})}</td>
        </tr>

        "#cardType" #> SHtml.select(
            CardType.values.map(v => new SHtml.SelectableOption(v+"",v+"")).toList
          , Full(CardType.MultipleChoice+"")
          , (s) => {cardType = CardType.withNameOpt(s).get}) &
        "#question"    #> SHtml.textarea( "", s => { question = s }, "style" -> "height: 450px; width: 100%;")&
        "#submit *"    #> SHtml.submit(locStr("submit"), submitted _) &
        "#explanation" #> SHtml.textarea( "", s => { explanation = s }, "style" -> "height: 450px; width: 100%;")&
        "#answers *"   #> <table>{List(answer,answer,answer,answer,answer,answer,answer,answer,answer,answer)}</table>
        }
      case _ =>  "*" #> <span>{loc("noCourseSpecified")}</span>
      }
    
  }


}
