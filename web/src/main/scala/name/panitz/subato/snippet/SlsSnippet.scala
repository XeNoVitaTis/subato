package name.panitz.subato.snippet

import net.liftweb.http.{S, SHtml, SessionVar, StatefulSnippet}
import name.panitz.subato.model.{ModelDependencies, MapperModelDependencies}
import name.panitz.subato.model.learnsystem.{FlashcardCourse, SubscribedFlashcardCourse, FinalGrade}
import name.panitz.subato._
import scala.xml.{NodeSeq, Elem, XML}
import net.liftweb.util.{CssSel}
import net.liftweb.http.js.{JsCmds, JsCmd, JsExp, JE}
import net.liftweb.util.Helpers._
import net.liftweb.http.GUIDJsExp
import net.liftweb.common.{Box, Full, Logger}


object CourseFilter extends Enumeration {
  type CourseFilter = Value

  val MY_COURSES      = Value(1)
  val STARRED_COURSES = Value(2)
  val EXPLORE_COURSES = Value(3)
}

import CourseFilter._
import FinalGrade._
trait AbstractSlsSnippet extends StatefulSnippet with Logger with ClienSideRefresher {
  this: ModelDependencies with L10n =>

  val COURSES_PER_PAGE = 15
  val SLS_URL = "/sls"
  val COURSE_CONTAINER_ID = "listOfCourses_"
  val warningTxtForLambdaUser = """
      <span>Since you are <b>not logged in</b> you are considered an <b>anonymous user</b>:
        <ul style='text-align: left; margin-top: 15px;'>
          <li>your learning progress is <b>not persisted</b>.</li>
          <li>You can't access <b>SLS Studio</b> to create your own flashcards.</li>
        </ul>
      </span>
  """

  override def dispatch: PartialFunction[String, CssSel] = {
    case "listCourses"    => showNews ; listFlashcardCourses
    case "initSearchBar"  => showNews ; initSearchBar
    case "initSlsBar"     => showNews ; initSlsBar
    case "initNavButtons" => showNews ; createCoursesNavButtonsByNeed
    case "activateFinder" => showNews ; activateFinder
  }

  object myCoursesFilter extends SessionVar[Boolean](false)

  object warningForLambdaUserDisplayed extends SessionVar[Boolean](false)

  object starredCoursesFilter extends SessionVar[Boolean](false)

  object pr extends SessionVar[Int](0)

  object exploreCourseFilter extends SessionVar[Boolean](true)

  object displayableCourses extends SessionVar[Seq[(FlashcardCourse, SubscribedFlashcardCourse)]](Seq.empty)

  object displayableCoursesOffset extends SessionVar[Int](0)

  def switchFilters(active: CourseFilter): JsCmd = {
    active match {
      case MY_COURSES => {
        myCoursesFilter.set(true)
        starredCoursesFilter.set(false)
        exploreCourseFilter.set(false)
      }
      case STARRED_COURSES => {
        myCoursesFilter set false
        starredCoursesFilter set true
        exploreCourseFilter set false
      }
      case _ => {
        myCoursesFilter.set(false)
        starredCoursesFilter.set(false)
        exploreCourseFilter.set(true)
      }
    }
    actualizeDisplayableCourseList
    updateListOfCoursesOnUI
  }

  def activateFinder: CssSel = {
    def isUserAllowedToImportCourses_? : Boolean = {
      try {
        currentUser.isAdmin || currentUser.isLecturer
      } catch {
        case _: Exception => false
      }
    }

    def activeAbilityToImportSLSCourse: CssSel = {
      if (isUserAllowedToImportCourses_?) {
        SlsCourseImporter.addAbility
      } else {
        "#slsCourseImport [style]" #> "display: none;"
      }
    }

    val eventCatcher = SHtml onEvent { actualizeDisplayableCourseList(_) }
    "#iFinder [onchange]" #> eventCatcher & activeAbilityToImportSLSCourse
  }

  def actualizeDisplayableCourseList(requierment: String): JsCmd = {
    def searchFilter: FlashcardCourse => Boolean = {
      import scala.util.matching.Regex
      val topicExtractor      = """.*t:(.*)""".r
      val courseNameExtractor = """.*n:(.*)""".r
      val ownerExtractor      = """.*o:(.*)""".r
      
      def extract(container: String, extractor: Regex): String = {
        val defaultValue = ""
        container match {
          case extractor(value) => value
          case _                => defaultValue
        }
      }

      val topic = extract(requierment, topicExtractor).trim.toLowerCase
      val courseName = extract(requierment, courseNameExtractor).trim.toLowerCase
      val owner = extract(requierment, ownerExtractor).trim.toLowerCase
      val defaultFilterIsActive_? = topic.isEmpty && courseName.isEmpty && owner.isEmpty
      lazy val defaultFilter: FlashcardCourse => Boolean = _.title.toLowerCase contains requierment.trim.toLowerCase
      lazy val defaultPred: FlashcardCourse => Boolean = _ => true 
      lazy val topicPred: FlashcardCourse => Boolean      = if (topic.isEmpty)      { defaultPred } else { _.topics exists { _.toLowerCase contains topic }}
      lazy val courseNamePred: FlashcardCourse => Boolean = if (courseName.isEmpty) { defaultPred } else { _.title.toLowerCase   contains courseName }
      lazy val ownerPred: FlashcardCourse => Boolean      = if (owner.isEmpty)      { defaultPred } else { _.ownerId.toLowerCase contains owner }
      if (defaultFilterIsActive_?) defaultFilter else x => courseNamePred(x) && topicPred(x) && ownerPred(x)
    }
    if (requierment.trim.isEmpty) {
      updateListOfCoursesOnUI
    } else {
      val displayables = accordingToFiltersQualifiedCourses
      val filteredCourses = displayables filter { case (c, _) => searchFilter apply c } take COURSES_PER_PAGE
      val nodes = filteredCourses map { case (x, y) => x.formatForDisplay(currentUser, this, y) }
      val hideNavButtons = hide("prevBtn", "nextBtn")
      refresh(COURSE_CONTAINER_ID, nodes) & hideNavButtons & activeComplexTooltips
    }
  }

  def actualizeDisplayableCourseList: Unit = {
    displayableCourses.set(accordingToFiltersQualifiedCourses)
    displayableCoursesOffset.set(0)
  }

  def accordingToFiltersQualifiedCourses: Seq[(FlashcardCourse, SubscribedFlashcardCourse)] = {

    def forLoggedInUserQualifiedCourses(uid: String): Seq[(FlashcardCourse, SubscribedFlashcardCourse)] = {
      if (myCoursesFilter.get) {
        val subscriptions = flashcardCourses findAllSubscribedBy uid
        flashcardCourses findAllCreatedBy uid map { toFlashcardCouresTuple(subscriptions) }
      } else if (starredCoursesFilter.get) {
        flashcardCourses.findAllSubscribedBy(uid)
          .map { _.toFlashcardCourse }
          .foldRight(Seq.empty[(FlashcardCourse, SubscribedFlashcardCourse)]) {
            case (n, result) => if (isVisibleForUser_?(uid, n)) (n.openOrThrowException("") +: result) else result
          }
      } else {
        val subscriptions = flashcardCourses findAllSubscribedBy uid
        flashcardCourses.findAllAccessibleBy(uid) map { toFlashcardCouresTuple(subscriptions) }
      }
    }

    def isVisibleForUser_?(uid: String, item: Box[(FlashcardCourse, SubscribedFlashcardCourse)]): Boolean = {
      item match {
        case Full((fcc, _)) => (!fcc.isPrivate_?) || (fcc.ownerId == uid)
        case _              => false
      }
    }

    def toFlashcardCouresTuple (subscriptions: Seq[SubscribedFlashcardCourse]) (fcc: FlashcardCourse) = {
      val foundSubscription = subscriptions find {_ isLinkedTo fcc }
      (fcc, foundSubscription.getOrElse(null))
    }

    def forNonLoggedInUserQualifiedCourses: Seq[(FlashcardCourse, SubscribedFlashcardCourse)] = {
      if (exploreCourseFilter.get)
        flashcardCourses.findAllAccessibleBy("") map { x  => (x, null) }
      else
        Seq.empty
    }

    val uid = currentUser.uid.getOrElse("")
    val result = if (currentUser.loggedIn) forLoggedInUserQualifiedCourses(uid) else forNonLoggedInUserQualifiedCourses
    result sortBy { case (fcc, _) => fcc.title }
  }

  def createCoursesNavButtonsByNeed: CssSel = {
    val isNextButtonNeeded_? : Boolean = offsetIsIncrementable_?
    val isPrevButtonNeeded_? : Boolean = offsetIsDecrementable_?

    lazy val hideNextButton: CssSel = "#nextBtn [style]" #> "display: none;"
    lazy val hidePrevButton: CssSel = "#prevBtn [style]" #> "display: none;"

    val setNextButtonFunctionality: CssSel = "#nextBtn [onclick]" #> SHtml.ajaxInvoke { () => incrementIndexOfLastDisplayedCourse }
    val setPrevButtonFunctionality: CssSel = "#prevBtn [onclick]" #> SHtml.ajaxInvoke { () => decrementIndexOfLastDisplayedCourse }

    lazy val activateButtonGroup: CssSel   = "#navBtnGr [class]" #> "btn-group"
    lazy val deactivateButtonGroup: CssSel = "#navBtnGr [class]" #> ""

    val nextBtn = if (isNextButtonNeeded_?) setNextButtonFunctionality else hideNextButton & setNextButtonFunctionality
    val prevBtn = if (isPrevButtonNeeded_?) setPrevButtonFunctionality else hidePrevButton & setPrevButtonFunctionality
    val group   = if (isNextButtonNeeded_? && isPrevButtonNeeded_?) activateButtonGroup else deactivateButtonGroup
    nextBtn & prevBtn & group
  }

  def incrementIndexOfLastDisplayedCourse: JsCmd = {
    if (offsetIsIncrementable_?) displayableCoursesOffset.set(displayableCoursesOffset.get + COURSES_PER_PAGE)
    updateListOfCoursesOnUI
  }

  def offsetIsIncrementable_? : Boolean = (displayableCoursesOffset.get + COURSES_PER_PAGE) < displayableCourses.get.size

  def decrementIndexOfLastDisplayedCourse: JsCmd = {
    if (offsetIsDecrementable_?) displayableCoursesOffset.set(displayableCoursesOffset.get - COURSES_PER_PAGE)
    updateListOfCoursesOnUI
  }

  def updateListOfCoursesOnUI: JsCmd = {
    val from = displayableCoursesOffset.get
    val to = from + COURSES_PER_PAGE
    val displayables = accordingToFiltersQualifiedCourses slice (from, to)
    val nodes = displayables map { case (c, y) => c.formatForDisplay(currentUser, this, y) }
    val isNextButtonNeeded_? = offsetIsIncrementable_?
    val isPrevButtonNeeded_? = offsetIsDecrementable_?
    val showOrHideNextButton = if (isNextButtonNeeded_?) show("nextBtn") else hide("nextBtn")
    val showOrHidePrevButton = if (isPrevButtonNeeded_?) show("prevBtn") else hide("prevBtn")
    val group =
      if (isNextButtonNeeded_? && isPrevButtonNeeded_?) setAttr("navBtnGr", "class", "btn-group")
      else setAttr("navBtnGr", "class", "")
    refresh(COURSE_CONTAINER_ID, nodes) & showOrHideNextButton & showOrHidePrevButton & group & activeComplexTooltips
  }

  def offsetIsDecrementable_? : Boolean = displayableCoursesOffset.get >= COURSES_PER_PAGE

  def listFlashcardCourses: CssSel = {
    actualizeDisplayableCourseList
    val from = displayableCoursesOffset.get
    val to   = from + COURSES_PER_PAGE
    val displayables = displayableCourses.get
    val qualified    = displayables.sortBy(courses => courses._1.title.toLowerCase) slice (from, to)
    if ((!currentUser.loggedIn) && (!warningForLambdaUserDisplayed.get)) {
      val warningForLambdaUser = XML loadString warningTxtForLambdaUser.trim
      PrettyAlerter.setMsg(new SimpleAlert(Severity.Warn, "λ User", warningForLambdaUser, "OK", false))
      warningForLambdaUserDisplayed set true
    }
    "#listOfCourses" #> (qualified map { case (course, subscription) => course formatForDisplay (currentUser, this, subscription) })
  }

  private def createUserButton: CssSel = {
    val logoutButton = <div class="dropdown-divider"></div><a class="dropdown-item" href="/user_mgt/logout">Abmelden</a>
    val maybeLogoutButton = if (currentUser.loggedIn) logoutButton else NodeSeq.Empty

    "#userAvatar [data-jdenticon-value]" #> currentUser.uid.getOrElse("no name") &
    "#logoutBtn" #> maybeLogoutButton
  }


  private def mkStudioLink: CssSel = {
    "#studio" #> {if (currentUser.isLecturer) <a class="nav-link" href="/SlsStudio">Studio</a> else <span />}
  }
  private def mkStatisticsLink: CssSel = {
    "#statistics" #> {if (currentUser.isLecturer) <a class="nav-link" href="/SlsStatistics">{loc("coursestatistics")}</a>
                      else <span />}
  }
  private def addHelpForUser: CssSel = {
    null
  }

  private def addHintsForUser: CssSel = "#hintsForUser [onclick]" #> SHtml.onEvent((_) => Alerts.slsHints)

  def initSearchBar: CssSel = {
    def activeFilterOnUI: CssSel = {
      val filterId = if (myCoursesFilter.get) "#myCoursesFilter"
                     else if (starredCoursesFilter.get) "#starredCoursesFilter"
                     else "#exploreCourseFilter"
      s"$filterId [class+]" #> "active"
    }

    activeFilterOnUI &
    "#myCoursesFilter [onclick]"      #> SHtml.ajaxInvoke { () => switchFilters(MY_COURSES)      } &
    "#starredCoursesFilter [onclick]" #> SHtml.ajaxInvoke { () => switchFilters(STARRED_COURSES) } &
    "#exploreCourseFilter [onclick]"  #> SHtml.ajaxInvoke { () => switchFilters(EXPLORE_COURSES) }
  }

  def initSlsBar: CssSel = createUserButton & addHintsForUser & mkStudioLink & mkStatisticsLink

  def addNextButtonByNeed: CssSel = {
    null
  }

  override def refresh: JsCmd = {
    val nodes = accordingToFiltersQualifiedCourses map { case (c, y) => c.formatForDisplay(currentUser, this, y) } take COURSES_PER_PAGE
    refresh(COURSE_CONTAINER_ID, nodes) & setValue("iFinder", "")
  }

  def showNews: Unit = {

     def isVisibleForUser_?(uid: String, item: Box[(FlashcardCourse, SubscribedFlashcardCourse)]): Boolean = {
      item match {
        case Full((fcc, _)) => (!fcc.isPrivate_?) || (fcc.ownerId == uid)
        case _              => false
      }
     }

    val uid = currentUser.uid.getOrElse("")
    val courses = flashcardCourses.findAllSubscribedBy(uid)
      .map { _.toFlashcardCourse }
      .foldRight(Seq.empty[(FlashcardCourse, SubscribedFlashcardCourse)]) {
        case (n, result) => if (isVisibleForUser_?(uid, n)) (n.openOrThrowException("") +: result) else result
      }
   
    val namesOfdeletedSubscribedCourses = deletedFlashcardcourses namesOfDeletionsThatConcern uid
    val updatedCourses = courses filter { case (fcc, sfcc) => (!fcc.isOwner(uid)) && (sfcc.lastLearningSession isBefore fcc.lastUpdate)}
    val news =  updatedCourses.map { case (x, _) => <li>Course {">>" + x.name + "<<"} was updated </li> } ++ namesOfdeletedSubscribedCourses.map {x => <li>Course {">>" + x + "<<"} was deleted</li>}
   
    if (news.nonEmpty)
      PrettyAlerter.setMsg(new SimpleAlert(Severity.Info, "News", <ul>{news}</ul>, "OK", false))
  }
}

class SlsSnippet extends AbstractSlsSnippet with MapperModelDependencies with SWrapperL10n {}
