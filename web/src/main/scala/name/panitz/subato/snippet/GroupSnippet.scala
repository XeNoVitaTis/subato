package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}

import scala.xml.{NodeSeq, Text}


class GroupSnippet(val c:Box[Course])
    extends AbstractGroupSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  def this(e:(String,Box[Course])) = {
    this(e._2)
    this.termId = e._1
  }

  override protected var termId:String = null
  override protected val params = ParamsImpl
  override protected val course = c

}

trait AbstractGroupSnippet {
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val course:Box[Course]
  protected var termId:String


  def createGroup={
    var name = ""
    var term:Term=null
    var theCourse:Course=null

    def solutionSubmitted(){
      val create = groups.createAndSave(name,theCourse,term)
      import net.liftweb.http.S
      S.redirectTo("/ListGroupsForCourseAndTerm/"+theCourse.id+"/"+termId)
    }
    
    (course,terms.find(termId.toLong)) match {
      case (Full(c),Full(t)) => {
        term=t
        theCourse=c
        if (currentUser.isLecturerIn(c)){
          "#course *" #> (c.name+"") &
          "#term *" #> ("("+t.name+")") &
          "#createGroup *" #> loc("createGroupFor") &
          "#name *" #> SHtml.text("", (text)=> name=text)&
          "#hiddenTerm * " #> SHtml.hidden(()=>()) &
          "#hiddenCourse *" #> SHtml.hidden(()=>())&
          "#create *" #> 
             SHtml.button(loc("create"),solutionSubmitted _,("type","submit"))  //,("onclick","startSpinner()"))
        }else{
          "#courseName *" #> (c.name +" "+loc("groups"))
        }

      }
      case _ =>  "*" #> <span>{loc("noGroupSpecified")}</span>
    }
  }



  def listGroups={
    (course,terms.find(termId.toLong)) match {
      case (Full(c),Full(t)) => {
        "#course *" #> (c.name+"") &
        "#term *" #> ("("+t.name+")") &
        "#create" #> <a href={"/CreateGroupForCourseAndTerm/"+c.id+"/"+t.id}>{loc("createGroup")}</a> &
        "#row *" #>
          groups.findByCourseAndTerm(c.id,termId.toLong)
            .map(group => <tr>
              <td>{group.name}</td>
              <td>{group.weekday+" "+group.time}</td>
              <td>{group.room}</td>
              <td>{group.lecturer}</td>
              <td><a href={"/group_t/edit/"+group.id}>{loc("edit")}</a></td>
              <td><a href={"/group_t/delete/"+group.id}>{loc("delete")}</a></td>
              </tr>
            )
      }
      case _ => loc("noCourseSpecified")
    }
  }



}
