package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._

import scala.xml.{NodeSeq, Text}

class GroupDetailSnippet(val c:Box[Group])
    extends AbstractGroupDetailSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }

  override protected val params = ParamsImpl
  override protected val group = c
}

trait AbstractGroupDetailSnippet {
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val group:Box[Group]

  def listMember={
    var theGroup:Group=null

    (group) match {
      case (Full(g)) => {
        theGroup = g
        if (theGroup.lecturer==currentUser.uid.openOr("--------")){
          var cp = theGroup.coursePage
          "#row  " #> theGroup.member.sortBy(_.name).map(m => (<tr><td><a href={"/CoursePageStudentResults/"+cp.id+"/"+m.name}>{m.name}</a></td><td>{m.group.name}</td></tr>))
        }else{
          "#row  " #> Text("not allowed")
        }
      }
      case _ =>  "*" #> <span>{loc("noGroup")}</span>
    }
  }
}
