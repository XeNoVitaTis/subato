package name.panitz.subato.snippet

import net.liftweb.http.js.{JsCmds, JsCmd}
import name.panitz.subato.SWrapperL10n
import name.panitz.subato.model.learnsystem.{FinalGrade, MapperFlashcardCourses}
import Severity._
import scala.xml.Node
import scala.language.implicitConversions
import FinalGrade._

sealed trait Alert {
  def toJsCmd: JsCmd = JsCmds Run toJsCode
  def toJsCode: String
  def >>(next: Alert): Alert
  def nonEmpty: Boolean
}


object Alert {
  implicit def toJsCmd(alert: Alert): JsCmd =  alert.toJsCmd
  implicit def toString(alert: Alert): String = alert.toJsCode
}

sealed case class AlertSeq(alerts: Seq[Alert] = Nil) extends Alert {

  def assembleJsCode(liAlerts: Seq[Alert]): String = {
    liAlerts match {
      case alert +: Nil  => alert.toJsCode
      case alert +: tail => s"${alert.toJsCode}.then(_ => {if (_) ${assembleJsCode(tail)} ;})"
      case _               => ""
    }
  }

  def toJsCode: String = assembleJsCode(alerts)

  override def >>(next: Alert): Alert = AlertSeq(alerts :+ next)

  override def nonEmpty: Boolean = alerts.nonEmpty
}

sealed class SimpleAlert(val severity: Severity,
  val title: String,
  val txt: String,
  val content: Node,
  val confirmButton: String,
  val withSkipOption_? : Boolean = false) extends Alert {

  def this(severity: Severity, title: String, txt: String, confirmButton: String, withSkipOption_? : Boolean) {
    this(severity, title, txt, null, confirmButton, withSkipOption_?)
  }

  def this(severity: Severity, title: String, content: Node, confirmButton: String, withSkipOption_? : Boolean) {
    this(severity, title, "", content, confirmButton, withSkipOption_?)
  }

  def this(title: String, content: Node) = {
    this(Severity.Success, title, content, "OK", false)
  }

  override def nonEmpty: Boolean = true

  private def containsHtmlNode_? : Boolean = content != null

  def toJsCode: String = {
    val contentAsString = if (containsHtmlNode_?) content.toString else ""
    val jsSkipOption = if (withSkipOption_?) ", cancel: \"Skip\"" else ""
    "swal({" +
      "content: $.parseHTML(`" + contentAsString + "`)[0]," + 
      "title: \"" + title  + "\"," +
      "closeOnClickOutside: false," +
      "icon: `" + severity + "`," +
      "text: `" + txt + "`," +
      "buttons: {" +
        "confirm: {text: `" + confirmButton + "`, value: true}" +
        jsSkipOption +
      "}" + 
    "})"
  }

  override def >>(next: Alert): Alert = AlertSeq(Seq(this, next))
}

object Alerts extends SWrapperL10n {

  def slsHints: Alert = {
    val title = loc("goodToKnow") + ""
    val alerts = Seq(
      new SimpleAlert(Info, title, loc("learningCoursePersonalSpaceTip") + "", "OK", true),
      new SimpleAlert(Info, title, loc("learningCourseCreationTip") + "", "OK", true),
      new SimpleAlert(Info, title, loc("learningCourseAktualisationTip") + "", "OK", true),
      new SimpleAlert(Info, title, loc("learningCourseHowToTip") + "", "OK", true),
      new SimpleAlert(Info, title, loc("notShowAnymore") + "", "OK", false)
    )
    AlertSeq(alerts)
  }

  def buildCourseCongratulation(grade: FinalGrade): Alert = buildCourseCongratulation(FinalGrade.toStars(grade))

  private def buildCourseCongratulation(stars: Int): Alert = new SimpleAlert("Course completed!", starsHtmlTag(stars))

  private def starsHtmlTag(stars: Int): Node = {
    val emptyStar = MapperFlashcardCourses.emptyStarPattern
    val fullStar  = MapperFlashcardCourses.fullStarPattern

    if      (stars <= 0) <span> {emptyStar} {emptyStar} {emptyStar} </span>
    else if (stars == 1) <span> {fullStar}  {emptyStar} {emptyStar} </span>
    else if (stars == 2) <span> {fullStar}  {fullStar}  {emptyStar} </span>
    else                 <span> {fullStar}  {fullStar}  {fullStar}  </span> 
  }
}
