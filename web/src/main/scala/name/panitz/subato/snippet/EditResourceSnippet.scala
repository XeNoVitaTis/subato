package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._
import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{SHtml, StatefulSnippet}

import scala.xml.{Elem, Node, NodeSeq, Text}
import net.liftweb.common.Logger
import net.liftweb.http.SessionVar

import scala.xml.transform.RewriteRule

class EditResourceSnippet(val c: Box[Exercise])
  extends AbstractEditResourceSnippet
    with SWrapperL10n
    with XmlConfigDependency
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }

  def this(e: (String, Box[Exercise])) = {
    this(e._2)
    this.fileName = e._1
  }

  override protected var fileName: String = null
  override protected val params = ParamsImpl
  override val exercise = c
}

trait AbstractEditResourceSnippet extends StatefulSnippet
  with name.panitz.subato.XmlConfigDependency
  with Logger {
  this: ModelDependencies
    with L10n =>

  protected val params: Params
  protected val exercise: Box[Exercise]
  protected var fileName: String

  protected def config: Config

  override def dispatch = {
    case "editForm" => editForm
    case "deleteForm" => deleteForm
  }


  def deleteForm = {
    (exercise) match {
      case Full(ex) => {
        val fileN = fileName.replace("___", ".")
        val fileBase = fileN split '.'

        def solutionSubmitted = {
          var filesToSave = ex.getTestFiles()
          val selector = " name="+ fileBase(0) +" lang="+ fileBase(1)
          val trans = selector #> ""
          val z = trans(filesToSave)

          ex.setTestFiles(z.toString())
          ex.save()

          redirectTo("/Exercise/" + ex.id + "/" + ex.course.id)
        }

        def abbrechenSubmitted = {
          redirectTo("/Exercise/" + ex.id + "/" + ex.course.id)
        }


        "#exName *" #> ex.name &
          "#resourceFile *" #> (loc("reallyDeleteFile") + " " + fileName) &
          "#abbrechen *" #> {
            SHtml.button(loc("abbrechen"), abbrechenSubmitted _, ("type", "submit"))

          } &
          "#submit *" #> {
            if (currentUser.loggedIn && (currentUser.isTutorIn(ex.course) || currentUser.uid == ex.owner)) {
              SHtml.button(loc("delete"), solutionSubmitted _, ("type", "submit"), ("onclick", "startSpinner()"))
            } else {
              <span>
                {locStr("cannotSubmit")}
              </span>
            }
          }
      }
      case _ => "*" #> <span>
        {loc("noExerciseSpecified")}
      </span>

    }
  }

  def editForm = {
    (exercise) match {
      case Full(ex) => {
        val fileN = fileName.replace("___", ".")
        val fileBase = fileN split '.'
        var content = (ex.getTestFiles() \ "class").find(x => ((x \@ "name") + "." + (x \@ "lang")).equals(fileN)).map(_.text).orNull


        def solutionSubmitted = {
          //val filesToSave = (ex.getTestFiles() \ "class").filter(x => (x \@ "name") + "." + (x \@ "lang") != fileN)
          var filesToSave = ex.getTestFiles()
          val selector = " name="+ fileBase(0) +" lang="+ fileBase(1)+ " *"
          val trans = selector #> content
          val z = trans(filesToSave)

          System.out.println(z.toString())
          ex.setTestFiles(z.toString())
          ex.save()

          redirectTo("/Exercise/" + ex.id + "/" + ex.course.id)
        }


        "#javascript" #> <script language="javascript" type="text/javascript">
          <![CDATA[
      editAreaLoader.init(
        {id : "code"	                // textarea id
        ,syntax: "java"	                // syntax to be uses for highglBiting
        ,start_highlight: true	        // to display with highlight mode on start-up
        ,replace_tab_by_spaces: 2
        }
      );
   ]]>
        </script> &
          "#exName *" #> ex.name &
          "#submit *" #> {
            if (currentUser.loggedIn && (currentUser.isTutorIn(ex.course) || currentUser.uid == ex.owner)) {
              SHtml.button(loc("submitSolution"), solutionSubmitted _, ("type", "submit"), ("onclick", "startSpinner()"))
            } else {
              <span>
                {locStr("cannotSubmit")}
              </span>
            }
          } &
          "#solution" #>
            SHtml.textarea(
              content
              , s => {
                content = s
              }
              , "id" -> "code"
              , "style" -> "height: 450px; width: 100%;"
            )
      }
      case _ => "*" #> <span>
        {loc("noExerciseSpecified")}
      </span>


    }
  }
}

