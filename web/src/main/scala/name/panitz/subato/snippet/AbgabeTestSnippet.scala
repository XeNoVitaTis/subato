package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}
import scala.xml.{NodeSeq, Text}
import net.liftweb.common.Logger
import net.liftweb.http.SessionVar

class AbgabeTestSnippet(val c:Box[ Exercise ])
    extends AbstractAbgabeTestSnippet
    with SWrapperL10n
    with XmlConfigDependency 
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }

  def this(e:(String,Box[Exercise])) = {
    this(e._2)
    this.submissionId = e._1
  }

  override protected var submissionId:String = null
  override protected val params = ParamsImpl
  override val exercise = c
}

trait AbstractAbgabeTestSnippet extends StatefulSnippet
    with name.panitz.subato.XmlConfigDependency
    with Logger{
  this:ModelDependencies
  with L10n  =>

  protected val params:Params
  protected val exercise:Box[ Exercise]
  protected var submissionId:String

  protected def config:Config
  private object results extends SessionVar[Box[(Submission)]](Empty)

  override def dispatch = {
    case "TestSolution" => testSolution
  }

  def testSolution = {
    (exercise) match {
      case Full(ex) => {
        val userDir =
          if (currentUser.loggedIn){
              config.usersBaseDir +
                "/" + currentUser.uid.openOrThrowException("no current user found") +
                "/" + ex.id + "/"
          }else "/dev/null"

//        results.set(Full(ex.evaluator.testSolution(ex.solution, userDir, ex, courseId.toLong, ex.latestTerm.id, null,false)))
        redirectTo("/Results")
      }
      case _ =>  "*" #> <span>{loc("noExerciseSpecified")}</span>
    }
  }

}
