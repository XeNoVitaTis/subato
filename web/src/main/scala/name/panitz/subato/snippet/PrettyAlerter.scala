package name.panitz.subato.snippet

import name.panitz.subato.SWrapperL10n
import name.panitz.subato.model.MapperModelDependencies
import net.liftweb.http.{SessionVar, StatefulSnippet}
import net.liftweb.util.CssSel
import net.liftweb.util.Helpers._

object Severity extends Enumeration {
  type Severity = Value

  val Info    = Value("info")
  val Warn    = Value("warning")
  val Error   = Value("error")
  val Success = Value("success")
}

import name.panitz.subato.snippet.Severity._

trait AbstractPrettyAlerter extends SWrapperL10n with StatefulSnippet with MapperModelDependencies {
  def show: CssSel
  def setMsg(title: String, msg: String, severity: Severity): Unit
  def setMsg(alert: Alert): Unit
  def display_sls_welcome_msg_by_need: CssSel
}

object PrettyAlerter extends AbstractPrettyAlerter {

  object alerts extends SessionVar[Alert](AlertSeq())

  override def setMsg(title:String, msg: String, severity: Severity): Unit = {
    val alert = new SimpleAlert(severity, title, msg, "OK", false)
    setMsg(alert)
  }

  override def setMsg(alert: Alert): Unit = alerts.set(alerts.get >> alert)

  private def reinitialize(): Unit = alerts set AlertSeq()

  /**
    * The script-tag will be injected in an html and the browser
    * run the script content.
    */
  override def show: CssSel = {
    if (alertCanBeDisplayed_?) {
      val code = alerts.get
      this.reinitialize()
      injectAsJsScript(code)
    } else {
      injectAsJsScript("")
    }
  }

  private def alertCanBeDisplayed_? : Boolean = alerts.get.nonEmpty
  
  override def dispatch: PartialFunction[String, CssSel] = {
    case "push"       => show
    case "sayWelcome" => display_sls_welcome_msg_by_need
  }

  override def display_sls_welcome_msg_by_need: CssSel = {
    val title = loc("goodToKnow") + ""
    lazy val code: String = Alerts.slsHints.toJsCode
    if (hintsCanBeDisplayed_?) injectAsJsScript(code) else injectAsJsScript("")
  }

  private def injectAsJsScript(code: String): CssSel = "#whatsNewScript *+" #> code

  def hintsCanBeDisplayed_? : Boolean = {
    if(currentUser.loggedIn) {
      val maybeLP = learningProfiles.findOrCreate(currentUser.uid.getOrElse(""))
      val displayable = maybeLP.nonEmpty && maybeLP.get.hintsShouldBeDisplayed_?
      if (displayable) maybeLP.get.deactivateDisplayHints
      displayable
    } else {
      false
    }
  }

}
