package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato.model.mapper.CardBoxEntry
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml, FileParamHolder, S }
import name.panitz.subato.snippet

import scala.xml.{NodeSeq, Text}

class ImportSurveySnippet(val c:Box[Course])
    extends AbstractImportSurveySnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val course = c
}

trait AbstractImportSurveySnippet extends  XmlConfigDependency{
  this:ModelDependencies
      with L10n
      =>

  protected val params:Params
  protected val course:Box[Course]

  def importSurvey={
    var fileHolder : FileParamHolder = null
    def exerciseSubmitted = {
      fileHolder match {
      // An empty upload gets reported with a null mime type,
      // so we need to handle this special case
        case FileParamHolder(_, "text/xml", _, data) => {
          val xml =  scala.xml.XML.load(fileHolder.fileStream)
          val Full(c) = course
          val Full(uid) = currentUser.uid
          val surv = surveys.create(c.id,(xml\\"surveyName").text)
          val cards =  xml \\ "card"
          val tuple = CardCreation.byNeed(cards(0), course)//createCardFromXML(cards(0))

          surv.card = tuple._1
          surv.card.isSurveyCard = true
          surv.save

          S.redirectTo("/AdminSurveys")
        }
        case _ => {
          S.error("Invalid receipt attachment")
          false
        }
       }
    }

    (course) match {
      case (Full(c)) => {
        "#course" #> (c.name) &
        "#file *" #> SHtml.fileUpload(fileHolder = _) &
        "#submit *" #> SHtml.submit(locStr("importSurvey"), exerciseSubmitted _) 
      }
      case _ => loc("noCourseSpecified")
    }
  }
}
