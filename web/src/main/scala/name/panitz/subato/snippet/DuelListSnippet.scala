package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._

import scala.xml.{NodeSeq, Text}


class DuelListSnippet
    extends AbstractDuelListSnippet
    with SWrapperL10n
    with MapperModelDependencies {
}

trait AbstractDuelListSnippet {
  this:ModelDependencies
      with L10n =>

  def list:net.liftweb.util.CssSel={
    def renderDuelList(uid:String,duels:List[Duel])={
      
      <table>
      {duels.map((d) => {
        val (myPoints,opponentPoints) = if (d.player1 == uid) (d.score1,d.score2) else (d.score2,d.score1)
        
        <tr>
        <td>
          {d.htmlDescription}
          <h2><span>{uid} {myPoints}</span>   <span style="text-align: right;float:right;">{if (uid==d.player1) d.player2 else d.player1} {opponentPoints}</span></h2>
        </td>           
           {if (!d.finished)
             <td width="150px"><center><a href={"/Duel/"+d.id}  class={if (d.userCanPlay(uid))"pushButton" else "dontPushButton"}>{
               if (d.userCanPlay(uid))locStr("play") else locStr("waitForOpponent")}</a></center></td>
            else  
             <td  width="150px"><center><a class="dontPushButton"  href={"/Duel/"+d.id}>{if (d.score1==d.score2) locStr("draw")
             else if (d.player1==uid && d.score1>d.score2 || d.player2==uid && d.score2>d.score1)locStr("victory")
             else locStr("defeat")}</a></center></td>
           }
         </tr>
        })
      }
      </table>
    }
    currentUser.uid match{
      case Full(user) =>
        val (finished,open) = duels.findForUser(user).partition(_.finished)
//        "#header1 *" #> ("Duelle für " +user) &
        "#openDuels *" #> renderDuelList(user,open) &
        "#finishedDuels *" #> renderDuelList(user,finished)
      case _ => "*" #> <span>{loc("notLoggedIn")}</span>
    }
  }
}
