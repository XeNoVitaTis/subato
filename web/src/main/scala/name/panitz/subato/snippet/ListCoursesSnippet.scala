package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._

import scala.xml.{NodeSeq, Text}

class ListCoursesSnippet(val c:Box[Course])
  extends AbstractListCoursesSnippet
  with SWrapperL10n
  with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val course = c
}

trait AbstractListCoursesSnippet {
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val course:Box[Course]
	
  def list={
    "#name *" #>
      courses
        .findAll
        .filter((c) => currentUser.isLecturerIn(c))
        .sortWith((x, y) ⇒ x.name<y.name)
        .map((c) => <div><b>{c.name}</b>{
          if (currentUser.isLecturerIn(c.id)){
            <div><ul>
             <li><a href={"/ListExercises/"+c.id}>{loc("exercises")}</a></li>
             <li><a href={"/exportAllExercises/"+c.id+"/history"}>{loc("exportAllExercises")}</a></li>
             <li><a href={"/ImportExercises/"+c.id}>{loc("importExercises")}</a></li>
          <!--   <li><a href={"/ListExerciseSheets/"+c.id}>{loc("exerciseSheets")}</a></li> -->
            </ul></div>
          }else Text("")
        }</div> )
  }
}
