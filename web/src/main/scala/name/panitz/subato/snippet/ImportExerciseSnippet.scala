package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty, Full}
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml, FileParamHolder, S }

import scala.xml.{NodeSeq, Text}

class ImportExerciseSnippet(val c:Box[Course])
    extends AbstractImportExerciseSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val course = c
}

trait AbstractImportExerciseSnippet extends  XmlConfigDependency{
  this:ModelDependencies
      with L10n
      =>

  protected val params:Params
  protected val course:Box[Course]

  def importExercise={
    var fileHolder : FileParamHolder = null
    val courseId = course.openOrThrowException("").id

    def exerciseSubmitted = {
//      println(fileHolder)
      fileHolder match {
      // An empty upload gets reported with a null mime type,
      // so we need to handle this special case
        case FileParamHolder(_, "text/xml", _, data) => {
          val xml =  scala.xml.XML.load(fileHolder.fileStream)
          ImportExercise.createExerciseFromXML(xml,courseId)
        }
        case _ => {
          S.error("Invalid receipt attachment")
          false
        }
       }
      //redirectTo("/Results")
    }

    (course) match {
      case (Full(c)) => {
        "#course" #> (c.name) &
        "#file *" #> SHtml.fileUpload(fileHolder = _) &
        "#submit *" #> SHtml.submit(locStr("submitExercise"), exerciseSubmitted _)
      }
      case _ => loc("noCourseSpecified")
    }
  }


  def importExercises={
    var fileHolder : FileParamHolder = null
    def exerciseSubmitted = {
      val courseId = course.openOrThrowException("").id
      fileHolder match {
      // An empty upload gets reported with a null mime type,
      // so we need to handle this special case
        case FileParamHolder(_, "text/xml", _, data) => {
          val xml =  scala.xml.XML.load(fileHolder.fileStream)
          for (exXml <- xml \\ "exercise") ImportExercise.createExerciseFromXML(exXml,courseId)
        }
        case _ => {
          S.error("Invalid receipt attachment")
          false
        }
       }
    }

    (course) match {
      case (Full(c)) => {
        "#course" #> (c.name) &
        "#file *" #> SHtml.fileUpload(fileHolder = _) &
        "#submit *" #> SHtml.submit(locStr("submitExercise"), exerciseSubmitted _)
      }
      case _ => loc("noCourseSpecified")
    }
  }


}


object ImportExercise
    extends MapperModelDependencies
    with XmlConfigDependency{

  type Name    = String
  type TestCode = String
  type SolutionFQCN = String
  type SolutionCode = String
  type UnitTest = (Name, TestCode)
  type SampleSolution = (SolutionFQCN, SolutionCode)
  def createExerciseFrom(lang: String, sampleSolution: SampleSolution, unitTests: Seq[UnitTest]): Exercise = {
    val ex = exercises.create(-1 , lang , "" , "" , "" , sampleSolution._1 , "" , sampleSolution._2, 1)

    //TODO move to DB too

    import java.io._
    val exFolder = s"${config.exBaseDir}/${ex.id}/"
    FileUtil.mkdir(exFolder)
    val testsClssFolder = exFolder + "test-classes/src/"
    FileUtil.mkdir(testsClssFolder)
    unitTests foreach { case (name: Name, testCode: TestCode) =>
      val file = new File(s"$testsClssFolder$name.$lang")
      val writer = new OutputStreamWriter(new FileOutputStream(file),"utf-8")
      writer write testCode
      writer.close()
    }
    ex
  }

  def createExerciseFromXML(xml:scala.xml.Node, courseId:Long): Exercise = {
    val lang = (xml \\ "lang").text
    val ex = exercises.create(courseId
      , lang
      , (xml \\ "name").text
      , (xml \\ "description").text
      , (xml \\ "text").head + ""
      , (xml \\ "solutionFQCN").text
      , (xml \\ "solutionTemplate").text
      , (xml \\ "solution").text
      , (xml \\ "testclasses").toString()
      , 1
    )

    val testExecutorB = testExecutors.find(lang.toLowerCase)
    testExecutorB match {
      case Full(testExecutor) => ex.testExecutor = testExecutor
      case _ => {}  
    }

    ex.save()
    ex
  }
}
