package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato.model.learnsystem._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}

import scala.xml.{NodeSeq, Text}


class CreateCoursePageForCourseAndTermSnippet(val c:Box[Course])
    extends AbstractCreateCoursePageForCourseAndTermSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  def this(e:(String,Box[Course])) = {
    this(e._2)
    this.termId = e._1
  }

  override protected var termId:String = null
  override protected val params = ParamsImpl
  override protected val course = c

}

trait AbstractCreateCoursePageForCourseAndTermSnippet {
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val course:Box[Course]
  protected var termId:String


  def createForm={
    var name = ""
    var term:Term=null
    var theCourse:Course=null

    def solutionSubmitted(){
      //      val create = groups.createAndSave(name,theCourse,term)
      val create = coursePages.createAndSave(theCourse.id,term.id)
      import net.liftweb.http.S
      net.liftweb.http.S.redirectTo("/CoursePage/"+create.id)
    }
    
    (course,terms.find(termId.toLong)) match {
      case (Full(c),Full(t)) => {
        term=t
        theCourse=c
        val pages = coursePages.find(c.id,t.id)
        if (!pages.isEmpty){
          net.liftweb.http.S.redirectTo("/CoursePage/"+pages(0).id)
        } else if (currentUser.isLecturerIn(c)){
          "#course *" #> (c.name+"") &
          "#term *" #> ("("+t.name+")") &
          "#hiddenTerm * " #> SHtml.hidden(()=>()) &
          "#hiddenCourse *" #> SHtml.hidden(()=>())&
          "#create *" #> 
             SHtml.button(loc("create"),solutionSubmitted _,("type","submit"))  //,("onclick","startSpinner()"))
        }else{
          "#course *" #> (c.name+"") &
          "#term *" #> ("("+t.name+")") &
          "#explanation *" #> <div>You are not allowed to create a course page in this term.</div> &
          "#create *" #> Text("")
        }

      }
      case _ =>  "*" #> <span>{loc("noGroupSpecified")}</span>
    }
  }


}
