package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._

import scala.xml.{NodeSeq, Text}

class CourseSnippet(val c:Box[Course])
    extends AbstractCourseSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  def this(e:(String,Box[Course])) = {
    this(e._2)
    this.termId = e._1
  }

  override protected var termId:String = null
  override protected val params = ParamsImpl
  override protected val course = c

}

trait AbstractCourseSnippet {
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val course:Box[Course]
  protected var termId:String
  
  def showCourse={
    (course,terms.find(termId.toLong)) match {
      case (Full(c),Full(t)) => {
        "#course" #> (c.name+" ("+t.name+")")&
        "#exerciseSheet" #>
          c.exerciseSheets(termId.toLong)
           .filter(_.isPublic || currentUser.isTutorIn(c.id,termId.toLong))
           .sortWith((x,y)=>x.number<y.number)
            .map((s) =>
              <div>
                <h4>{loc("exerciseSheet")+" "+s.number}.
                  <a href={"/ExerciseSheet/"+s.id}>{s.name}</a>
                   {if (currentUser.loggedIn)
                     <span>{
                      s.isGranted(currentUser.uid.openOrThrowException(""))
                        .dmap(<span>{locStr("notGranted")}</span>)((x)=> <span style="background-color:yellow;">{locStr("grantedBy")+" "+x}</span>)
                     }</span>
                     if (s.reachablePoints>0){
                       <span><br />Bepunktetes Übungsblatt. Maximale Punkanzahl {s.reachablePoints}</span>
                     }
                 }
                </h4>
                {
                  if (s.lastSubmission!=null) <h4>{loc("lastSubmission")+": "+s.lastSubmission.toString}</h4>
                }
                {s.description}
                <div>{
                  if (currentUser.isTutorIn(c.id,termId.toLong)){
                 <span><a href={"/StudentsBySheetTerm/"+s.id+"/"+termId}>{loc("courseResults")}</a></span>
                  }
               }{if (currentUser.loggedIn)
                  <span>&nbsp;&nbsp;<a href={"/SheetResults/"+s.id+"/"+termId+"/"+currentUser.uid.openOrThrowException("")}>{loc("results")}</a></span>
               }</div>
             <p /><br /></div> )
      }
      case _ => loc("noCourseSpecified")
    }
  }
}
