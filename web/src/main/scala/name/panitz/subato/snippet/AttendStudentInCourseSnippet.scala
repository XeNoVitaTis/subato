package name.panitz.subato.snippet

import net.liftweb.common.{Box, Empty}

import name.panitz.subato.model._
import name.panitz.subato._

import java.time.LocalDate

import net.liftweb.common.Full
import scala.xml.NodeSeq
import net.liftweb.util.Helpers._

class AttendStudentInCourseSnippet(e:Box[CoursePage])
    extends AbstractAttendStudentInCourseSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  override protected var uid:String = null
  override protected val params = ParamsImpl
  override protected val coursePage = e

  def this() {
    this(Empty)
  }
  def this(e:(String,Box[CoursePage])) = {
    this(e._2)
    this.uid = e._1
  }
}

trait AbstractAttendStudentInCourseSnippet {
  this:ModelDependencies
    with L10n  =>

  protected val params:Params
  protected val coursePage:Box[CoursePage]
  protected var uid:String

  import scala.xml.Text;
  def attendCourse = coursePage match {
    case Full(cp) => {
      if (currentUser.isTutorIn(cp.course.id,cp.term.id)) {
        val t = attendances.createAndSave(uid,cp)
        "#ok" #> Text(""+t.date) 
      }else{
        "#ok" #> Text("not possible")
      }
    }
    case _ => loc("noSuchCoursePage")
  }

}
