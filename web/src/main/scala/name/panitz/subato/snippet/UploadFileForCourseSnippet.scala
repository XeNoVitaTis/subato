package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml, FileParamHolder, S }

import scala.xml.{NodeSeq, Text}

class UploadFileForCourseSnippet(val c:Box[CoursePage])
    extends AbstractUploadFileForCourseSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val coursePage = c
}

trait AbstractUploadFileForCourseSnippet
  extends XmlConfigDependency{
  this:ModelDependencies
      with L10n
      =>

  protected val params:Params
  protected val coursePage:Box[CoursePage]

  def upload={
    var fileHolder : FileParamHolder = null
    var courseId=0L
    var coursePageId=0L

    def fileSubmitted = {
      fileHolder match {
        case FileParamHolder(_,_, name,_) => {
          import java.io._
          val read = fileHolder.fileStream;
          FileUtil.mkdir(config.courseBaseDir+"/"+courseId+"/");
          val fileName = config.courseBaseDir+"/"+courseId+"/"+name

          val write = new FileOutputStream(fileName)
          var i = read.read();
          while(i>=0){
            write.write(i);
            i = read.read();
          }
          write.flush
          write.close
        }
        case _ => {
          S.error("Invalid received attachment")
          false
        }
      }
      S.redirectTo("/ListResourceFilesForCourse/"+coursePageId)
    }

    (coursePage) match {
      case (Full(c)) => {
        if (c.isTutorIn){
          courseId = c.course.id
          coursePageId = c.id
          "#course" #> ("Upload  file for course: "+ c.name) &
          "#file *" #> SHtml.fileUpload(fileHolder = _) &
          "#submit *" #> SHtml.submit(locStr("submitFile"), fileSubmitted _)
        }else{
          "*" #> <span>{loc("noallowed")}</span>
        }
      }
      case _ =>  "*" #> <span>{loc("noCourseSpecified")}</span>
    }
  }
}

