package name.panitz.subato.snippet

import net.liftweb.common.{Box, Empty}

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.Full
import scala.xml.NodeSeq
import net.liftweb.util.Helpers._

class StudentSheetResultsSnippet(e:Box[ExerciseSheet])
    extends AbstractSheetStudentResultsSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  override protected var termId:String = null
  override protected var uid:String = null
  override protected val params = ParamsImpl
  override protected val exerciseSheet = e

  def this() {
    this(Empty)
  }
  def this(e:(String,String,Box[ExerciseSheet])) = {
    this(e._3)
    this.termId = e._2
    this.uid = e._1
  }
}

trait AbstractSheetStudentResultsSnippet {
  this:ModelDependencies
    with L10n  =>

  protected val params:Params
  protected val exerciseSheet:Box[ExerciseSheet]
  protected var termId:String
  protected var uid:String


  def studentByExercise = exerciseSheet match {
    case Full(sheet) => {
      if (currentUser.isTutorIn(sheet.course.id,termId.toLong)
        || currentUser.uid.openOrThrowException("")==uid
      ) {
        import net.liftweb.mapper._
        val exercises = sheet.sheetExercises.map(_.exercise)
        val subs = exercises
               .filter(_.isGraded)
               .map((ex) => (ex,submissions.findBest(uid, sheet.course.id,ex.id,termId.toLong)))
        val rows = subs.map({case (ex,sub) =>
           (<td>{studentLink(ex.id,uid,termId)}</td>
            <td>{ex.name}</td>
            <td>{sub.map(_.tests.toString).openOr("/")}</td>
            <td>{sub.map(_.failures.toString).openOr("/")}</td>
            <td>{sub.map(_.errors.toString).openOr("/")}</td>
            <td>{sub.map((x)=>(x.allocations-x.frees).toString).openOr("/")}</td>
            <td>{sub.map(_.stylecheckErrors.toString).openOr("/")}</td>
            <td>{sub.map(_.score.toString).openOr("/")}</td>
            <td>{
              var numberOfCommets = submissions.commentsAvailable(uid, ex)
              if (numberOfCommets>0)
                <span style="background-color: yellow;">({numberOfCommets} {locStr("commentAvailable")})</span>
            }</td>
            
           )}
        )
        "#row *" #> rows &
        "#grantButton *" #> {if (currentUser.isTutorIn(sheet.course.id,termId.toLong))
             {<span>{sheet.isGranted(uid).openOr(sheet.grantButton(uid))}</span>}
           else{ <span>{sheet.isGranted(uid).openOr("not granted")}</span>}}
      }
      else  "#row *" #> <span />
    }
    case _ => loc("noSuchExerciseSheet")
  }

  private def studentLink(exId:Long, uid:String,termId:String):NodeSeq = {
    <a href={"/ExerciseResults/" +exId+"/"+termId  + "/" + uid}>{uid}</a>
  }

}
