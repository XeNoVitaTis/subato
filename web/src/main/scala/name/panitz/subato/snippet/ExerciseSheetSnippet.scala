package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._

import scala.xml.{NodeSeq, Text}

class ExerciseSheetSnippet(val c:Box[ ExerciseSheet ])
    extends AbstractExerciseSheetSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val exerciseSheet = c

}

trait AbstractExerciseSheetSnippet {
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val exerciseSheet:Box[ ExerciseSheet]

  def listEntries={
    (exerciseSheet) match {
      case Full(s) => {        
        if (currentUser.isLecturerIn(s.course.id,s.term.id)){
          var cuid = currentUser.uid.openOrThrowException("no user logged in")
          "#row" #> 
           s.sheetExercises.map((ex) =>
             <tr class="myrow">
               <td class="myrow-item" id="number">{ex.number}</td>
               <td class="myrow-item" id="exercise">{ex.exercise.name}</td>
               <td id="edit"><a href={"/exercisesheetentry/edit/"+ex.id}>{loc("edit")}</a></td>
               <td id="edit"><a href={"/exercisesheetentry/delete/"+ex.id}>{loc("delete")}</a></td>
             </tr>) &
          "#sheetName *" #> (loc("exerciseSheet")+" "+s.name)&
          "#addEntry *"  #> (<a href={"/SheetAddEntry/"+s.id}>{loc("createExerciseSheetEntry")}</a>) &
          "#back *"  #> (<a href={"/ListSheetsForCourseAndTerm/"+s.course.id+"/"+s.term.id}>{loc("back")}</a>)
        }else{
          "#sheetName *" #> (loc("exerciseSheet")+" "+s.name)
        }
      }
      case _ =>  "*" #> <span>{loc("noExerciseSheetSpecified")}</span>
    }
  }

	
  def showSheet={
    (exerciseSheet) match {
      case Full(s) => {
        //var cuid = currentUser.uid.openOrThrowException("no user logged in")
        "#course *" #> s.course.name &
        "#exerciseSheet *" #> s.name &
        "#term *" #> s.term.name  &
        "#description *" #> s.description  &
        "#number *" #> (loc("exerciseSheet")+" "+s.number)  &
        "#lastSubmission *" #> {if (s.lastSubmission!=null) (loc("lastSubmission")+": "+s.lastSubmission.toString) else ""} &
        ("#exercise *" #>
          s.sheetExercises.sortWith((x,y)=>x.number<y.number)
          .map((se) => (<li>
              <b><a href={"/Exercise/"+se.exercise.id+"/"+s.id}>{loc("exercise")+" "+se.number}</a></b>
            {se.exercise.name}
            {
              if (currentUser.loggedIn){
                var cuid = currentUser.uid.openOrThrowException("no user logged in")
                var numberOfComments = submissions.commentsAvailable(cuid, se.exercise)
                 <span style="background-color: yellow;">({numberOfComments} {locStr("commentAvailable")})</span>                
              }
            }
            {
                if (se.exercise.solutionIsPublic){<a href={"/Solution/" + se.exercise.id}>({locStr("solution")})</a>}
            }

            <div>{se.exercise.description}</div>
            {if (currentUser.loggedIn){
              var cuid = currentUser.uid.openOrThrowException("no user logged in")
              <div>
              <div>
                <a href={"/ExerciseResults/" + se.exercise.id+"/"+s.term.id + "/" + cuid}>{locStr("submissions")}</a>
                ({submissions.count(cuid, se.exercise,s.course.id,s.term.id) +"/"+se.exercise.attempts})
              </div>
              {if (currentUser.isTutorIn(s.course.id,s.term.id)) {
                 <div>
                  (<a href={"/StudentsByExerciseTerm/" + se.exercise.id+"/"+s.term.id+"/"+s.course.id}>{locStr("results")}</a>)
                  (<a href={"/exportExercise/"+se.exercise.id+"/history"}>{locStr("exportXML")}</a>)
                  (<a href={"/Solution/" + se.exercise.id}>{locStr("solution")}</a>)
                  {
                    if (currentUser.isLecturerIn(s.course.id,s.term.id)) {
                      <span>(<a href={"/exercise/edit/"+se.exercise.id}>{locStr("edit")}</a>)
                      (<a onclick="startSpinner()" href={"/TestSampleSolution/"+se.exercise.id+"/"+s.course.id}>{locStr("testSampleSolution")}</a>)
                      (<a href={"/Plagiarism/"+se.exercise.id+"/"+s.id}>{locStr("runplagiarism")}</a>)
                      (<a href={"/serveJPlagResult/"+se.exercise.id+"/"+s.id}>{locStr("downloadplagiarism")}</a>)
                      (<a href={"/TGZSubmissions/"+se.exercise.id+"/"+s.id}>{locStr("tgz")}</a>)
                      </span>
                    }
                  }
                 </div>
              }}</div>
            }}
           </li>)))
      }
      case _ => loc("noCourseSpecified")
    }
  }
}
