package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._

import net.liftweb.http.{StatefulSnippet, SHtml}

import scala.xml.{NodeSeq, Text}

class SheetSnippet(val c:Box[Course]) 
    extends AbstractSheetSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  def this(e:(String,Box[Course])) = {
    this(e._2)
    this.termId = e._1
  }

  override protected var termId:String = null
  override protected val params = ParamsImpl
  override protected val course = c
  override protected var courseId:String=""
}

trait AbstractSheetSnippet   extends StatefulSnippet{
  this:ModelDependencies
      with L10n =>

  override def dispatch = {
    case "Create" => createSheet 
    case "List" => listSheets 
  }


  protected val params:Params
  protected val course:Box[Course]
  protected var termId:String
  protected var courseId:String

  def createSheet={
    var isPublic = false
    var name = ""
    var number = 0
    var description = ""

    def solutionSubmitted(){
      val sheet = exerciseSheets.createAndSave(name,number,isPublic,description,courseId.toLong,termId.toLong)
      redirectTo("/ListSheetsForCourseAndTerm/"+courseId+"/"+termId)
    }

    (course,terms.find(termId.toLong)) match {
      case (Full(c),Full(t)) => {
        if (currentUser.isLecturerIn(c)){
          val cps = coursePages.find(c.id,t.id)
          "#course *" #> (c.name+"") &
          "#term *" #> ("("+t.name+")") &
          "#number *" #> SHtml.number(0, (text)=>number=text,0,Integer.MAX_VALUE)&
          "#isPublic *" #> SHtml.checkbox(false, (b)=> isPublic=b)&
          "#name *" #> SHtml.text("", (text)=> name=text)&
          "#textArea *" #> SHtml.textarea("", (str)=>description=str, "id" -> "mce1")&
          "#hiddenTerm * " #> SHtml.hidden(()=>()) &
          "#hiddenCourse *" #> SHtml.hidden(()=>courseId = c.id.toString)&
          "#create *" #> 
             SHtml.button(loc("create"),solutionSubmitted _,("type","submit"))&
          "#back *" #> {if (!cps.isEmpty) <a href={"/CoursePage/"+cps(0).id}>{loc("backToCoursePage")}</a>
                        else <span/> }
          //,("onclick","startSpinner()"))
        }else{
          "#courseName *" #> (c.name +" "+loc("exerciseSheets"))
        }

      }
      case _ =>  "*" #> <span>{loc("noCourseSpecified")}</span>
    }
  }


  def listSheets={
    (course,terms.find(termId.toLong)) match {
      case (Full(c),Full(t)) => {
        if (currentUser.isLecturerIn(c)){
          val cps = coursePages.find(c.id,t.id)
          "#course *" #> (c.name+"") &
          "#term *" #> ("("+t.name+")") &
          "#create *" #> <a href={"/CreateSheetForCourseAndTerm/"+c.id+"/"+termId}>{loc("createSheet")}</a> &
          "#row" #>
             exerciseSheets.findByCourseAndTerm(c.id,t.id).map((sheet) => <tr>
               <td>{sheet.number.toString}</td>
               <td>{sheet.name}</td>
               <td><a href={"/ExerciseSheet/"+sheet.id}>{loc("view")}</a></td>
               <td><a href={"/exercisesheet/edit/"+sheet.id}>{loc("edit")}</a></td>
               <td><a href={"/exercisesheet/delete/"+sheet.id}>{loc("delete")}</a></td>
               <td><a href={"/ListExerciseSheetEntries/"+sheet.id}>{loc("editEntries")}</a></td>
               <td><a href={"/SheetAddEntry/"+sheet.id}>{loc("newEntry")}</a></td>
               </tr>
             ) &
          "#back *" #> <div>{ if (!cps.isEmpty) <a href={"/CoursePage/"+cps(0).id}>{loc("backToCoursePage")}</a>
                         else <span/> }</div>
        }else{
          "#courseName *" #> (c.name +" "+loc("exerciseSheets"))
        }
      }
      case _ =>  "*" #> <span>{loc("noCourseSpecified")}</span>

    }
  }
}
