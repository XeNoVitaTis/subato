package name.panitz.subato.snippet

import java.time.{LocalDateTime => LDT}

import com.github.javaparser.ast.CompilationUnit
import name.panitz.subato.evaluator._
import name.panitz.subato.model.learnsystem.FlashcardCourse
import name.panitz.subato.model._
import name.panitz.subato.snippet.ImportExercise.UnitTest
import name.panitz.subato.{FileUtil, L10n, SWrapperL10n, XmlConfigDependency}
import net.liftweb.common.{Box, Full}
import net.liftweb.http._
import net.liftweb.http.js.{JE, JsCmd, JsCmds}
import net.liftweb.json.{JValue, _}
import net.liftweb.util.CssSel
import net.liftweb.util.Helpers._
import CardAttributeExtractor._
import CourseAttributeExtractor._

import scala.xml.{Node, NodeSeq}
import scala.collection.JavaConverters._

trait AbstractSlsStudioSnippet {
  this: ModelDependencies with L10n =>
  def initClientSideVariables: CssSel
  def bindCreationButton: CssSel
  def activateCardSearch: CssSel
  def activateCodePlayerButton: CssSel
  def activateCourseSearch: CssSel
}


class SlsStudioSnippet extends AbstractSlsStudioSnippet
  with MapperModelDependencies
  with SWrapperL10n
  with XmlConfigDependency {

  override def initClientSideVariables: CssSel = {
    val userCourses = flashcardCourses
      .findAllCreatedBy(currentUser.uid.getOrElse(""))
      .map(x => s"{'name': `${x.name}`, 'id': `${x.id}`}")
    val initialization = s"""
      var myCourses = [${ userCourses mkString "," }]
      var allCards = [];
      var cardTopics = ${allTopics.map{ "\"" + _ + "\"" }.mkString("[", ",", "]")};
    """
    "#cardContainer *" #> initialization
  }

  override def activateCardSearch: CssSel = {
    def fromCoursesToCards(courses: Seq[FlashcardCourse]): Seq[Card] = {
      courses.foldRight(Seq.empty[Box[Card]]) { (next, result) =>  next.learningMaterials.map { _.flashcard } ++: result }
        .filter { _.isDefined }
        .map { _.openOrThrowException("") }
    }

    def filterCardsByTopic(searchedTopic: String): JsCmd = {
      val searchedTopic_ = searchedTopic.trim.toLowerCase
      val (qualifiedCourses, nonQualiefiedCourses) = flashcardCourses
        .findAll
        .filter { _.isPublic_? }
        .foldRight((Seq.empty[FlashcardCourse], Seq.empty[FlashcardCourse])) {
          case (next, (xs, ys)) =>
            if (next.topics.exists(_.toLowerCase.contains(searchedTopic_))) (next +: xs, ys) else (xs, next +: ys)
        }

      val qualifiedCardsByCourse     = fromCoursesToCards(qualifiedCourses)
      val qualifiedCardsByOwnTopics =  fromCoursesToCards(nonQualiefiedCourses)
        .filter { _.topics.exists(_.toLowerCase.contains(searchedTopic_)) }
      val qualifiedCards  = qualifiedCardsByCourse ++: qualifiedCardsByOwnTopics
      val qualifiedTopics = (qualifiedCourses.map { _.topics } ++: qualifiedCards.map { _.topics })
        .foldRight(Seq.empty[String]) { (next, result) => next.filter(_.trim.nonEmpty) ++: result }
        .distinct
        .map { "\"" + _ + "\"" }
      val jsCode = s"""refreshCardsContainers(${qualifiedCards.map(jsonify).mkString("[", ",","]")},
                         ${qualifiedTopics.mkString("[", ",", "]")});
                         showCards();
                        removeLoadingIcon(`loading-card-icon`);
                    """
      JsCmds Run jsCode
    }
    "#card-by-name [onclick]" #> SHtml.ajaxCall(JE.Call("searchedCardTopic"), filterCardsByTopic)
  }

  override def activateCourseSearch: CssSel = {
    def selectCourseById(id: String): JsCmd = {
      if (id != "-1") {
        flashcardCourses.find(id.toLong) match {
          case Full(course) => JsCmds Run s"""shouldCleanOtherCard(${jsonify(course)});"""
          case _            => JsCmds.jsExpToJsCmd(JE.Call("removeLoadingIcon", "loading-course-icon")) & new SimpleAlert(Severity.Error, "Course not found!", <b>Unerwarteter fehler!</b>, "Ok", false)
        }
      } else {
        JsCmds Run ""
      }
    }
    "#course-by-name [onclick]" #> SHtml.ajaxCall(JE.Call("searchedCourseId"), selectCourseById)
  }

  override def activateCodePlayerButton: CssSel = {
    "#run-code-btn [onclick]" #> SHtml.jsonCall(JE.Call("createNewCard"), checkFromStudioEditedCodeIfUserLoggedIn)
  }

  override def bindCreationButton: CssSel = {
    if (currentUser.loggedIn && currentUser.isLecturer) {
      "#create-course-btn [onclick]" #> SHtml.jsonCall(JE.Call("jsonifyCourseToBeCreated"), jsonToCourse)
    } else {
      val alert = new SimpleAlert(Severity.Error, s"Course creation failed", "Only logged in user are allowed to create courses", "Ok", false).toJsCode
      "#create-course-btn [onclick]" #>  alert
    }
  }

  private def jsonToCourse(json: JValue): JsCmd = {
    if(currentUser.loggedIn){
      if (courseCanBeUpdatedFrom(json, currentUser.uid.getOrElse(""))){
        updateCourseWith(json)
      } else {
        createNewCourseFrom(json)
      }
    } else {
      new SimpleAlert(Severity.Error, "Access denied!", <b>You must sign up to create a course</b>, "OK", false)
    }
  }

  private def updateCourseWith(json: JValue): JsCmd = {
    flashcardCourses.find(extractCourseIdFrom(json)) match {
      case Full(course) =>
        val oldCards = course.cards
        updateCourse(course, json)
        var actualCards = Seq.empty[Card]
        extractCardJsonsFrom(json).foreach( x => {
          if (containsNewCard_?(x)) {
            createNewCardFrom(x) match {
              case Right(card) => learningMaterials.createAndSaveFrom(course.id, card.id) ; actualCards = card +: actualCards
              case _           =>
            }
          } else {
            val id = extractIdFrom(x)
            multipleCards find id match {
              case Full(card) => actualCards = card +: actualCards
              case _          =>
            }
          }
        })
        oldCards filter { x => !(actualCards exists { _.id == x.id }) } foreach { _.delete() }
        new SimpleAlert(Severity.Success, s"Course successfully updated", <div></div>, "OK", false)
      case _ => new SimpleAlert(Severity.Error, s"Course update failed", <div>Unexpected failure</div>, "OK", false)
    }
  }

  private def updateCourse(oldCourse: FlashcardCourse, json: JValue): Unit = {
    val isPrivate = if (extractCoursePrivacyFrom(json) == "private") true else false
    oldCourse.update(extractCourseTitleFrom(json),
      extractCourseDescriptionFrom(json),
      extractCourseTopicsFrom(json),
      isPrivate
    )
  }

  private def updateCard(oldCard: Card, json: JValue): Unit = {
    oldCard.update(-1,
      extractTypeFrom(json),
      extractQuestionFrom(json).fold(_ => Nil, x => x),
      extractExplanationFrom(json),
      extractTitleFrom(json),
      extractBloomsLevelFrom(json),
      extractTopicsFrom(json)
    )
    updateSimpleCardAnswers(oldCard, json)
  }

  private def createNewCourseFrom(json: JValue): JsCmd = {
    lazy val (cardErrorMessages, courseCards) = extractCardJsonsFrom(json)
      .foldRight(Seq.empty[Node], Seq.empty[Card]) {
        case (next, (errors, cards)) =>
          createNewCardFrom(next) match {
            case Right(card) => (errors, card +: cards)
            case Left(error) => (formatErrorMessage(error) +: errors, cards)
          }
      }

    courseCanBeCreatedFrom(json) match {
      case (true, _)   =>
        val userId = currentUser.uid.getOrElse("")
        val courseTitle = extractCourseTitleFrom(json)
        createCourse(userId, courseTitle, extractCourseDescriptionFrom(json), isCoursePrivate(json), extractCourseTopicsFrom(json), courseCards)
        new SimpleAlert(Severity.Success, "Course successfully created", s"New Course »$courseTitle« succesfully created", <div>{cardErrorMessages}</div>, "Ok", false)
      case (_, courseErrorMessage) =>
        new SimpleAlert(Severity.Error, s"Course creation failed", <div>{courseErrorMessage}{cardErrorMessages}</div>, "OK", false)
    }
  }


  private def jsonToCard(json: JValue): Either[ErrorMessage, Card] = {
    if (containsNewCard_?(json)) {
      createNewCardFrom(json)
    } else {
      val id = extractIdFrom(json)
      multipleCards find id match {
        case Full(c) => Right(c)
        case _       => Left((extractTitleFrom(json), <li>Unexpected failure</li>))
      }
    }
  }

  private def createNewCardFrom(json: JValue): Either[ErrorMessage, Card] = {
    if (containsProgrammingCard_?(json) && programmingCardCanBeCreatedFrom(json)) {
      Right(createProgrammingCardFrom(json: JValue))
    } else if (simpleCardCanBeCreatedFrom(json)) {
      Right(createSimpleCardFrom(json))
    } else {
      Left(whatIsIncorrect_?(json))
    }
  }

  private def createSimpleCardFrom(json: JValue): Card = {
    createSimpleCard(
      extractTypeFrom(json),
      extractQuestionFrom(json).fold(_ => Nil, x => x),
      extractCorrectAnswersFrom(json),
      extractInCorrectAnswersFrom(json),
      extractExplanationFrom(json),
      extractTitleFrom(json),
      extractBloomsLevelFrom(json),
      extractTopicsFrom(json)
    )
  }

  private def createProgrammingCardFrom(json: JValue): Card = {
    createProgrammingCard(
      extractProgrammingLanguageFrom(json),
      extractTitleFrom(json),
      extractQuestionFrom(json).fold(_ => Nil, x => x),
      extractExplanationFrom(json),
      extractSampleSolutionCode(json).fold(_ => null, x => x),
      extractTestCodeFrom(json).fold(_ => null, x => x),
      extractBloomsLevelFrom(json),
      extractTopicsFrom(json)
    )
  }

  private def updateSimpleCardAnswers(card: Card, json: JValue): Unit = {
    card.deleteAnswers()
    val correctAns = extractCorrectAnswersFrom(json)
    val incorrectAns = extractInCorrectAnswersFrom(json)
    for ((ans, index) <- correctAns.zipWithIndex) {
      val answer = answers.create(card.id, index, ans, c = true)
      answer.save()
    }
    val sizeOfCorrectAns = correctAns.size
    for ((ans, index) <- incorrectAns.zipWithIndex) {
      val answer = answers.create(card.id, index + sizeOfCorrectAns , ans, c = false)
      answer.save()
    }
  }

  private def createProgrammingCard(language: String, title: String, questionNode: NodeSeq, explanation: String, correctAns: CompilationUnit, testCode: CompilationUnit, bloomsLevel: String, topics: Seq[String]): Card = {
    val solution = (correctAns.getType(0).getName.asString(), correctAns.toString)
    val tests: Seq[UnitTest] = testCode.getTypes.iterator.asScala.map(x => (x.getName.asString, x.toString)).toSeq
    val exercise = ImportExercise.createExerciseFrom(language, solution, tests)
    val card = multipleCards.create(-1, "Programming", questionNode, explanation, title, bloomsLevel, topics)
    card.save()
    val answer = answers.create(card.id, 0, correctAns.toString, c = true)
    answer.save()
    card.exerciseId = exercise.id
    card.save()
    exercise.save()
    card
  }

  private def createSimpleCard(typ: String, questionNode: NodeSeq, correctAns: Seq[String], incorrectAns: Seq[String], explanation: String, title: String, bloomsLevel: String, topics: Seq[String]): Card = {
    val card = multipleCards.create(-1, typ, questionNode, explanation, title, bloomsLevel, topics)
    card.save()
    for ((ans, index) <- correctAns.zipWithIndex) {
      val answer = answers.create(card.id, index, ans, c = true)
      answer.save()
    }
    val sizeOfCorrectAns = correctAns.size
    for ((ans, index) <- incorrectAns.zipWithIndex) {
      val answer = answers.create(card.id, index + sizeOfCorrectAns , ans, c = false)
      answer.save()
    }
    card
  }


  private def createCourse(userId: String, name: String, description: String, isPrivate: Boolean, topics: Seq[String], cards: Seq[Card]): Unit = {
    val course = flashcardCourses.createAndSave(name, isPrivate, userId, description, topics, LDT.now, LDT.now)
    cards foreach { card => learningMaterials.createAndSaveFrom(course.id, card.id) }
    subscribedFlashcardCourses.createAndSave(userId, course.id)
  }

  private def allTopics: Seq[String] = {
    flashcardCourses
      .findAll
      .foldRight(Seq.empty[String]) {
        (next, result) => {
          val cardTopics: Seq[String] = next
            .learningMaterials
            .map { _.flashcard.map(_.topics).getOrElse(Seq.empty[String]) }
            .foldRight(Seq.empty[String]) { (n, r) => n ++: r }
          cardTopics ++: next.topics ++: result
        }
      }.filter { _.nonEmpty }
  }

  private def jsonify(card: Card): String = {
    val cs = statistics findCardStatistic card.id
    lazy val exercise = card.exercise
    val correctAnswers = if (card.isProgrammingCard_?) exercise.solution else s"""${card.answers filter { _.isCorrect } map { x => s"`${x.genericText.toString}`" } mkString ","}"""
    val programmingCardCompl = if (card.isProgrammingCard_?) s""", "tests": ``, "initialSnippet": `${exercise.solutionTemplate}`, "programmingLanguage": `${exercise.language}` """ else ""
    s"""{
      "id": ${card.id},
      "title": `${card.name}`,
      "topics": [${ card.topics.map { x => "\"" + x + "\""} mkString "," }],
      "question": `${card.genericExerciseText}`,
      "customizable": true,
      "status": "selectable",
      "bloomslevel": `${card.bloomsLevel}`,
      "type": `${card.cardType.toLowerCase}`,
      "explanation": `${card.genericExplanation}`,
      "answers": {
        "correctAnswers": [$correctAnswers],
        "incorrectAnswers": [${card.answers filter { !_.isCorrect } map { x => s"`${x.genericText.toString}`" } mkString ","}]
      },
      "statistic": {
         "goodAns": ${cs.goodAnswerPercent},
         "almostGoodAns": ${cs.almostGoodAnswerPercent},
         "wrongAns": ${cs.wrongAnswerPercent},
         "avrTime": ${cs.averageTime}
      }$programmingCardCompl
    }"""
  }

  private def jsonify(course: FlashcardCourse): String = {
    s"""{
      "id": `${course.id}`,
      "title": `${course.name}`,
      "privacy": `${ if (course.isPrivate_?) "private" else "public" }`,
      "topics": [${course.topics filter { _.nonEmpty } map { x => "\"" + x + "\""} mkString "," }],
      "description": `${course.description.text}`,
      "cards": [${course.cards map jsonify mkString "," }]
    }"""
  }

  private def checkFromStudioEditedCodeIfUserLoggedIn(json: JValue): JsCmd = {
    if(currentUser.loggedIn) {
      checkFromStudioEditedCode(json: JValue)
    } else {
      new SimpleAlert(Severity.Error, "Acces denied", <div>The service is only available for registered users</div>, "OK", false)
    }
  }

  private def checkFromStudioEditedCode(json: JValue): JsCmd = {
    implicit val formats: DefaultFormats.type = DefaultFormats
    val sampleSolution = extractSampleSolutionCode(json)
    val testCode       = extractTestCodeFrom(json)
    val language       = extractProgrammingLanguageFrom(json)
    (sampleSolution, testCode) match {
      case (Right(a), Right(b)) =>
        process(language, a, b)
          .fold(x => new SimpleAlert(Severity.Info, "Sls Code Player", x, "OK", false), fromSubmissionToFeedback)
      case _ =>
        val errorMessages = Seq(sampleSolution fold (x => x, _ => ""), testCode fold (x => x, _ => ""))
        new SimpleAlert(Severity.Error,
          "Sls Code Player", <ul style="text-align: left; margin-top: 15px; margin-left: 10px;">{errorMessages}</ul>,
          "OK",
          false)
    }
  }

  private def fromSubmissionToFeedback(submission: Submission): JsCmd = {
    val isCorrect = !submission.compilationFailed && (submission.errors+submission.failures == 0)
    if(isCorrect) {
      new SimpleAlert(Severity.Success, "Sls Code Player", "The sample solution was validated by the tests", "OK", false)
    } else {
      // TODO: includes exec output in the feedbacks
      new SimpleAlert(Severity.Error, "Sls Code Player", "The sample solution was not validated by the tests", "OK", false)
    }
  }

  private def process(language: String, sampleCode: CompilationUnit, testCode: CompilationUnit) : Either[String, Submission] = {
    try {
      val solution = (sampleCode.getType(0).getName.asString(), sampleCode.toString)
      val tests: Seq[UnitTest] = testCode.getTypes.iterator.asScala.map(x => (x.getName.asString, x.toString)).toSeq
      val exercise = ImportExercise.createExerciseFrom(language, solution, tests)
      val userDir = s"${config.usersBaseDir}/anonymous/${exercise.id}/"
      val submission = exercise.evaluator.testSolution(sampleCode.toString, userDir,exercise,0,0,null)
      FileUtil.deleteDir(new java.io.File(userDir))
      Right(submission)
    } catch {
      case e: Throwable => Left(e.getMessage)
    }
  }



  private def fromSubmissionToXML(submission: Submission): Node = {
    <div>Details
      <div>{
        if (submission.compilationFailed) {
          <div><b>Compilation failed</b>
            <div style="width: 100%; overflow-x: auto;"><pre>{submission.compilationOutput}</pre></div>
          </div>
        } else {
          <div>
            <b>Compilation successfull</b><br />
            <b>Test results</b><br />
            <b>Tests: </b> {submission.tests} <b>Failures: </b> {submission.failures} <b>Errors: </b> {submission.errors}<br />
          </div>
        }
        }</div>
    </div>
  }

  /*
  private def buildEvaluator(typ: String): Option[Evaluator] = {
    typ.trim.toLowerCase match {
      case "c"               => Some(new C)
      case "java"            => Some(new Java)
      case "haskell"         => Some(new Haskell)
      case "literateHaskell" => Some(new LiterateHaskell)
      case "python"          => Some(new Python)
      case _                 => None
    }
  }

*/

}
