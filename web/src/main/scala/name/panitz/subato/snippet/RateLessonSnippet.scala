package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{SHtml}

import scala.xml.{NodeSeq, Text}

class RateLessonSnippet(val c:Box[Lesson])
    extends AbstractRateLessonSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  def this(e:(String,Box[Lesson])) = {
    this(e._2)
    this.stars = e._1
  }

  override protected var stars:String = null
  override protected val params = ParamsImpl
  override protected val lesson = c
}

trait AbstractRateLessonSnippet {
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val lesson:Box[Lesson]
  protected var stars:String

  def rate={
    (lesson) match {
      case (Full(l)) => {
        "#rating" #> {
          if (!l.hasRated(currentUser)){
            l.addStars(stars.toInt)
            hasRatedLessons.createAndSave(l,currentUser.uid.openOr(""))
          }
          l.evaluationAsHTML
        }
      }
      case (_) => "#rating" #> Text("no lesson found to rate")
    }
  }
}
