package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._
import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{FileParamHolder, S, SHtml, StatefulSnippet}

import scala.xml.{Elem, Node, NodeSeq, Text}

class UploadFileSnippet(val c:Box[Exercise])
    extends AbstractUploadFileSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val exercise = c
}

trait AbstractUploadFileSnippet extends StatefulSnippet
  with XmlConfigDependency{
  this:ModelDependencies
      with L10n
      =>

  protected val params:Params
  protected val exercise:Box[Exercise]
  override def dispatch = {
    case "upload" => uploadFile

  }

  def addNode(to: Node, newNode: Node) = to match {
    case Elem(prefix, label, attributes, scope, child@_*) => Elem(prefix, label, attributes, scope, child ++ newNode: _*)
    case _ => System.out.println("could not find node"); to
  }


  def uploadFile={
    var fileHolder : FileParamHolder = null
    val exerciseId = exercise.openOrThrowException("").id
    var isSrcFile = false

    def fileSubmitted = {
      System.out.println(isSrcFile)
      fileHolder match {
        case FileParamHolder(_,_, name,_) => {

          if (isSrcFile) {
            val testFiles = exercise.openOrThrowException("").getTestFiles()
            System.out.println(fileHolder.fileName)
            val f = fileHolder.fileName.split("\\.")
            val content = scala.io.Source.fromInputStream(fileHolder.fileStream).mkString
            val xml = <class lang={f(1)} package="." name={f(0)}>{content}</class>

            exercise.openOrThrowException("").setTestFiles(addNode(testFiles, xml).toString())
            exercise.openOrThrowException("").save()
          } else {
            import java.io._

            val dir = new java.io.File(config.exBaseDir+"/"+exerciseId+"/test-classes/src/")
            if (!dir.exists) dir.mkdirs

            val read = fileHolder.fileStream;
            val fileName = config.exBaseDir+exerciseId+"/"+name
            println(fileName)
            val write = new FileOutputStream(fileName)
            var i = read.read();
            while(i>=0){
              write.write(i);
              i = read.read();
            }
            write.flush
            write.close
          }

        }
        case _ => {
          S.error("Invalid receipt attachment")
          false
        }
      }
      redirectTo("/ListResourceFilesForExercise/"+exerciseId)
    }

    (exercise) match {
      case (Full(c)) => {
        "#exercise" #> ("Upload excercise file for exercise: "+ c.name) &
        "#file *" #> SHtml.fileUpload(fileHolder = _) &
        "#isSrcFile *" #> SHtml.checkbox( isSrcFile,s=> {isSrcFile = s}) &
        "#submit *" #> SHtml.submit(locStr("submitFile"), fileSubmitted _)
      }
      case _ =>  "*" #> <span>{loc("noExerciseSpecified")}</span>
    }
  }
}

