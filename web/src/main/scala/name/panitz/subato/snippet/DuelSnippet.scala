package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato.model.Progress._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}
import net.liftweb.http.SessionVar
import net.liftweb.common.Logger
import net.liftweb.mapper.By

import scala.xml.{NodeSeq, Text}

class DuelSnippet(val c:Box[Duel])
    extends AbstractDuelSnippet
    with SWrapperL10n

    with MapperModelDependencies {

  def this() {
    this(Empty)
  }

  override protected val params = ParamsImpl
  override protected val duel = c
}

trait AbstractDuelSnippet extends StatefulSnippet with AnswerHandler{
  this:ModelDependencies
      with L10n =>
  protected val params:Params
  protected val duel:Box[Duel]

  override def dispatch = {
    case "display" => display
    case "play" => play
    case "result" => result
    case "createNewDuelForm" => createNewDuelForm
  }


  abstract class StateVar[T](defV:T) extends SessionVar[Map[Long,T]](Map()) {
    def <= (ep : T)={
      duel match{
        case Full(cb) => set(get + (cb.id -> ep))
        case _ => 
      }
    }
    def >= ={
      duel match{
        case Full(cb) =>  get.get(cb.id).getOrElse(defV)
        case _ => defV
      }
    }
  }


  object lastResult extends SessionVar[Progress](NotPlayed) {}
  object submission extends StateVar[Option[Solution]](None) {}

  override def processResult(id:Long, res : Progress) ={
    lastResult.set(res)

    val uid = currentUser.uid.openOrThrowException("")
    duel match{
      case Full(d) =>
        if (uid == d.player1 || uid==d.player2){
          val next = if(uid==d.player1) d.next1 else d.next2
          val (card, resGetter, resSetter, scoreGetter, scoreSetter ) =
            if(uid==d.player1) (d.cards()(next), d.score1, (x)=>d.score1_=(x), d.p1Scores(next),d.p1ScoresSetter(next))
            else               (d.cards()(next), d.score2, (x)=>d.score2_=(x), d.p2Scores(next),d.p2ScoresSetter(next))
          if (id==card.id && scoreGetter==NotPlayed){
            scoreSetter(res)
            res match{
              case Correct => resSetter(resGetter+2)
              case PartialCorrect => resSetter(resGetter+1)
              case _ => 
            }
            if(uid==d.player1) d.next1 = d.next1+1 else d.next2= d.next2 + 1
            d.save
            name.panitz.subato.comet.DuelMaster ! name.panitz.subato.comet.Ticke(d)
            redirectTo("/DuelResult/"+d.id)
          }
        }
      case _ =>  "*" #> <span>{loc("noDuelSpecified")} </span>
    }
  }

  def solutionSubmitted(sol:Solution)={
    submission <= (Some(sol))
  }

  def display={
    import _root_.name.panitz.subato.model.Progress._
    def getBox(p:Progress)= p match {
      case NotPlayed => <span class="greybox" >&nbsp;</span>
      case Correct => <span class="greenbox" >&nbsp;</span>
      case Incorrect => <span class="redbox" >&nbsp;</span>
      case PartialCorrect => <span class="yellowbox" >&nbsp;</span>
    }
    val uid = currentUser.uid.openOrThrowException("")
    duel match{
      case Full(d) =>
          "#comet [name]" #> ("duel"+d.id) &
          "#playOrWait *" #>
             (if (d.finished) Text(locStr("finished"))
             else if (d.userCanPlay(uid)) <a href={"/PlayDuel/"+d.id}>Play</a>
             else Text(locStr("waiting")) )&
          "#player1 *"    #> d.player1  &
          "#player2 *"    #> d.player2  &
          "#points1 *"    #> d.score1  &
          "#points2 *"    #> d.score2  &
          "#course *" #> d.htmlDescription // Text(locStr("duel")+": "+d.course.name)
      case _ =>  "*" #> <span>{loc("noDuelSpecified")} </span>
    }
  }

  def play={
    val uid = currentUser.uid.openOrThrowException("")
    duel match{
      case Full(d) =>
        if (d.userCanPlay(uid)){
          val card = d.nextCard(uid)
          card.snippet(this, d.id)
              }else{
          redirectTo("/Duel/"+d.id)
        }
      case _ =>  "*" #> <span>{loc("noDuelSpecified")} </span>
    }
  }

  def createNewDuelForm:net.liftweb.util.CssSel={
    var courseId=1L
    var cardBoxId=1L

    currentUser.uid match{
      case Full(uid) =>
        def submitted = {
          val existing = duels find(courseId,uid)
      
          val newDuel = if (!existing.isEmpty){
            existing(0).player2 = uid
            existing(0)
          }else {
            duels.create(courseId,uid)
          }
          newDuel.save
          redirectTo("/DuelList")
        }

        def submitted2 = {
          val existing = duels findForCardBox(cardBoxId,uid)
      
          val newDuel = if (!existing.isEmpty){
            existing(0).player2 = uid
            existing(0)
          }else {
            duels.create(courseId,cardBoxId,uid)
          }
          newDuel.save
          redirectTo("/DuelList")
        }


        "#courseSelector" #> SHtml.select(courses
            .findAll
            .filter((course)=>multipleCards.courseHasEnoughDuelCards(course.id))
            .map(c => new SHtml.SelectableOption(c.id+"",c.name+""))
          , Empty
          , (s) => {courseId=s.toLong}) &
        "#submit *"    #> SHtml.submit(locStr("submit"), submitted _) &
        "#cardBoxSelector" #> SHtml.select(cardBoxes
            .findAll
            .filter((cardbox)=>cardbox.isPublic && cardbox.hasEnoughDuelCards)
            .map(c => new SHtml.SelectableOption(c.id+"",c.name+""))
          , Empty
          , (s) => {cardBoxId=s.toLong}) &
        "#submit2 *"    #> SHtml.submit(locStr("submit"), submitted2 _)

      case _ => "*" #> <span>{loc("notLoggedIn")}</span>
    }
  }

  def result={
    val uid = currentUser.uid.openOrThrowException("")
    duel match{
      case Full(d) =>
        val card = d.lastCard(uid)
        card.duelResultSnippet(card, this, d.id, "/Duel/"+d.id )
      case _ =>  "*" #> <span>{loc("noDuelSpecified")} </span>
    }

  }
}
