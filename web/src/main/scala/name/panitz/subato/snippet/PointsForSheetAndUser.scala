package name.panitz.subato.snippet

import net.liftweb.common.{Box, Empty,Full}
import net.liftweb.http.{StatefulSnippet,SHtml}
import name.panitz.subato.model._
import name.panitz.subato._


import scala.xml.NodeSeq
import net.liftweb.util.Helpers._

class PointsForSheetAndUserSnippet(e:Box[ExerciseSheet])
    extends AbstractPointsForSheetAndUserSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  override protected var uid:String = null
  override protected val params = ParamsImpl
  override protected val exerciseSheet = e

  def this() {
    this(Empty)
  }
  def this(e:(String,Box[ExerciseSheet])) = {
    this(e._2)
    this.uid = e._1
  }
}

trait AbstractPointsForSheetAndUserSnippet  extends StatefulSnippet{
  this:ModelDependencies
    with L10n  =>

  protected val params:Params
  protected val exerciseSheet:Box[ExerciseSheet]
  protected var uid:String

  override def dispatch = {
    case "getForm" => showForm
  }

  def showForm =  exerciseSheet match {
    case Full(sheet) => {
      if (currentUser.isTutorIn(sheet.course.id,sheet.term.id)) {
        var sheetId = 0L
        var uidLocal = ""
        var maxPoints= sheet.reachablePoints
        var points = 0

        def solutionSubmitted = {
          import name.panitz.subato.model.mapper.Points
          import net.liftweb.mapper.By
          val pointsGivens = Points.findAll(By(Points.uid, uid),By(Points.term, sheet.term.id),By(Points.exerciseSheet,sheet.id))

          for (pg<-pointsGivens){
            var success = pg.delete_!
          }
          import java.util.Date
          var gr = new Points(uid,points,currentUser.uid.openOrThrowException("uid is not set for User"),new Date(),sheet.id,sheet.course.id,sheet.term.id)
          gr.save
          redirectTo("/CoursePageStudentResults/"+sheet.id+"/"+uid)
        }

        import net.liftweb.mapper._
        "#hiddenSheetID " #> SHtml.hidden(()=>sheetId = sheet.id) &
        "#hiddenUID " #> SHtml.hidden(()=>uidLocal = uid) &
        "#points "#>  SHtml.select( for (i <- 0 to  maxPoints) yield (""+i,""+i), Full(""+sheet.getPoints(uid)),(e)=> points=e.toInt) &
        "#submit *" #> SHtml.button(loc("change"),solutionSubmitted _,("type","submit")) &
        "#title *" #> <span>{"Punkte in "+sheet.course.name+" "+sheet.term.name+" Übungsbsblatt "+sheet.number+" für "+uid}</span> 
      }
      else  "#submit *" #> <span />
    }
    case _ =>  "*" #> loc("noSuchExerciseSheet")
  }
}
