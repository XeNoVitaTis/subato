package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}
import scala.xml.{NodeSeq, Text}
import net.liftweb.common.Logger
import net.liftweb.http.SessionVar

class ListExercisesSnippet(val c:Box[ Course ])
    extends AbstractListExercisesSnippet
    with SWrapperL10n
    with XmlConfigDependency 
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val course = c
}

trait AbstractListExercisesSnippet {
  this:ModelDependencies
      with L10n  =>

  protected val params:Params
  protected val course:Box[ Course]

  def list = {
    (course) match {
      case Full(c) => {
        if (currentUser.isLecturerIn(c)){
        "#row" #> 
           c.exercises.map((ex) =>
             <tr class="myrow">
               <td class="myrow-item" id="name">{ex.name}</td>
               <td class="myrow-item" id="description">{ex.description}</td>
               <td id="view"><a href={"/Exercise/"+ex.id+"/-1"}>{loc("view")}</a></td>
               <td id="edit"><a href={"/exercise/edit/"+ex.id}>{loc("edit")}</a></td>
               <td><a href={"/exportExercise/"+ex.id+"/history"}>{loc("exportXML")}</a></td>
               <td id="delete"><a href={"/exercise/delete/"+ex.id}>{loc("delete")}</a></td>
             </tr>) &
          "#courseName *" #> (c.name +" "+loc("exercises")) &
          "#importLink *" #> <a href={"/ImportExercise/"+c.id}>{loc("importExercise")}</a>
        }else{
          "#courseName *" #> (c.name +" "+loc("exercises"))
        }
      }
      case _ =>  "*" #> <span>{loc("noCourseSpecified")}</span>
    }
  }


  def listExerciseSheets = {
    (course) match {
      case Full(c) => {
        if (currentUser.isLecturerIn(c)){
        "#row" #> 
           c.exerciseSheets.map((ex) =>
             <tr class="myrow">
               <td class="myrow-item" id="number">{ex.number.toString}</td>
               <td class="myrow-item" id="name">{ex.name}</td>
               <td class="myrow-item" id="term">{ex.term.name}</td>
               <td id="view"><a href={"/ExerciseSheet/"+ex.id}>{loc("view")}</a></td>
               <td id="edit"><a href={"/exercisesheet/edit/"+ex.id}>{loc("edit")}</a></td>
               <td id="delete"><a href={"/exercisesheet/deletet/"+ex.id}>{loc("delete")}</a></td>
               <td id="editEntries"><a href={"/ListExerciseSheetEntries/"+ex.id}>{loc("editEntries")}</a></td>
             </tr>) &
          "#courseName *" #> (c.name +" "+loc("exerciseSheets"))
        }else{
          "#courseName *" #> (c.name +" "+loc("exerciseSheets"))
        }
      }
      case _ =>  "*" #> <span>{loc("noCourseSpecified")}</span>
    }
  }

}
