
const standardQuestionPlaceholder = "Please enter the question of the card!";

const clozetextCardQuestionPlaceholder =
    `${standardQuestionPlaceholder}
You can hide words by enclosing them with '!!'. Example !!Clozetext!!.`;

const standardAnswerPlaceholder = `Please enter the possible answer(s) of the question`;

const standardExplanationPlaceholder = `Please enter the explanation of the learninformation your card(question) is about`;

const questionEditorId    = "question-editor";
const answerEditorId      = "answer-editor";
const explanationEditorId = "explanation-editor";
const codeEditorId        = "code-editor";

const questionRowId            = "question-row";
const answerRowId              = "answer-row";
const explanationRowId         = "explanation-row";
const programmingLanguageRowId = "prog-lang-row";
const programmingLanguageId    = "prog-lang";
const programmingLanguageIconId = "prog-lang-icon";
const codeEditorRowId          = "code-editor-row";
const answerCreationButtonId   = "create-answer-btn";
const cardtypeId                 = "card-type";
const answersId      = "answers-list";
const cardTopicsId = "card-topics";

const globalClozeTextAnswerRegex = /!!\w+!!/g;
const uniqueClozeTextAnswerRegex = /!!\w+!!/;
const javaInitialSnippetRegex = /\/\*@_InitialCode_\*\/(.(?!\/\*@_))*/;
const javaTestCodeRegex = /\/\*@_Tests_\*\/(.(?!\/\*@_))*/;
const javaSampleSolutionRegex = /\/\*@_SampleSolution_\*\/(.(?!\/\*@_))*/;

let questionEditor;
let answerEditor;
let explanationEditor;
let editorsAlreadyInitiated = false;
let codeEditor;

const quillConfig = [
    ['bold', 'italic'],
    ['link', 'blockquote', 'code-block', 'image'],
    [{list: 'ordered'}, {list: 'bullet'}]
];

const quillTheme = 'snow';

const defaultProgrammingLanguage = "javascript";

const defaultCodeEditorTheme = "monokai";


function initEditorsIfNeeded() {
    if (! editorsAlreadyInitiated) {
        questionEditor = initTextEditor(questionEditorId, standardQuestionPlaceholder, quillTheme, quillConfig);
        answerEditor = initTextEditor(answerEditorId, standardAnswerPlaceholder, quillTheme, quillConfig);
        explanationEditor = initTextEditor(explanationEditorId, standardExplanationPlaceholder, quillTheme, quillConfig);
        codeEditor = initCodeEditor(codeEditorId, defaultProgrammingLanguage, "", defaultCodeEditorTheme);
        editorsAlreadyInitiated = true;
    }
}

function resetEditorsContent(question = "", answers = "", explanation = "", code = "") {
    $(questionEditor.container.firstChild).html(htmlToTextEditorConformHtml(question));
    $(answerEditor.container.firstChild).html(htmlToTextEditorConformHtml(answers));
    $(explanationEditor.container.firstChild).html(htmlToTextEditorConformHtml(explanation));
    codeEditor.setValue(code, -1);
}

function initTextEditor(editorId, placeHolder, theme, toolbarItems) {
    let id = editorId.trim().startsWith("#") ? editorId.trim() : `#${editorId.trim()}`;
    return new Quill(id, {
        modules: {
            toolbar: toolbarItems
        },
        placeholder: placeHolder,
        theme: theme
    });
}

function deleteAnswers() {
    $(`#${answersId}`).html("");
}

function showOnlyNeededEditors() {
    if (! editorsAlreadyInitiated) initEditorsIfNeeded();
    const cardtype = extractElementValueById(cardtypeId).toLowerCase();
    resetEditorsContent();
    deleteAnswers();
    hideEditors();
    switch (cardtype) {
        case 'programming': showNeededEditorsForProgrammingCard(); break;
        case 'clozetext': showNeededEditorsForClozetextCard(); break;
        case 'freetext': showNeededEditorsForFreetextCard(); break;
        default: showNeededEditorsForSimpleCard();
    }
}

function showNeededEditorsForProgrammingCard() {
    const language = extractElementValueById(programmingLanguageId).toLowerCase();
    codeEditor = initCodeEditor(codeEditorId, language, javaInitialCode, defaultCodeEditorTheme);
    setAttrVal(questionEditor.container.firstChild, "data-placeholder", standardQuestionPlaceholder);
    showElement(programmingLanguageRowId);
    showElement(codeEditorRowId);
    showElement(explanationRowId);
    showElement(questionRowId);
    showProgrammingLanguageIcon(language);
}

function showNeededEditorsForClozetextCard() {
    setAttrVal(questionEditor.container.firstChild, "data-placeholder", clozetextCardQuestionPlaceholder);
    showElement(explanationRowId);
    showElement(questionRowId);
}

function showNeededEditorsForFreetextCard() {
    setAttrVal(questionEditor.container.firstChild, "data-placeholder", standardQuestionPlaceholder);
    showElement(explanationRowId);
    showElement(questionRowId);
    showElement(answerRowId);
}

function showNeededEditorsForSimpleCard() {
    setAttrVal(questionEditor.container.firstChild, "data-placeholder", standardQuestionPlaceholder);
    showElement(explanationRowId);
    showElement(questionRowId);
    showElement(answerRowId);
    showElement(answersId);
    showElement(answerCreationButtonId);
}

function hideEditors() {
    hideElement(answerRowId);
    hideElement(questionRowId);
    hideElement(explanationRowId);
    hideElement(codeEditorRowId);
    hideElement(programmingLanguageRowId);
    hideElement(answerCreationButtonId);
    hideElement(answersId);
}

function showProgrammingLanguageIcon(language) {
    if (definedAndNonEmpty(language)) {
        const icon = `<span><i style="font-size: 16px;" class="icon-${language}"></i> Code</span>`;
        resetElement(programmingLanguageIconId, icon);
    }
}

function initCodeEditor(editorId, lang, placeholder, theme) {
    let editor = ace.edit(editorId);
    editor.setTheme(`ace/theme/${theme}`);
    editor.setValue(javaInitialCode, -1);
    editor.getSession().setMode(`ace/mode/${lang}`);
    return editor;
}

function extractAnswers() {
    const cardType = extractElementValueById(cardtypeId).toLowerCase();
    switch (cardType) {
        case 'programming': return extractProgrammingCardAnswers();
        case 'clozetext':   return extractClozetextCardAnswers();
        case 'freetext':    return extractFreeTextCardAnswers();
        default:            return extractSimpleCardAnswers();
    }
}

function extractQuestion() {
    const cardType = extractElementValueById(cardtypeId).toLowerCase();
    if (cardType === 'clozetext') {
        return buildQuestionFrom(questionEditor.container.firstChild.innerHTML);
    }
    return questionEditor.container.firstChild.innerHTML;
}

function buildQuestionFrom(clozeText) {
    let result = clozeText;
    let pastResult = undefined;
    let i = 0;
    while (result !== pastResult) {
        pastResult = result;
        const matchedWords = result.match(uniqueClozeTextAnswerRegex);
        const matchedWord = definedAndNonEmpty(matchedWords) ? matchedWords.shift() : "";
        result = result.replace(uniqueClozeTextAnswerRegex, `<span style='max-width: ${matchedWord*8}px;' id='cloze${i++}'/>`);
    }
    return result;
}

function extractProgrammingCardAnswers() {
    return {
        'correctAnswers': extractCodePortion(javaSampleSolutionRegex),
        'incorrectAnswers': []
    }
}

function extractClozetextCardAnswers() {
    const text = htmlContentOf(questionEditor.container.firstChild.innerHTML);
    return {
        'correctAnswers': text.match(globalClozeTextAnswerRegex).map(x => x.substring(2, x.length - 2)),
        'incorrectAnswers': []
    };
}

function extractFreeTextCardAnswers() {
    return {
        'correctAnswers': [htmlContentOf(answerEditor.container.firstChild.innerHTML)],
        'incorrectAnswers': []
    };
}

function extractSimpleCardAnswers() {
    const correctAnswers = [];
    const incorrectAnswers = [];
    for(let i = 1; i < answerId; i++) {
        const answer = $(`#${genHiddenAnswerIdFrom(i)}`).html();
        const answerIsCorrect = $(`#${genAnswerCheckboxIdFrom(i)}`).prop("checked");
        if (definedAndNonEmpty(answer) && defined(answerIsCorrect) && answerIsCorrect) {
            correctAnswers.unshift(answer);
        }
        else if (definedAndNonEmpty(answer) && defined(answerIsCorrect) && (! answerIsCorrect)) {
            incorrectAnswers.unshift(answer);
        }
    }
    return {'correctAnswers': correctAnswers, 'incorrectAnswers': incorrectAnswers};
}

function extractExplanation() {
    return explanationEditor.container.firstChild.innerHTML;
}

function extractElementValueById(elementId) {
    return $(`#${elementId}`)
        .val()
        .trim()
}

function showElement(id) {
    return $(`#${id}`).show();
}

function hideElement(id) {
    return $(`#${id}`).hide();
}

function resetElement(id, content = "") {
    return $(`#${id}`).html(content);
}

function setAttrVal(element, attributeName, value) {
    return $(element).attr(attributeName, value)
}

function htmlContentOf(element) {
    return $(element).html();
}

function textContentOf(element) {
    return $(element).text();
}

function textContentOfElementById(elementId) {
    return $(`#${elementId}`).text();
}

function extractInitialSnippet() {
    return extractCodePortion(javaInitialSnippetRegex);
}

function extractSampleSolution() {
    return extractCodePortion(javaSampleSolutionRegex);
}

function extractTestCode() {
    return extractCodePortion(javaTestCodeRegex);
}

function extractCodePortion(extractionRegex) {
    const codeEditorContent = codeEditor.getValue();
    const matches = codeEditorContent.match(extractionRegex);
    return definedAndNonEmpty(matches) ? matches.shift() : ""
}


function definedAndNonEmpty(container) {
    return defined(container) && (container.length !== 0)
}

function defined(item) {
    return (item !== undefined) && (item !== null)
}

function createAnswerFrom(answerContent, isCorrect = false) {
    if (answerContent !== "<br>") {
        const id = genNewAnswerId();
        $(answerEditor.container.firstChild).html("");
        answerContent.attr("style", "display: inline");
        const newAnswer = `
            <li id="${id}">
                 <span id="${genDisplayedAnswerIdFrom(id)}" style="max-width: 390px; display: inline">${limitText(answerContent.text(), 45)}</span>
                 <span id="${genHiddenAnswerIdFrom(id)}" hidden>${answerContent.html()}</span>
                 <div style="transform: translate(375px, -24px); position: relative;">
                     <a href="#answer-container">
                        <i style="color: red;" class="far fa-window-close" id="selected-destroy-icon" onclick="destroyAnswer(${id});" data-toggle="tooltip" title="Destroy answer"></i>
                     </a>
                     <a href="#answers-container" style="left: 7px; position: relative;" >
                        <i onclick="editAnswer(${id});" id="selected-edit-icon" class="fas fa-edit"  data-toggle="tooltip" title="Edit answer"></i>
                     </a>
                     <span class="form-check" style="display: inline; left: 14px; position: relative;">
                        <input type="checkbox" class="form-check-input" id="${genAnswerCheckboxIdFrom(id)}" ${isCorrect ? "checked": ""}>
                        <label class="form-check-label" for="materialUnchecked">correct answer</label>
                     </span>
                 </div>
            </li>`;
        $(`#${answersId}`).append(newAnswer);
    }
}

function createAnswer() {
    const answerContent = $(answerEditor.container.firstChild.innerHTML);
    createAnswerFrom(answerContent);
}

function genDisplayedAnswerIdFrom(id) {
    return `answer-${id}-content`;
}

function genHiddenAnswerIdFrom(id) {
    return `answer-${id}-true-content`;
}

function genAnswerCheckboxIdFrom(id) {
    return `checkbox-${id}`;
}

function limitText(text, limit) {
    let result = text.trim();
    if (result.length > limit) {
        return `${result.substring(0, limit - 3)}...`
    }
    return result;
}

function extractCardTopics() {
    return extractElementValueById(cardTopicsId)
        .split(",")
        .filter(definedAndNonEmpty);
}

function extractCardTitle() {
    return extractElementValueById(cardTitleInputId);
}

function extractCardType() {
    return extractElementValueById(cardtypeId).toLowerCase();
}

function destroyAnswer(id) {
    $(`#${id}`).remove()
}

function editAnswer(id) {
    const answerToEdit = $(`#${genHiddenAnswerIdFrom(id)}`).html();
    $(answerEditor.container.firstChild).html(answerToEdit);
    destroyAnswer(id);
}

function htmlToTextEditorConformHtml(html) {
    return html
        .replace(/<div>/g, "")
        .replace(/<\/div>/g, "")
        .replace(/<span>/g, "")
        .replace(/<\/span>/g, "")
        .replace(/[\n|\r]/g, "");
}
