const javaInitialCode = `
/**
 * Subato Learning System -- Studio
 */

/**
 * Bitte definieren Sie nach der Annotation, den Anfang-Code,
 * den der Lernende beim Lernen ergänzen sollte
 */
@_InitialCode_
    
    
/**
 * Bitte definieren Sie nach der Annotation den richtigen Code(Muster Lösung)
 */
@_SampleSolution_
    
    
/**
 * Bitte definieren Sie nach der Annotation, mindestens einen Testfall,
 * anhandessen die Lösung des Lernenden auf Qualität überprüft wird.
 */
@_Tests_
import static org.junit.Assert.*;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;
    
    
`;