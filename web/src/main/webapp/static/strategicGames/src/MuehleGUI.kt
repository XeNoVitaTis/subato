import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.HTMLCanvasElement
import org.w3c.dom.get
import kotlin.browser.document
import kotlin.dom.appendText
import kotlin.math.PI

class MuehleGUI{
    var game:Muehle = Muehle()
    val div = document.createElement("div")
    var fs = Array<Array<Field>>(game.columns,{_ -> Array<Field>(size = game.rows, init = { _ -> Field(0,0)})})

    fun init() {
        val empty = document.getElementsByClassName("empty")
        for (i in 0 until empty.length) {
            empty.get(i)!!.setAttribute("width", 80.toString())
            empty.get(i)!!.setAttribute("height", 80.toString())
            empty.get(i)!!.appendText(" ")
        }
        val field = document.getElementsByClassName("field")
        for (i in 0 until field.length) {
            field.get(i)!!.setAttribute("width", 80.toString())
            field.get(i)!!.setAttribute("height", 80.toString())
        }
        val horiz = document.getElementsByClassName("horiz")
        for (i in 0 until horiz.length) {
            horiz.get(i)!!.setAttribute("width", 80.toString())
            horiz.get(i)!!.setAttribute("height", 80.toString())
            val horiz1 = Verti()
            horiz.get(i)!!.appendChild(horiz1.canvas)
            horiz1.paint()
        }
        val verti = document.getElementsByClassName("verti")
        for (i in 0 until verti.length) {
            verti.get(i)!!.setAttribute("width", 80.toString())
            verti.get(i)!!.setAttribute("height", 80.toString())
            val horiz1 = Horiz()
            verti.get(i)!!.appendChild(horiz1.canvas)
            horiz1.paint()
        }
        for (c in 0 until fs.size) {
            for (f in 0 until fs[c].size){
                fs[c][f] = Field(c.toByte(),f.toByte())
                document.getElementById("$f$c")!!.appendChild(fs[c][f].canvas)
                fs[c][f].paint()

            }
        }
    }

    fun paint(){
        for (c in fs) {
            for (f in c) {
                f.paint()
            }
        }

    }

    inner class Verti(val canvas: HTMLCanvasElement = document.createElement("canvas")as HTMLCanvasElement) {
        init {
            canvas.height = 80
            canvas.width = 80
        }
        fun paint(){
            var ctx: CanvasRenderingContext2D = canvas.getContext("2d") as CanvasRenderingContext2D
            ctx.fillRect(0.0,30.0,80.0,20.0)
            ctx.stroke();
        }
    }
    inner class Horiz(val canvas: HTMLCanvasElement = document.createElement("canvas")as HTMLCanvasElement) {
        init {
            canvas.height = 80
            canvas.width = 80
        }
        fun paint(){
            var ctx: CanvasRenderingContext2D = canvas.getContext("2d") as CanvasRenderingContext2D
            ctx.fillRect(30.0,0.0,20.0,80.0)
            ctx.stroke();
        }
    }

    inner class Field(val c:Byte,val r:Byte
                      ,val canvas: HTMLCanvasElement = document.createElement("canvas")as HTMLCanvasElement) {

        init {
            canvas.height = 80
            canvas.width = 80
            canvas.addEventListener("click", {
                println("clicked: "+c+" "+r)

                val m: Move? = (this@MuehleGUI.game.createMove(c, r))
                println(m)
                println(this@MuehleGUI.game.legalMoves())
                if (m!=null){ // && this@MuehleGUI.game.legalMoves().find {x -> x.startsWith(m)} !=null)){
                    println("was legal")
                    this@MuehleGUI.game = this@MuehleGUI.game.doMove(m)
                    println("moved")
                }
                println(this@MuehleGUI)
                this@MuehleGUI.paint()
            })
        }
        fun paint(){
            var ctx: CanvasRenderingContext2D = canvas.getContext("2d") as CanvasRenderingContext2D
            ctx.beginPath();
            ctx.arc(40.0,40.0,40.0,0.0,2* PI)

            ctx.fillStyle =  when (this@MuehleGUI.game.getAtPosition(c,r)) {
                this@MuehleGUI.game.playerNone -> "#000000"
                this@MuehleGUI.game.playerOne -> "#FF0000"
                else -> "#0000FF"
            }
            ctx.fill()
            ctx.stroke();
        }

    }

}


fun startMuehleGUI(){
    MuehleGUI().init()
}
