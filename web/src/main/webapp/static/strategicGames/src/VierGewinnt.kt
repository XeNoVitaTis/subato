class Vier : AbstractRegularGame<Byte>(7, 6) {
    override fun createMove(column: Byte, row: Byte): Byte {
       return column
    }

    override fun interestingMoves(): List<Byte> {
        val result = mutableListOf<Byte>()
        val pms = if (movesDone == 0 || (movesDone == 1 && lastColumn.toInt() >= 2&& lastColumn.toInt() <= 4))
            startMoves else possibleMoves
        for (c in pms) if (b[c.toInt()][rows.toInt() - 1] == AbstractRegularGame.NONE) result.add(c)

        return result
    }

    override fun legalMoves(): List<Byte> {
        val result = mutableListOf<Byte>()
        val pms =  possibleMoves

        for (c in pms) if (b[c.toInt()][rows.toInt() - 1] == AbstractRegularGame.NONE) result.add(c)

        return result
    }

    override fun doMove(m: Byte): Vier {
        val result = this.copy()
        for (r in 0 until rows) {
            if (result.b[m.toInt()][r] == AbstractRegularGame.NONE) {
                result.b[m.toInt()][r] = player
                result.player = nextPlayer()
                result.lastColumn = m
                result.lastRow = r.toByte()
                result.movesDone = movesDone + 1
                break
            }
        }
        return result
    }

    override fun noMoreMove(): Boolean {
        return movesDone == rows * columns
    }

    override fun wins(player: Byte): Boolean {
        if (movesDone < 7) return false
        return if (b[lastColumn.toInt()][lastRow.toInt()] != player) false else (vertical(player) || horizontal(player)
                || diagonal1(player) || diagonal2(player))
    }

    private fun vertical(p: Byte): Boolean {
        var inRow: Byte = 1
        var cr = lastRow - 1
        while (cr >= 0 && b[lastColumn.toInt()][cr] == p) {
            inRow++
            cr--
        }
        return inRow >= 4
    }

    private fun horizontal(p: Byte): Boolean {
        var inRow: Byte = 1
        run {
            var cc = lastColumn - 1
            while (cc >= 0 && b[cc][lastRow.toInt()] == p) {
                inRow++
                cc--
            }
        }
        var cc = lastColumn + 1
        while (cc < columns && b[cc][lastRow.toInt()] == p) {
            inRow++
            cc++
        }
        return inRow >= 4
    }

    private fun diagonal1(p: Byte): Boolean {
        var inRow: Byte = 1
        var cc = (lastColumn - 1).toByte()
        var cr = (lastRow - 1).toByte()
        while (cc >= 0 && cr >= 0 && b[cc.toInt()][cr.toInt()] == p) {
            inRow++
            cc--
            cr--
        }
        cc = (lastColumn + 1).toByte()
        cr = (lastRow + 1).toByte()
        while (cc < columns && cr < rows && b[cc.toInt()][cr.toInt()] == p) {
            inRow++
            cc++
            cr++
        }
        return inRow >= 4
    }

    private fun diagonal2(p: Byte): Boolean {
        var inRow: Byte = 1
        var cc = (lastColumn - 1).toByte()
        var cr = (lastRow + 1).toByte()
        while (cc >= 0 && cr < rows && b[cc.toInt()][cr.toInt()] == p) {
            inRow++
            cc--
            cr++
        }
        cc = (lastColumn + 1).toByte()
        cr = (lastRow - 1).toByte()
        while (cc < columns && cr >= 0 && b[cc.toInt()][cr.toInt()] == p) {
            inRow++
            cc++
            cr--
        }
        return inRow >= 4
    }

    override fun evalState(player: Byte): Int {
        return if (wins())
            if (lastPlayer() == player)
                Int.MAX_VALUE
            else
                -Int.MAX_VALUE
        else
            state(player) - 10 * state(otherPlayer(player))
    }

    fun ps(player: Byte, rV: Int): Int {
        return if ((rV == 3 * player)) 100 else (if ((rV == 2 * player)) 10 else 0)
    }

    fun state(p: Byte): Int {
        var result = 0
        for (c in 0 until columns - 3) {
            for (r in 0 until rows - 3) {
                result = (result
                        + ps(p, b[c][r].toInt() + b[c][r + 1].toInt() + b[c][r + 2].toInt() + b[c][r + 3].toInt())
                        + ps(p, b[c][r].toInt() + b[c + 1][r + 1].toInt() + b[c + 2][r + 2].toInt() + b[c + 3][r + 3].toInt())
                        + ps(p, b[c][r].toInt() + b[c + 1][r].toInt() + b[c + 2][r].toInt() + b[c + 3][r].toInt()))

                val c2 = columns - 1 - c
                if (c2 > 3)
                    result = (result + ps(p, b[c2][r].toInt() + b[c2][r + 1].toInt() + b[c2][r + 2].toInt() + b[c2][r + 3].toInt()))

                val s = rows - 1 - r
                result = (result
                        + ps(p, b[c][s].toInt() + b[c + 1][s].toInt() + b[c + 2][s].toInt() + b[c + 3][s].toInt())
                        + ps(p, b[c][s].toInt() + b[c + 1][s - 1].toInt() + b[c + 2][s - 2].toInt() + b[c + 3][s - 3].toInt()))
            }
        }
        return result
    }
    fun copy():Vier{
        var result = Vier();
        result.lastColumn = lastColumn;
        result.lastRow=lastRow
        result.movesDone=movesDone
        result.player=player
        for (c in 0 until columns)
            for (r in 0 until rows) {
                result.b[c][r] = b[c][r]
            }
        return result;
    }

    companion object {

        val possibleMoves = byteArrayOf(3, 2, 4, 1, 5, 0, 6)

        val startMoves = byteArrayOf(3, 2, 4 )
    }

}

