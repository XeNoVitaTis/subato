import org.w3c.dom.CanvasRenderingContext2D
import org.w3c.dom.HTMLCanvasElement
import kotlin.browser.document
import kotlin.math.PI

class GameBoard<M>(val game: RegularGame<M>){
    var g:RegularGame<M> = game
    val div = document.createElement("div")

    var fs = Array<Array<Field>>(game.columns,{_ -> Array<Field>(size = game.rows, init = { _ -> Field(0,0)})})

    fun paint(){
        for (c in fs) {
            for (f in c) {
                f.paint()
            }
        }

    }

    inner class Field(val c:Byte,val r:Byte
                      ,val canvas: HTMLCanvasElement = document.createElement("canvas")as HTMLCanvasElement){

        init {
            canvas.height = 80
            canvas.width = 80
            canvas.addEventListener("click", {
    //            println("clicked: "+c+" "+r)
                val m:M= (this@GameBoard.g.createMove(c, r))!!
    //            println("move: "+m+" "+this@GameBoard.g.currentPlayer())
    //            println(this@GameBoard.g.legalMoves())
                if (this@GameBoard.g.currentPlayer()==this@GameBoard.g.playerOne
                        &&  !this@GameBoard.g.ended()
                        &&  this@GameBoard.g.legalMoves().contains(m)) {
    //                println("making move")
                    this@GameBoard.g = this@GameBoard.g.doMove(m) as RegularGame<M>
    //                println("repaint")
                    this@GameBoard.paint()
                    if ( !this@GameBoard.g.ended()){
    //                    println("calculating AI move")
                        val bestMove:M = bestMove(this@GameBoard.g, 6)!!
    //                    println(bestMove)
                        this@GameBoard.g = this@GameBoard.g.doMove(bestMove) as RegularGame<M>
                        this@GameBoard.paint()
                    }
                }
            })
            paint()
        }
        fun paint(){
            var ctx: CanvasRenderingContext2D = canvas.getContext("2d") as CanvasRenderingContext2D
            ctx.beginPath();
            ctx.arc(40.0,40.0,40.0,0.0,2* PI)

            ctx.fillStyle =  when (this@GameBoard.g.getAtPosition(c,r)) {
                this@GameBoard.g.playerNone -> "#000000"
                this@GameBoard.g.playerOne -> "#FF0000"
                else -> "#0000FF"
            }
            ctx.fill()
            ctx.stroke();
        }
    }

    init {
        g=game
        for (r in 0 until game.rows) {
            for (c in 0 until game.columns) {
                val rr:Int = game.rows-r-1
                fs[c][rr] = Field(c.toByte(),rr.toByte())
                div.appendChild(fs[c][rr].canvas)
            }
            div.appendChild(document.createElement("br"))
        }
    }

}

