class TicTacToe internal constructor() : AbstractRegularGame<Pair<Byte, Byte>>(3, 3) {
    override  fun createMove(column: Byte, row: Byte):Pair<Byte,Byte>{
        return Pair(column,row)
    }

    override fun legalMoves(): List<Pair<Byte, Byte>> {
        val result = mutableListOf<Pair<Byte, Byte>>()

        for (c in 0 until columns)
            for (r in 0 until rows)
                if (b[c][r] == AbstractRegularGame.NONE)
                    result.add(Pair<Byte, Byte>(c.toByte(), r.toByte()))

        return result
    }


    override fun doMove(m: Pair<Byte, Byte>): TicTacToe {
        val result = this.copy()
        result.player = nextPlayer()
        result.b[m.first.toInt()][m.second.toInt()] = player
        result.movesDone = (movesDone + 1).toByte().toInt()
        return result
    }

    override fun noMoreMove(): Boolean {
        return rows * columns == movesDone
    }

    override fun wins(player:  Byte): Boolean {
        return (checkRows(player)
                || checkColumns(player)
                || checkDiagonal1(player)
                || checkDiagonal2(player))
    }

    private fun checkRows(p: Byte): Boolean {
        for (r in 0 until rows) {

            for (c in 0 until columns) {
                if (b[c][r] != p) break

                if (c.toInt() == columns - 1) return true
            }
        }
        return false
    }

    private fun checkColumns(p: Byte): Boolean {
        for (c in 0 until columns) {
            for (r in 0 until rows) {
                if (b[c][r] != p) break
                if (r.toInt() == rows - 1) return true
            }
        }
        return false
    }

    private fun checkDiagonal1(p: Byte): Boolean {
        for (r in 0 until rows) {
            if (b[r][r] != p) break
            if (r.toInt() == rows - 1) return true
        }
        return false
    }

    private fun checkDiagonal2(p: Byte): Boolean {
        for (r in 0 until rows) {
            if (b[r][rows - r.toInt() - 1] != p) break
            if (r.toInt() == rows - 1) return true
        }
        return false
    }

    override fun evalState(player: Byte): Int {
        return if (wins()) if (lastPlayer() == player) Int.MAX_VALUE else -Int.MAX_VALUE else 0
    }

    fun copy():TicTacToe{
        var result = TicTacToe();
        result.lastColumn = lastColumn;
        result.lastRow=lastRow
        result.movesDone=movesDone
        result.player=player
        for (c in 0 until columns)
            for (r in 0 until rows)
                result.b[c][r] = b[c][r]
        return result;
    }

}
