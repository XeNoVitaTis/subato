name := "subato"

version := "0.1.0"

organization := "name.panitz"

scalaVersion := "2.11.12"

resolvers ++= Seq("snapshots"     at "https://oss.sonatype.org/content/repositories/snapshots",
                "releases"        at "https://oss.sonatype.org/content/repositories/releases"
                )

Seq(webSettings :_*)


//brauch ich das???
//unmanagedResourceDirectories in Test <+= (baseDirectory) { _ / "src/main/webapp" }




//??????? wie werde ich die emacs dateien los????

excludeFilter in unmanagedSources := HiddenFileFilter || "*~"
excludeFilter in unmanagedResources := HiddenFileFilter || "*~"

scalacOptions ++= Seq("-deprecation", "-unchecked","-feature")

// Lazy Installation of ace.io artifact
lazy val installAceIO = taskKey[Unit]("Install ace.io")
installAceIO := {
  val aceIOInstallationPath      = "./src/main/webapp/static/sls/js/src-min-noconflict/"
  lazy val aceIONoConflict       = "/tmp/ace-builds-master/src-min-noconflict/"
  lazy val aceIOUrl              = new URL("https://github.com/ajaxorg/ace-builds/archive/master.zip")
  lazy val tmp                   = "/tmp/"
  lazy val aceIODownloadTarget   = "/tmp/ace-builds-master/"
  try {
    if(java.nio.file.Files.notExists(new File(aceIOInstallationPath).toPath)) {
      println("[info] Installing Ace.io ...")
      IO.unzipURL(aceIOUrl, new File(tmp))
      IO.move(new File(aceIONoConflict), new File(aceIOInstallationPath))
      IO.delete(new File(aceIODownloadTarget))
    } else {
      println("[warn] Ace.io is already available")
    }
  } catch {
    case e: Throwable => println(s"[error] Unable to install Ace.io due to: ${e.getMessage}")
  }
}

//(compile : compile)  := { (compile:compile) dependsOn thirddPartyJavascript }

lazy val thirdPartyJavascript = taskKey[Unit]("Install 3dPartyJavascript")
thirdPartyJavascript := {
  println("Getting EditArea")
  IO.unzipURL(new URL("https://downloads.sourceforge.net/project/editarea/EditArea/EditArea%200.8.2/editarea_0_8_2.zip"),
    new File("src/main/webapp/static/3dpartyjscript/"))

  println("Getting ACE-Edit")
  IO.unzipURL(new URL("https://github.com/ajaxorg/ace-builds/archive/master.zip"),
    new File("src/main/webapp/static/3dpartyjscript/"))
  IO.createDirectory(new File("src/main/webapp/static/3dpartyjscript/ace/src-min"))
  IO.move(new File("src/main/webapp/static/3dpartyjscript/ace-builds-master/src-min"),
    new File("src/main/webapp/static/3dpartyjscript/ace/src-min"))
  IO.delete(new File("src/main/webapp/static/3dpartyjscript/ace-builds-master"))

  println("Getting tinymce")
  val file = IO.unzipURL(new URL("https://github.com/tinymce/tinymce-dist/archive/4.9.9.zip"),
    new File("src/main/webapp/static/3dpartyjscript/"))
  IO.createDirectory(new File("src/main/webapp/static/3dpartyjscript/tiny_mce/js/tinymce"))
  IO.move(new File("src/main/webapp/static/3dpartyjscript/tinymce-dist-4.9.9"),
    new File("src/main/webapp/static/3dpartyjscript/tiny_mce/js/tinymce"))
}


//ProjectRef("")

PB.protoSources in Compile := Seq(file("../rpc"))

PB.targets in Compile := Seq(
  scalapb.gen() -> (sourceManaged in Compile).value
)


libraryDependencies ++= {
  val liftVersion = "3.2.0" //"3.0-M2" //.1"
  Seq(
    "net.liftweb"       %% "lift-json"          % liftVersion ,
    "net.liftweb"       %% "lift-webkit"        % liftVersion        % "compile",
    "net.liftweb"       %% "lift-mapper"        % liftVersion        % "compile",
    "net.liftweb"       % "lift-webkit_2.11"    % liftVersion        % "provided",
    "net.liftmodules"   %% "lift-jquery-module_3.0" % "2.10",
    "net.liftmodules" %% "fobo_3.0"    % "1.7"       % "compile",
    "org.eclipse.jetty" % "jetty-webapp"        % "8.1.17.v20150415"  % "container,test",
    "org.eclipse.jetty" % "jetty-plus"          % "8.1.17.v20150415"  % "container,test", // For Jetty Config
    "org.eclipse.jetty.orbit" % "javax.servlet" % "3.0.0.v201112011016" % "container,test" artifacts Artifact("javax.servlet", "jar", "jar"),
    "ch.qos.logback"    % "logback-classic"     % "1.1.3",
    // "org.apache.logging.log4j" % "log4j-core" % "2.8.2",
    //"org.webjars" % "syntaxhighlighter" % "3.0.83",
    "com.h2database"    % "h2"                  % "1.4.187",
    "org.specs2"        %% "specs2-core"        % "3.6.4"           % "test",
    "com.github.javaparser" % "javaparser-core" % "3.9.1",
    "org.jsoup"             % "jsoup"           % "1.8.3",
    "org.apache.httpcomponents" % "httpclient" % "4.5.9",
    "com.trueaccord.scalapb" %% "scalapb-runtime"      % com.trueaccord.scalapb.compiler.Version.scalapbVersion % "protobuf",
    // for gRPC
    "io.grpc"                %  "grpc-netty"           % "1.24.0",
    "io.netty"  % "netty-tcnative-boringssl-static" % "2.0.25.Final",
    "com.trueaccord.scalapb" %% "scalapb-runtime-grpc" % com.trueaccord.scalapb.compiler.Version.scalapbVersion,
    // for JSON conversion
    "com.trueaccord.scalapb" %% "scalapb-json4s"       % "0.3.0"
  )
}



