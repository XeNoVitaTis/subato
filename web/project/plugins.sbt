//Enable the sbt web plugin
addSbtPlugin("com.earldouglas" % "xsbt-web-plugin" % "0.7.0")

//Enable the sbt eclipse plugin
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "4.0.0")

//addSbtPlugin("org.ensime" % "sbt-ensime" % "2.6.1")

addSbtPlugin("com.thesamet" % "sbt-protoc" % "0.99.9")

libraryDependencies += "com.trueaccord.scalapb" %% "compilerplugin" % "0.6.0-pre5"
